--
-- ADT Interfaces Configuration
-- File Name:  NSSintfConfig_asa8.0.sql
-- $Revision:   1.18  $
--
--////////////////////////////////////////////////
--
--  Converted by BW		14-Jan-2004		Initial 	
--  Boilerplate Configuration Data...  Do not change
--
--/////////////////////////////////////////////////

BEGIN
	DECLARE s_facility_name VARCHAR(250);
	DECLARE l_unitId INTEGER;

	-- MUST USE UNIT 0 NAME!!  ALSO must use UNIT 0 name in Registry arg 4
	-- Someday, this may change, but for now the use of unit 0 is required
	SELECT UnitName INTO s_facility_name FROM cbord.cbo3001p_bizunit WHERE UnitId=0;

	-- find our unit id based on facility name
	SELECT UnitId INTO l_unitId FROM cbord.cbo3001p_bizunit WHERE Unitname=s_facility_name AND unittype='F';

	-- delete old configuration for this interface (note this relies on a cascading delete of all other entries)
	DELETE FROM cbord.cif0001p_ConfigurationSet WHERE appid='ADT' AND unitid=l_unitId;

	--////////////////////////////////////////////////
	--
	--  Configurable DB Parameters
	--
	--/////////////////////////////////////////////////
	--AutoAdmit
	--	valid parmvalues:  Y or N
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='AutoAdmit' AND application='ADT') THEN
	  DELETE FROM cbord.cbo0001p_parms where parmname='AutoAdmit' AND application='ADT'
	END IF;
	INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
	     VALUES                ('AutoAdmit','Y','ADT',      l_unitId);

	--AutoReAdmit
	--	valid parmvalues:  OFF, ALWAYS, IFNODISCHARGE, IFROOM, IFNEWBILLINGID, or IFDIETORDER
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='AutoReAdmit' AND application='ADT') THEN
	  DELETE FROM cbord.cbo0001p_parms where parmname='AutoReAdmit' AND application='ADT'
	END IF;
	INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
     VALUES                ('AutoReAdmit','IFNODISCHARGE','ADT',      l_unitId);

	-- AccountNbrDefinesVisit. 
	-- Set to 'Y' the interface will attempt to match on MRN/BillingID
	-- and reject transactions that do not match both.  Default is 'N'.
	IF EXISTS (SELECT * FROM cbo0001p_parms where parmname='AccountNbrDefinesVisit' AND application='ADT') THEN
	  DELETE FROM cbo0001p_parms where parmname='AccountNbrDefinesVisit' AND application='ADT'
	END IF;
	INSERT INTO cbo0001p_parms (Parmname,             parmvalue, application, unitid)
	VALUES                     ('AccountNbrDefinesVisit','Y',     'ADT',       l_unitId);

	-- UpdatesToInactiveBillingID.
	-- Set to 'Y' or 'N'.  
	-- If a tx is received that the MRN/BillingID correlates to an inactive visit, 
	-- Y will allow that visit's admission information to be updated, N will reject that transaction.
	--IF EXISTS (SELECT * FROM cbo0001p_parms where parmname='UpdatesToInactiveBillingID' AND application='ADT') THEN
	--  DELETE FROM cbo0001p_parms where parmname='UpdatesToInactiveBillingID' AND application='ADT'
	--END IF;
	--INSERT INTO cbo0001p_parms (Parmname,             parmvalue, application, unitid)
	--VALUES                     ('UpdatesToInactiveBillingID','Y',     'ADT',       l_unitId);

	--Coded Allergy Handling
	--     Valid Parmvalues = UNION or REPLACE
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='AllergyUpdate' AND application='ADT') THEN
	  DELETE FROM cbord.cbo0001p_parms where parmname='AllergyUpdate' AND application='ADT'
	END IF;
	INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
     VALUES                ('AllergyUpdate','REPLACE','ADT',      l_unitId);

	-- Critical data changes
	--		Valid Parmvalues = WARN, NOWARN, SUPRESS
	--		Default is WARN
	IF EXISTS (SELECT * FROM cbo0001p_parms where parmname='CriticalDataUpdate' AND application='ADT') THEN
	  DELETE FROM cbo0001p_parms where parmname='CriticalDataUpdate' AND application='ADT'
	END IF;
	INSERT INTO cbo0001p_parms (Parmname,             parmvalue, application, unitid)
	VALUES                     ('CriticalDataUpdate','NOWARN',     'ADT',       l_unitId);

	-- ClearDischargedtmOnCancelDischarge: flag to control behavior of CancelDischarge message
	-- 'Y' --> clear the Discharge DTM (set to null) 
	-- 'N' --> leave the Discharge DTM alone
	-- Default to 'N'
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='ClearDischargedtmOnCancelDischarge' AND application='ADT') THEN
	  DELETE FROM cbord.cbo0001p_parms where parmname='ClearDischargedtmOnCancelDischarge' AND application='ADT'
	END IF;
	INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
	     VALUES                ('ClearDischargedtmOnCancelDischarge','Y','ADT',      l_unitId);
	
	-- DietRestrictionsOnReadmit:  controls what happens to diet orders on a readmit
	-- 'CANCEL' - Orders are effectively canceled, left with previous admission, default
	-- 'KEEP' - Orders are copied from prior admission, status intact
	-- 'KEEPIFBILLINGIDMATCH' - Orders are copied from prior admission, status intact, only
	--                          if their is a BillingID match
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='DietRestrictionsOnReadmit' AND application='ADT') THEN
  		DELETE FROM cbord.cbo0001p_parms where parmname='DietRestrictionsOnReadmit' AND application='ADT'
	END IF;
	INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
     		VALUES                ('DietRestrictionsOnReadmit','CANCEL','ADT',      l_unitId);


	-- /////////////////////////////////////////
	-- // EOD Settings for Clinical Interfaces
	-- /////////////////////////////////////////
	
	-- Frequency of system even log purge  (Default:  0 = Daily)
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='PurgeFreq_sysevent' AND application='EOD') THEN
		UPDATE cbord.cbo0001p_parms set parmvalue='0'
		WHERE parmname='PurgeFreq_sysevent' and application='EOD';
	ELSE
		INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
		VALUES ('PurgeFreq_sysevent','0','EOD',0);
	END IF;


	-- Frequency of application event log purge  (Default:  0 = Daily)
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='PurgeFreq_applog' AND application='EOD') THEN
		UPDATE cbord.cbo0001p_parms set parmvalue='0'
		WHERE parmname='PurgeFreq_applog' and application='EOD';
	ELSE
		INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
		VALUES ('PurgeFreq_applog','0','EOD',0);
	END IF;

	-- Purge oldest ADT interface logs after how many entries  (Default:  '')
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='Genpurgeapplogadt2' AND application='EOD') THEN
		UPDATE cbord.cbo0001p_parms set parmvalue=''
		WHERE parmname='Genpurgeapplogadt2' and application='EOD';
	ELSE
		INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
		VALUES ('Genpurgeapplogadt2','','EOD',0);
	END IF;

	-- Purge ADT interface logs after how many days  (Default:  7)
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='Genpurgeapplogadt' AND application='EOD') THEN
		UPDATE cbord.cbo0001p_parms set parmvalue='7'
		WHERE parmname='Genpurgeapplogadt' and application='EOD';
	ELSE
		INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
		VALUES ('Genpurgeapplogadt','7','EOD',0);
	END IF;

	-- Purge oldest ADT system even logs after how many entries  (Default:  '')
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='GenPurgesyseventadt2' AND application='EOD') THEN
		UPDATE cbord.cbo0001p_parms set parmvalue=''
		WHERE parmname='GenPurgesyseventadt2' and application='EOD';
	ELSE
		INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
		VALUES ('GenPurgesyseventadt2','','EOD',0);
	END IF;

	-- Purge ADT system event logs after how many days  (Default:  7)
	IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='GenPurgesyseventadt' AND application='EOD') THEN
		UPDATE cbord.cbo0001p_parms set parmvalue='7'
		WHERE parmname='GenPurgesyseventadt' and application='EOD';
	ELSE
		INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
		VALUES ('GenPurgesyseventadt','7','EOD',0);
	END IF;

END;
