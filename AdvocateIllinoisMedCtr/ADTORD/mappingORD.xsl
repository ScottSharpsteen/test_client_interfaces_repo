<?xml version="1.0"?>
<!-- ADT XSLT for transforming HL7 messages. PVCS version: $Revision: #2 $ -->
<!-- this is for inclusion in the main xlate script for ADT messages -->
<!-- PVCS version: $Revision: #2 $ -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:translator="urn:translatorObject"
                version="1.0">

<!--	ORD: Map an ORDER  message root tag plus order type into a NSS update action
	'NW' = OrderEnter
	'CA' or 'DC' = OrderCancel
	'RP' = OrderReplace 
	'Unknown' otherwise -->
<xsl:template name="extractActionORD">
  <xsl:variable name="orderType" select="/ORM_O01/*/ORC[1]/ORC.1"/>
  <xsl:choose>

    <xsl:when test="$orderType='NW'">
      <xsl:value-of select="'OrderEnter'"/>
    </xsl:when>

    <xsl:when test="$orderType='CA' or $orderType='DC'">
      <xsl:value-of select="'OrderCancel'"/>
    </xsl:when>

    <xsl:when test="$orderType='RP'">
      <xsl:value-of select="'OrderReplace'"/>
    </xsl:when>

    <xsl:otherwise>
      <xsl:value-of select="'Unknown'"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template> 

<xsl:template name="extractOrderInfo">
	<orderInfo>
		<DietOrderList>
            	<xsl:for-each select="/ORM_O01/*/ORC">
				<DietOrderInfo>
					<!-- Extract Order Placer ID -->
					<!-- Extract order placer id if valued in ORC.2/EI.1 (default) -->
					<xsl:if test="ORC.2/EI.1">
						<OrderID><xsl:value-of select="ORC.2/EI.1"/></OrderID>
					</xsl:if>
					<!-- Extract Order Start DTM -->
					<!-- By assigning variables in the format below, if the date and time format received
						is invalid, then it will display they invalid warning, and it will continue to look
						for a propper date through out the message, and if a propper date is not found, it 
						will use the system date and time. -->
					<orderStartDTM>
						<!-- try for ORC.7/XCM.4 (default) first -->
						<xsl:variable name="ORC7Startdate">
							<xsl:choose>
								<!-- Check for data in primary default StartDTM location -->
								<xsl:when test="ORC.7/XCM.4">
									<xsl:value-of select="cbord:formatHL7DTM('Order StartDTM (ORC.7/XCM.4)',string(ORC.7/XCM.4))"/>
								</xsl:when>
								<!-- No data in ORC.7/XCM.4 (default) -->
								<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:choose>
						<!-- Data in ORC.7/XCM.4 is valued and valid: use it -->
							<xsl:when test="$ORC7Startdate != '' and not(contains($ORC7Startdate, 'Invalid'))">
								<xsl:value-of select="$ORC7Startdate"/>
							</xsl:when>
						      <!-- Invalid or no data in ORC.7/XCM.4 (default), try for ORC.15 (default) second -->
							<xsl:otherwise>
								<!-- try for ORC.15 (default) second -->
								<xsl:variable name="ORC15date">
									<xsl:choose>
										<!-- Check for data in secondary StartDTM location -->
										<xsl:when test="ORC.15">
											<xsl:value-of select="cbord:formatHL7DTM('Order StartDTM (ORC.15)',string(ORC.15))"/>
										</xsl:when>
										<!-- Invalid or no data in ORC.15 (default) -->											
										<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:choose>
									<!-- Data in ORC.15 is valued and valid: use it -->
									<xsl:when test="$ORC15date!='' and  not(contains($ORC15date, 'Invalid')) and contains($ORC7Startdate, 'Invalid')">
										<Start><xsl:value-of select="$ORC15date"/></Start>
										<WARNING><xsl:value-of select="$ORC7Startdate"/></WARNING>
									</xsl:when>
									<xsl:when test="$ORC15date!='' and  not(contains($ORC15date, 'Invalid'))">
										<xsl:value-of select="$ORC15date"/>
									</xsl:when>
									<!-- Give up, use system DTM -->
									<xsl:otherwise>
										<xsl:if test="not(contains($ORC7Startdate, 'Invalid')) and not(contains($ORC15date, 'Invalid'))">
											<xsl:value-of select="cbord:getNow()"/>
										</xsl:if>
										<xsl:if test="contains($ORC7Startdate, 'Invalid')">
											<Start><xsl:value-of select="cbord:getNow()"/></Start>
											<WARNING><xsl:value-of select="$ORC7Startdate"/></WARNING>
										</xsl:if>
										<xsl:if test="contains($ORC15date, 'Invalid')">
											<Start><xsl:value-of select="cbord:getNow()"/></Start>
											<WARNING><xsl:value-of select="$ORC15date"/></WARNING>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</orderStartDTM>

					<!-- Extract Order End DTM -->
					<!-- By assigning variables in the format below, if the date and time format received
						is invalid, then it will display the invalid warning, if it is a CA Order
						transaction, it will use the system DTM, otherwise tags will be empty. -->
					<xsl:variable name="ORC7Enddate">
						<xsl:choose>
							<xsl:when test="ORC.7/XCM.5">
								<xsl:value-of select="cbord:formatHL7DTM('Order EndDTM (ORC.7/XCM.5)',string(ORC.7/XCM.5))"/>
							</xsl:when>
							<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>

					<xsl:choose>
						<!-- When ORC.7/XCM.5 (default) is valued and propperly formatted, use it. -->
						<xsl:when test="$ORC7Enddate != '' and not(contains($ORC7Enddate, 'Invalid'))">
							<EndDTM><xsl:value-of select="$ORC7Enddate"/></EndDTM>
						</xsl:when>
						<!-- When ORC.7/XCM.5 (default) is not valued or invalid and msg type is Order Cancel use system DTM. -->
						<xsl:when test="$msgAction='OrderCancel'">
							<EndDTM>
								<xsl:choose>
									<xsl:when test="contains($ORC7Enddate, 'Invalid')">
										<End><xsl:value-of select="cbord:getNow()"/></End>
										<WARNING><xsl:value-of select="$ORC7Enddate"/></WARNING>
									</xsl:when>
									<xsl:otherwise><xsl:value-of select="cbord:getNow()"/></xsl:otherwise>
								</xsl:choose>
							</EndDTM>
						</xsl:when>
						<!-- Otherwise do check for invalid ORC7 end DTM to display WARNING -->
						<xsl:otherwise>
							<xsl:if test="contains($ORC7Enddate, 'Invalid')">
								<EndDTM>
									<WARNING><xsl:value-of select="$ORC7Enddate"/></WARNING>
								</EndDTM>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>

					<!-- Extract Order Confirmed By -->
					<xsl:choose>
						<!-- ORC.10 (default) Extract when data is valued. -->
						<xsl:when test="ORC.10">
							<ConfirmedBy><xsl:value-of select="ORC.10"/></ConfirmedBy>
						</xsl:when>
						<!-- No data in ORC.10 (default) Extract from ORC.11 (default) when data is valued. -->
						<xsl:when test="ORC.11">
							<ConfirmedBy><xsl:value-of select="ORC.11"/></ConfirmedBy>
						</xsl:when>
						<xsl:when test="ORC.12">
						<!-- No data in ORC.11 (default) Extract from ORC.12 (default) when data is valued. -->
							<ConfirmedBy><xsl:value-of select="ORC.12"/></ConfirmedBy>
						</xsl:when>
						<!-- No data valued, do nothing. -->
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>

					<!-- Extract Order Note -->
					<!-- If NTE segment follows the ODS segment, use NTE.3 (default) as ORD note. -->
					<xsl:if test="following-sibling::*/NTE">
						<OrderNote>
							<xsl:value-of select="following-sibling::*/NTE/NTE.3.LST/NTE.3"/>
						</OrderNote>
					</xsl:if>

					<!-- Extract Order Restrictions -->
					<xsl:if test="following-sibling::ODS/*/ODS.3">
						<CodedRestrictionList>
							<xsl:for-each select="following-sibling::ODS[1]/*/ODS.3/*">
								<CodedRestriction><xsl:value-of select="."/></CodedRestriction>
							</xsl:for-each>
						</CodedRestrictionList>
					</xsl:if>
				</DietOrderInfo>
           		</xsl:for-each>
            </DietOrderList>
     	</orderInfo>
    </xsl:template>
</xsl:stylesheet>