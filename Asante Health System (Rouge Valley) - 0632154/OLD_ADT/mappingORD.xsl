<?xml version="1.0"?>
<!-- ADT XSLT for transforming HL7 messages. PVCS version: $Revision:   1.0  $ -->
<!-- this is for inclusion in the main xlate script for ADT messages -->
<!-- PVCS version: $Revision:   1.0  $ -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:translator="urn:translatorObject"
                version="1.0">
                
<!-- include service order interface templates-->
<xsl:import href="mappingSRV.xsl"/>
<!-- include enteral order interface templates-->
<xsl:import href="mappingENT.xsl"/>
				
<xsl:key name="meal-key" match="ODS" use="concat(generate-id(..),ODS.2.LST)"/>
<!-- Identify an ODT within a ORM list -->
<xsl:key name="ODTList" match="ORM_O01.LST.ODT" use="generate-id(preceding-sibling::ORC[1])"/>
<!-- Identify an RXO within a ORM or OMP list -->
<xsl:key name="RXOList" match="RXO" use="generate-id(preceding::ORC[1])"/>

<!--	ORD: Map an ORDER  message root tag plus order type into a NSS update action
	'NW' = OrderEnter
	'CA' or 'DC' = OrderCancel
	'RP' = OrderReplace 
	'Unknown' otherwise -->
<xsl:template name="extractActionORD">
	
  <xsl:variable name="orderType" select="//ORC[1]/ORC.1"/>
  <xsl:choose>

    <xsl:when test="$orderType='NW'">
      <xsl:value-of select="'OrderEnter'"/>
    </xsl:when>

    <xsl:when test="$orderType='CA' or $orderType='DC'">
      <xsl:value-of select="'OrderCancel'"/>
    </xsl:when>

    <xsl:when test="$orderType='RP'">
      <xsl:value-of select="'OrderReplace'"/>
    </xsl:when>

    <xsl:otherwise>
      <xsl:value-of select="'Unknown'"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template> 

<xsl:template name="extractOrderInfo">
	<xsl:param name="MSGList"/>
	<!-- Check to see if an ORC was sent with no ODS segments -->
	<xsl:for-each select="$MSGList/*/ORC">
		<!-- Used to place a unique ID on the ORC currently being processed -->
		<xsl:variable name="ORC-ID" select="generate-id()"/>
		<!-- Throw warning if no ODS or ODT present in New or Replace Order message-->
		<xsl:if test="not(following-sibling::ORM_O01.LST.ODS[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) 
				and not(following-sibling::ORM_O01.LST.ODT[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) 
				and not(following-sibling::ORM_O01.LST.RXO[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) 
				and not(following-sibling::ORM_O09.LST.RXO[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) 
				and not(following-sibling::OMP_O09.LST.RXO[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])])">
			<xsl:if test="$msgAction='OrderEnter' or $msgAction='OrderReplace'">
				<WARNING><xsl:value-of select="concat('No Restriction, Supplement, Enteral or Service for Order with OrderID:  ',ORC.2/EI.1,'.  Order not processed.')"/></WARNING>
			</xsl:if>
		</xsl:if>
	</xsl:for-each>
	<!-- Var for Cancel Orders, contains 'Y' if an ORC was sent with no ODS segments -->
	<xsl:variable name="ORCwNoODS">
		<xsl:for-each select="/ORM_O01/*/ORC">
			<xsl:variable name="ORC-ID" select="generate-id()"/>
			<xsl:if test="not(following-sibling::ORM_O01.LST.ODS[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) and $msgAction='OrderCancel'">
				<xsl:value-of select="'Y'"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<!-- Var for Cancel Orders, contains 'Y' if an ORC was sent with no ODT segments -->
	<xsl:variable name="ORCwNoODT">
		<xsl:for-each select="/ORM_O01/*/ORC">
			<xsl:variable name="ORC-ID" select="generate-id()"/>
			<xsl:if test="not(following-sibling::ORM_O01.LST.ODT[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) and $msgAction='OrderCancel'">
				<xsl:value-of select="'Y'"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<!-- Var for Cancel Orders, contains 'Y' if an ORC was sent with no Enteral-relevent segments -->
	<xsl:variable name="ORCwNoENT">
		<xsl:for-each select="$MSGList/*/ORC">
			<xsl:variable name="ORC-ID" select="generate-id()"/>
			<xsl:if test="not(following-sibling::ORM_O01.LST.RXO[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) 
					or not(following-sibling::ORM_O09.LST.RXO[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) 
					or not(following-sibling::OMP_O09.LST.RXO[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) 
					and $msgAction='OrderCancel'">
				<xsl:value-of select="'Y'"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:variable>
	<orderInfo>
		<!--Diet Order Handling-->
		<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS/ODS/ODS.1='D' or (contains(string($ORCwNoODS),'Y') and $msgAction='OrderCancel')">
			<DietOrderList>
            		<xsl:for-each select="/ORM_O01/*/ORC">
					<xsl:variable name="ORC-ID" select="generate-id()"/>
					<!-- Var for Cancel Orders, contains 'Y' if current ORC was sent with no ODS segments -->
					<xsl:variable name="CurrentORCwNoODS">
						<xsl:if test="not(following-sibling::ORM_O01.LST.ODS[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) and $msgAction='OrderCancel'">
							<xsl:value-of select="'Y'"/>
						</xsl:if>
					</xsl:variable>

					<!-- If an ODS does exist after the current ORC or if a Cancel Order with No ODS segment, then pull data from ORC and ODS -->
					<xsl:if test="not(contains(string($CurrentORCwNoODS),'Y')) or (contains(string($CurrentORCwNoODS),'Y') and $msgAction='OrderCancel')">		
						<!-- If an ODS contains 'D' or if a Cancel Order with No ODS segment, then pull data from ORC and ODS -->
						<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS/ODS.1='D' or (contains(string($CurrentORCwNoODS),'Y') and $msgAction='OrderCancel')">
							<DietOrderInfo>
								<!-- Extract Order Placer ID -->
								<!-- Extract order placer id if valued in ORC.2/EI.1 (default) -->
								<xsl:if test="ORC.2/EI.1">
									<OrderID><xsl:value-of select="ORC.2/EI.1"/></OrderID>
								</xsl:if>
								<!-- Extract Order Start DTM -->
								<!-- By assigning variables in the format below, if the date and time format received
									is invalid, then it will display they invalid warning, and it will continue to look
									for a propper date through out the message, and if a propper date is not found, it 
									will use the system date and time. -->
								<orderStartDTM>
									<!-- try for ORC.7/XCM.4 (default) first -->
									<xsl:variable name="ORC7Startdate">
										<xsl:choose>
											<!-- Check for data in primary default StartDTM location -->
											<xsl:when test="ORC.7/XCM.4">
												<xsl:value-of select="cbord:formatHL7DTM('Order StartDTM (ORC.7/XCM.4)',string(ORC.7/XCM.4))"/>
											</xsl:when>
											<!-- No data in ORC.7/XCM.4 (default) -->
											<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<xsl:choose>
										<!-- Data in ORC.7/XCM.4 is valued and valid: use it -->
										<xsl:when test="$ORC7Startdate != '' and not(contains($ORC7Startdate, 'Invalid'))">
											<xsl:value-of select="$ORC7Startdate"/>
										</xsl:when>
						      				<!-- Invalid or no data in ORC.7/XCM.4 (default), try for ORC.15 (default) second -->
										<xsl:otherwise>
											<!-- try for ORC.15 (default) second -->
											<xsl:variable name="ORC15date">
												<xsl:choose>
													<!-- Check for data in secondary StartDTM location -->
													<xsl:when test="ORC.15">
														<xsl:value-of select="cbord:formatHL7DTM('Order StartDTM (ORC.15)',string(ORC.15))"/>
													</xsl:when>
													<!-- Invalid or no data in ORC.15 (default) -->											
													<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
												</xsl:choose>
											</xsl:variable>
											<xsl:choose>
												<!-- Data in ORC.15 is valued and valid: use it -->
												<xsl:when test="$ORC15date!='' and  not(contains($ORC15date, 'Invalid')) and contains($ORC7Startdate, 'Invalid')">
													<Start><xsl:value-of select="$ORC15date"/></Start>
													<WARNING><xsl:value-of select="$ORC7Startdate"/></WARNING>
												</xsl:when>
												<xsl:when test="$ORC15date!='' and  not(contains($ORC15date, 'Invalid'))">
													<xsl:value-of select="$ORC15date"/>
												</xsl:when>
												<!-- Give up, use system DTM -->
												<xsl:otherwise>
													<xsl:if test="not(contains($ORC7Startdate, 'Invalid')) and not(contains($ORC15date, 'Invalid'))">
														<xsl:value-of select="cbord:getNow()"/>
													</xsl:if>
													<xsl:if test="contains($ORC7Startdate, 'Invalid')">
														<Start><xsl:value-of select="cbord:getNow()"/></Start>
														<WARNING><xsl:value-of select="$ORC7Startdate"/></WARNING>
													</xsl:if>
													<xsl:if test="contains($ORC15date, 'Invalid')">
														<Start><xsl:value-of select="cbord:getNow()"/></Start>
														<WARNING><xsl:value-of select="$ORC15date"/></WARNING>
													</xsl:if>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:otherwise>
									</xsl:choose>
								</orderStartDTM>

								<!-- Extract Order End DTM -->
								<!-- By assigning variables in the format below, if the date and time format received
									is invalid, then it will display the invalid warning, if it is a CA Order
									transaction, it will use the system DTM, otherwise tags will be empty. -->
								<xsl:variable name="ORC7Enddate">
									<xsl:choose>
										<xsl:when test="ORC.7/XCM.5">
											<xsl:value-of select="cbord:formatHL7DTM('Order EndDTM (ORC.7/XCM.5)',string(ORC.7/XCM.5))"/>
										</xsl:when>
										<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
									</xsl:choose>
								</xsl:variable>

								<xsl:choose>
									<!-- When ORC.7/XCM.5 (default) is valued and propperly formatted, use it. -->
									<xsl:when test="$ORC7Enddate != '' and not(contains($ORC7Enddate, 'Invalid'))">
										<EndDTM><xsl:value-of select="$ORC7Enddate"/></EndDTM>
									</xsl:when>
									<!-- When ORC.7/XCM.5 (default) is not valued or invalid and msg type is Order Cancel use system DTM. -->
									<xsl:when test="$msgAction='OrderCancel'">
										<EndDTM>
											<xsl:choose>
												<xsl:when test="contains($ORC7Enddate, 'Invalid')">
													<End><xsl:value-of select="cbord:getNow()"/></End>
													<WARNING><xsl:value-of select="$ORC7Enddate"/></WARNING>
												</xsl:when>
												<xsl:otherwise><xsl:value-of select="cbord:getNow()"/></xsl:otherwise>
											</xsl:choose>
										</EndDTM>
									</xsl:when>
									<!-- Otherwise do check for invalid ORC7 end DTM to display WARNING -->
									<xsl:otherwise>
										<xsl:if test="contains($ORC7Enddate, 'Invalid')">
											<EndDTM>
												<WARNING><xsl:value-of select="$ORC7Enddate"/></WARNING>
											</EndDTM>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>

								<!-- Extract Order Confirmed By -->
								<xsl:choose>
									<!-- ORC.10 (default) Extract when data is valued. -->
									<xsl:when test="ORC.10/XCN.1">
										<ConfirmedBy><xsl:value-of select="ORC.10/XCN.1"/></ConfirmedBy>
									</xsl:when>
									<!-- No data in ORC.10 (default) Extract from ORC.11 (default) when data is valued. -->
									<xsl:when test="ORC.11/XCN.1">
										<ConfirmedBy><xsl:value-of select="ORC.11/XCN.1"/></ConfirmedBy>
									</xsl:when>
									<xsl:when test="ORC.12/XCN.1">
										<!-- No data in ORC.11 (default) Extract from ORC.12 (default) when data is valued. -->
										<ConfirmedBy><xsl:value-of select="ORC.12/XCN.1"/></ConfirmedBy>
									</xsl:when>
									<!-- No data valued, do nothing. -->
									<xsl:otherwise></xsl:otherwise>
								</xsl:choose>
	
								<!-- Extract Order Restrictions -->
								<!-- Extract order info for this ORC Group -->
								<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='D']">
									<CodedRestrictionList>
										<xsl:for-each select="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='D']">
											<xsl:if test="ODS.3.LST/ODS.3">	
												<xsl:for-each select="ODS.3.LST/ODS.3">
													<CodedRestriction><xsl:value-of select="CE.1"/></CodedRestriction>
												</xsl:for-each>
											</xsl:if>
										</xsl:for-each>
									</CodedRestrictionList>
								</xsl:if>
								<!-- Extract Order Note -->
								<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='D']">
									<OrderNote>
										<xsl:for-each select="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='D']">
											<xsl:if test="ODS.3.LST/ODS.3">	
												<!-- Used to place a unique ID on the ODS currently being processed -->	
												<xsl:variable name="ODS-ID" select="generate-id()"/>
												<!-- Use Id's to group all diet notes -->
												<xsl:if test="following-sibling::ORM_O01.LST.NTE[generate-id(preceding-sibling::ODS[1]) = $ODS-ID]">
													<xsl:for-each select="following-sibling::ORM_O01.LST.NTE[generate-id(preceding-sibling::ODS[1]) = $ODS-ID]/NTE/NTE.3.LST/NTE.3">
														<Note><xsl:value-of select="."/></Note>
													</xsl:for-each>
												</xsl:if>
											</xsl:if>
										</xsl:for-each>
									</OrderNote>
								</xsl:if>
							</DietOrderInfo>
						</xsl:if>
					</xsl:if>
	           	</xsl:for-each>
			</DietOrderList>
		</xsl:if>

		<!--Supplement Order Handling-->
		<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS/ODS/ODS.1='S' or (contains(string($ORCwNoODS),'Y') and $msgAction='OrderCancel')">
			<SupplementOrderList>
				<xsl:for-each select="/ORM_O01/*/ORC">
					<xsl:variable name="ORC-ID" select="generate-id()"/>
					<!-- Var for Cancel Orders, contains 'Y' if current ORC was sent with no ODS segments -->
					<xsl:variable name="CurrentORCwNoODS">
						<xsl:if test="not(following-sibling::ORM_O01.LST.ODS[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])]) and $msgAction='OrderCancel'">
							<xsl:value-of select="'Y'"/>
						</xsl:if>
					</xsl:variable>
	
					<xsl:if test="following-sibling::ORM_O01.LST.ODS[1][$ORC-ID=generate-id(preceding-sibling::ORC[1])] or (contains(string($CurrentORCwNoODS),'Y') and $msgAction='OrderCancel')">		
						<!-- Used to determine which ORC/ODS grouping is being processed-->
						<xsl:variable name="pos" select="position()"/>
						<!-- When grouping meals, this var ensures that we only use the ODS's that pertain to the ORC being processed -->
						<xsl:variable name="ODSListID" select="generate-id(../ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])])"/>
						<!-- Group all ODS's where meals are the same -->
						<xsl:variable name="unique-meal" select="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]
														 /ODS[ODS.1='S'][generate-id(.)=generate-id(key('meal-key',concat($ODSListID,ODS.2.LST)))]"/>
						<!-- Group any ODS's where no meals are specified.  Will later assign to ALL Meals -->
						<xsl:variable name="unique-nomeal" select="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]
														 /ODS[ODS.1='S'][not(ODS.2.LST)][1]"/>
			
						<xsl:for-each select="$unique-meal | $unique-nomeal">
							<xsl:call-template name="extractSuppInfo">
								<xsl:with-param name="pos" select="$pos"/>
								<xsl:with-param name="ORC-ID" select="$ORC-ID"/>
							</xsl:call-template>
						</xsl:for-each>
						<!-- Extract supp info for Cancels that don't contain ODS's, done for reporting purposes -->
						<xsl:if test="contains(string($CurrentORCwNoODS),'Y') and $msgAction='OrderCancel'">
							<xsl:call-template name="extractSuppInfo">
								<xsl:with-param name="pos" select="$pos"/>
								<xsl:with-param name="ORC-ID" select="$ORC-ID"/>
								<xsl:with-param name="CurrentORCwNoODS" select="$CurrentORCwNoODS"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:if>
				</xsl:for-each>
		    </SupplementOrderList>
		</xsl:if>

		<!--Dietary Services Message Handling -->		
		<!-- xsl:if test="'there is data in ODT.1 or ODT.3 and switch is on'"-->
		<xsl:if test="not(/ORM_O01/*/ORM_O01.LST.ODT/ODT/ODT.1='' and /ORM_O01/*/ORM_O01.LST.ODT/ODT/ODT.3='') or (contains(string($ORCwNoODT),'Y') and $msgAction='OrderCancel')">
			<xsl:if test="$useDietaryService='YES'">
				<ServiceOrderList>
					<xsl:for-each select="/ORM_O01/*/ORC">
					    <xsl:apply-templates select="." mode="serviceOrder"/>
					</xsl:for-each>
				</ServiceOrderList>
			</xsl:if>			
		</xsl:if>			

		<!--Enteral Message Handling -->
		<xsl:if test="(/ORM_O01/*/ORM_O01.LST.RXO/RXO 
				or /ORM_O09/*/ORM_O09.LST.RXO/RXO 
				or /OMP_O09/*/OMP_O09.LST.RXO/RXO) 
				or (contains(string($ORCwNoENT),'Y') 
				and $msgAction='OrderCancel')">
			<xsl:if test="$useEnteralOrder='YES'">
				<EnteralOrderList>
					<xsl:for-each select="$MSGList/*/ORC">
					    <xsl:apply-templates select="." mode="enteralOrder"/>
					</xsl:for-each>
				</EnteralOrderList>
			</xsl:if>			
		</xsl:if>			
	</orderInfo>
</xsl:template>

<xsl:template name="extractSuppInfo">
	<xsl:param name="pos"/>
	<xsl:param name="ORC-ID"/>
	<xsl:param name="CurrentORCwNoODS"/>

    	<SupplementOrderInfo>
		<!-- Extract Order Placer ID -->
		<!-- Extract order placer id if valued in ORC.2/EI.1 (default) -->
		<xsl:if test="/ORM_O01/*/ORC[position()=$pos]/ORC.2/EI.1">
			<OrderID><xsl:value-of select="/ORM_O01/*/ORC[position()=$pos]/ORC.2/EI.1"/></OrderID>
		</xsl:if>


		<!-- Extract Supplement Order Start DTM -->
		<!-- By assigning variables in the format below, if the date and time format received
		     is invalid, then it will display they invalid warning, and it will continue to look
		     for a propper date through out the message, and if a propper date is not found, it 
		     will use the system date and time. -->
		<orderStartDTM>
			<!-- try for ORC.7/XCM.4 (default) first -->
			<xsl:variable name="ORC7Startdate">
				<xsl:choose>
					<!-- Check for data in primary default StartDTM location -->
					<xsl:when test="/ORM_O01/*/ORC[position()=$pos]/ORC.7/XCM.4">
						<xsl:value-of select="cbord:formatHL7DTM('Order StartDTM (ORC.7/XCM.4)',string(/ORM_O01/*/ORC[position()=$pos]/ORC.7/XCM.4))"/>
					</xsl:when>
					<!-- No data in ORC.7/XCM.4 (default) -->
					<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:choose>
				<!-- Data in ORC.7/XCM.4 is valued and valid: use it -->
				<xsl:when test="$ORC7Startdate != '' and not(contains($ORC7Startdate, 'Invalid'))">
					<xsl:value-of select="$ORC7Startdate"/>
				</xsl:when>
			      <!-- Invalid or no data in ORC.7/XCM.4 (default), try for ORC.15 (default) second -->
				<xsl:otherwise>
					<!-- try for ORC.15 (default) second -->
					<xsl:variable name="ORC15date">
						<xsl:choose>
							<!-- Check for data in secondary StartDTM location -->
							<xsl:when test="/ORM_O01/*/ORC[position()=$pos]/ORC.15">
								<xsl:value-of select="cbord:formatHL7DTM('Order StartDTM (ORC.15)',string(/ORM_O01/*/ORC[position()=$pos]/ORC.15))"/>
							</xsl:when>
							<!-- Invalid or no data in ORC.15 (default) -->											
							<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:choose>
						<!-- Data in ORC.15 is valued and valid: use it -->
						<xsl:when test="$ORC15date!='' and  not(contains($ORC15date, 'Invalid')) and contains($ORC7Startdate, 'Invalid')">
							<Start><xsl:value-of select="$ORC15date"/></Start>
							<WARNING><xsl:value-of select="$ORC7Startdate"/></WARNING>
						</xsl:when>
						<xsl:when test="$ORC15date!='' and  not(contains($ORC15date, 'Invalid'))">
							<xsl:value-of select="$ORC15date"/>
						</xsl:when>
						<!-- Give up, use system DTM -->
						<xsl:otherwise>
							<xsl:if test="not(contains($ORC7Startdate, 'Invalid')) and not(contains($ORC15date, 'Invalid'))">
								<xsl:value-of select="cbord:getNow()"/>
							</xsl:if>
							<xsl:if test="contains($ORC7Startdate, 'Invalid')">
								<Start><xsl:value-of select="cbord:getNow()"/></Start>
								<WARNING><xsl:value-of select="$ORC7Startdate"/></WARNING>
							</xsl:if>
							<xsl:if test="contains($ORC15date, 'Invalid')">
								<Start><xsl:value-of select="cbord:getNow()"/></Start>
								<WARNING><xsl:value-of select="$ORC15date"/></WARNING>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</orderStartDTM>



		<!-- Extract Supplement Order End DTM -->
		<!-- By assigning variables in the format below, if the date and time format received
			is invalid, then it will display the invalid warning, if it is a CA Order
			transaction, it will use the system DTM, otherwise tags will be empty. -->
		<xsl:variable name="ORC7Enddate">
			<xsl:choose>
				<xsl:when test="/ORM_O01/*/ORC[position()=$pos]/ORC.7/XCM.5">
					<xsl:value-of select="cbord:formatHL7DTM('Order EndDTM (ORC.7/XCM.5)',string(/ORM_O01/*/ORC[position()=$pos]/ORC.7/XCM.5))"/>
				</xsl:when>
				<xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<!-- When ORC.7/XCM.5 (default) is valued and propperly formatted, use it. -->
			<xsl:when test="$ORC7Enddate != '' and not(contains($ORC7Enddate, 'Invalid'))">
				<EndDTM><xsl:value-of select="$ORC7Enddate"/></EndDTM>
			</xsl:when>
			<!-- When ORC.7/XCM.5 (default) is not valued or invalid and msg type is Order Cancel use system DTM. -->
			<xsl:when test="$msgAction='OrderCancel'">
				<EndDTM>
					<xsl:choose>
						<xsl:when test="contains($ORC7Enddate, 'Invalid')">
							<End><xsl:value-of select="cbord:getNow()"/></End>
							<WARNING><xsl:value-of select="$ORC7Enddate"/></WARNING>
						</xsl:when>
						<xsl:otherwise><xsl:value-of select="cbord:getNow()"/></xsl:otherwise>
					</xsl:choose>
				</EndDTM>
			</xsl:when>
			<!-- Otherwise do check for invalid ORC7 end DTM to display WARNING -->
			<xsl:otherwise>
				<xsl:if test="contains($ORC7Enddate, 'Invalid')">
					<EndDTM>
						<WARNING><xsl:value-of select="$ORC7Enddate"/></WARNING>
					</EndDTM>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>


		<!-- Extract Supplement Meal Codes -->
		<xsl:choose>
			<xsl:when test="ODS.2.LST/ODS.2/CE.1">
				<CodedMealRuleList>
					<xsl:for-each select="ODS.2.LST/ODS.2/CE.1">
						<CodedMealRule><xsl:value-of select="."/></CodedMealRule>
					</xsl:for-each>
				</CodedMealRuleList>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="not(contains(string($CurrentORCwNoODS),'Y') and $msgAction='OrderCancel')">
					<CodedMealRuleList>
						<!-- Used in the Cleanup script so can assign to All Meals when no meals specified -->
						<CodedMealRuleNotSpecified></CodedMealRuleNotSpecified>
					</CodedMealRuleList>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
		
		<!-- Extract Supplements -->
		<xsl:choose>
			<!-- For all ODS's within the ORC being processed; group supplements that have common meal periods -->
			<xsl:when test="ODS.2.LST/ODS.2/CE.1">
				<xsl:variable name="meal-group" select="."/>

				<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='S']">
					<CodedSupplementList>
						<xsl:for-each select="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='S'][ODS.2.LST = msxsl:node-set($meal-group)//ODS.2.LST]">
							<xsl:if test="ODS.3.LST/ODS.3">
								<xsl:for-each select="ODS.3.LST/ODS.3">
									<CodedSupplementInfo>
										<CodedItemUofM><xsl:value-of select="CE.1"/></CodedItemUofM>
										<xsl:choose>
											<xsl:when test="/ORM_O01/*/ORC[position()=$pos]/ORC.7/XCM.1">	
												<Quantity><xsl:value-of select="/ORM_O01/*/ORC[position()=$pos]/ORC.7/XCM.1"/></Quantity>
											</xsl:when>
											<xsl:otherwise>
												<Quantity><xsl:value-of select="1"/></Quantity>
											</xsl:otherwise>
										</xsl:choose>
									</CodedSupplementInfo>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</CodedSupplementList>
				</xsl:if>
			</xsl:when>
			<!-- For all ODS's within the ORC being processed; group all supplements that have no meals specified -->
			<xsl:otherwise>
				<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='S']">
					<CodedSupplementList>
						<xsl:for-each select="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='S'][not(ODS.2.LST)]">
							<xsl:if test="ODS.3.LST/ODS.3">
								<xsl:for-each select="ODS.3.LST/ODS.3">
									<CodedSupplementInfo>
										<CodedItemUofM><xsl:value-of select="CE.1"/></CodedItemUofM>
										<xsl:choose>
											<xsl:when test="/ORM_O01/*/ORC[position()=$pos]/ORC.7/XCM.1">	
												<Quantity><xsl:value-of select="/ORM_O01/*/ORC[position()=$pos]/ORC.7/XCM.1"/></Quantity>
											</xsl:when>
											<xsl:otherwise>
												<Quantity><xsl:value-of select="1"/></Quantity>
											</xsl:otherwise>
										</xsl:choose>
									</CodedSupplementInfo>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</CodedSupplementList>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>

		<!-- Extract Supplement Notes -->
		<xsl:choose>
			<!-- For all NTE's within the ORC being processed; group Notes that have common meal periods -->
			<xsl:when test="ODS.2.LST/ODS.2/CE.1">
				<xsl:variable name="meal-group" select="."/>
				<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='S']">
					<OrderNote>
						<xsl:for-each select="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='S'][ODS.2.LST = msxsl:node-set($meal-group)//ODS.2.LST]">
							<xsl:if test="ODS.3.LST/ODS.3">										
								<xsl:variable name="ODS-ID" select="generate-id()"/>
								<xsl:if test="following-sibling::ORM_O01.LST.NTE[generate-id(preceding-sibling::ODS[1]) = $ODS-ID]">
									<xsl:for-each select="following-sibling::ORM_O01.LST.NTE[generate-id(preceding-sibling::ODS[1]) = $ODS-ID]/NTE/NTE.3.LST/NTE.3">
										<Note><xsl:value-of select="."/></Note>
									</xsl:for-each>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
					</OrderNote>
				</xsl:if>
			</xsl:when>
			<!-- For all NTE's within the ORC being processed; group Notes that have no meals specified -->
			<xsl:otherwise>
				<xsl:if test="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='S']">
					<OrderNote>
						<xsl:for-each select="/ORM_O01/*/ORM_O01.LST.ODS[$ORC-ID=generate-id(preceding-sibling::ORC[1])]/ODS[ODS.1='S'][not(ODS.2.LST)]">
							<xsl:if test="ODS.3.LST/ODS.3">										
								<xsl:variable name="ODS-ID" select="generate-id()"/>
								<xsl:if test="following-sibling::ORM_O01.LST.NTE[generate-id(preceding-sibling::ODS[1]) = $ODS-ID]">
									<xsl:for-each select="following-sibling::ORM_O01.LST.NTE[generate-id(preceding-sibling::ODS[1]) = $ODS-ID]/NTE/NTE.3.LST/NTE.3">
										<Note><xsl:value-of select="."/></Note>
									</xsl:for-each>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
					</OrderNote>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>				
 	</SupplementOrderInfo>
</xsl:template>

<!-- Extract Order Dates and Times -->
<!-- Takes the two values given from HL7 message locations and checks 
	them for validity.  Failing that the system date/time is used.  
	hl7Where is fed to cbord:formathl7DTM as the location if there's 
	an error. This works for start and end times and can test up to 
	three different time locations.  -->
<xsl:template name="HL7DateTime">
	<!-- Time values -->
	<xsl:param name="Time1"/>
	<xsl:param name="Time2"/>
	<xsl:param name="Time3"/>
	<!-- Time origin location -->
	<xsl:param name="hl7Where1"/>
	<xsl:param name="hl7Where2"/>
	<xsl:param name="hl7Where3"/>
	<!-- Don't pass the system date/time if "Y" -->
	<xsl:param name="noSysDTM"/>
	<!-- Don't return warnings for no input if "N" -->
	<xsl:param name="displayWarningsOnAllBlank"/>
	<!-- Supress the time component if "Y" -->
	<xsl:param name="supressTimeReturn"/>
	
	<!-- send input times thru cbord:formatHT7DTM -->
	<!-- hl7Where# is tested b/c Time# may be null -->
	<!-- if hl7Where# is null then that # isn't used -->
	<xsl:variable name="timeOne">
		<xsl:if test="$hl7Where1">
			<xsl:value-of select="cbord:formatHL7DTM($hl7Where1,string($Time1))"/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="timeTwo">
		<xsl:if test="$hl7Where2">
			<xsl:value-of select="cbord:formatHL7DTM($hl7Where2,string($Time2))"/>
		</xsl:if>
	</xsl:variable>
	<xsl:variable name="timeThree">
		<xsl:if test="$hl7Where3">
			<xsl:value-of select="cbord:formatHL7DTM($hl7Where3,string($Time3))"/>
		</xsl:if>
	</xsl:variable>

	<xsl:choose>
		<!-- Check the first time -->
		<xsl:when test="not(contains($timeOne, 'Invalid')) and $Time1">
			<returnTime><xsl:value-of select="$timeOne"/></returnTime>
		</xsl:when>
		<!-- failing that check the second time -->
		<xsl:when test="not(contains($timeTwo, 'Invalid')) and (contains($timeOne, 'Invalid')) and $Time2">
			<returnTime><xsl:value-of select="$timeTwo"/></returnTime>
			<WARNING><xsl:value-of select="$timeOne"/></WARNING>
		</xsl:when>
		<!-- failing that check the third time  -->
		<xsl:when test="not(contains($timeThree, 'Invalid')) and (contains($timeOne, 'Invalid')) and (contains($timeTwo, 'Invalid')) and $Time3">
			<returnTime><xsl:value-of select="$timeThree"/></returnTime>
			<WARNING><xsl:value-of select="$timeOne"/></WARNING>
			<WARNING><xsl:value-of select="$timeTwo"/></WARNING>
		</xsl:when>
		<!-- if all else fails, use System DTM...  -->
		<xsl:otherwise>
			<xsl:if test="not($displayWarningsOnAllBlank = 'N')">
				<xsl:if test="contains($timeOne, 'Invalid')">
					<WARNING><xsl:value-of select="$timeOne"/></WARNING>
				</xsl:if>
				<xsl:if test="contains($timeTwo, 'Invalid')">
					<WARNING><xsl:value-of select="$timeTwo"/></WARNING>
				</xsl:if>
				<xsl:if test="contains($timeThree, 'Invalid')">
					<WARNING><xsl:value-of select="$timeThree"/></WARNING>
				</xsl:if>
			</xsl:if>
			<!-- ...unless instructed not to  -->
			<xsl:if test="not($noSysDTM = 'Y')">
				<returnTime><xsl:value-of select="cbord:getNow()"/></returnTime>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>