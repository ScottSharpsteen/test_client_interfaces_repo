<?xml version="1.0"?>
<!-- ADT XSLT for transforming HL7 messages. PVCS version: $Revision:   1.0  $ -->
<!-- this is for inclusion in the main xlate script for ADT messages -->
<!-- PVCS version: $Revision:   1.0  $ -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:translator="urn:translatorObject"
                version="1.0">
                
<!-- enteralOrder template called by extractOrderInfo. -->
<xsl:template match="ORC" mode="enteralOrder"/>


</xsl:stylesheet>