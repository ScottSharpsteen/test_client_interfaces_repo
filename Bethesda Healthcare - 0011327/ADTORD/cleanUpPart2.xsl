<?xml version="1.0"?>
<!-- Generic stylesheet for transforming ADT messages. PVCS version: $Revision:   1.35  $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:cbordRM="http://cbord.com/xsltnsRM"
                xmlns:translator="urn:translatorObject"
                version="1.0">


<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<xsl:template match="/">
	<CCIFADTList>
		<xsl:apply-templates select="//WARNING"/>
		<xsl:apply-templates select="/*/CCIFADT"/>

	</CCIFADTList>
</xsl:template>


<xsl:template match="WARNING"/>

<xsl:template match="CCIFADT">
	<CCIFADT>
		<xsl:apply-templates select="*|@*|text()"/>
		<xsl:if test=".//WARNING">
			<xsl:call-template name="warningFormat"/>
		</xsl:if>
	</CCIFADT>
</xsl:template>
			
<xsl:template name="warningFormat">
	<xlateLog>
		<logComponentInterface>
			<NSSlogInfo>
				<MsgId/>
				<ResultCode/>
					<Action><xsl:value-of select="./operation/@name"/></Action>
				<ProcessID><xsl:value-of select="'20000'"/></ProcessID>
				<EntryList>
					<xsl:apply-templates select="//WARNING" mode="WARNINGS"/>
				</EntryList>
			</NSSlogInfo>
		</logComponentInterface>
	</xlateLog>						
</xsl:template>

<xsl:template match="WARNING" mode="WARNINGS">
	<WarningMsg><xsl:value-of select="."/></WarningMsg>
</xsl:template>

<xsl:template match="*|@*|text()">
	<xsl:copy><xsl:apply-templates select="*|@*|text()"/></xsl:copy>
</xsl:template>

</xsl:stylesheet>
