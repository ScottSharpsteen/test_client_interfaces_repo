<?xml version="1.0" encoding="iso-8859-1"?>
<!-- stylesheet for Date Conversion Utility Templates: version $Revision: #1 $-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template name="dd-mmm-yyyy2yyyy-mm-dd">
  <xsl:param name="date"/>
  <xsl:variable name="cent"  select="substring($date,8,2)"/>
  <xsl:variable name="year"  select="substring($date,10,2)"/>
  <xsl:variable name="monthname" select="substring($date,4,3)"/>
  <xsl:variable name="day"   select="substring($date,1,2)"/>
  <xsl:variable name="month">
    <xsl:call-template name="DCName-to-Month">
      <xsl:with-param name='name' select='$monthname'/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:value-of select="concat($cent,$year,'-',$month,'-',$day)"/>
</xsl:template>

<xsl:template name="dd-mmm-yy2yyyy-mm-dd">
  <xsl:param name="date"/>
  <xsl:variable name="year"  select="substring($date,8,2)"/>
  <xsl:variable name="monthname" select="substring($date,4,3)"/>
  <xsl:variable name="day"   select="substring($date,1,2)"/>
  <xsl:variable name="month">
    <xsl:call-template name="DCName-to-Month">
      <xsl:with-param name='name' select='$monthname'/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:value-of select="concat('20',$year,'-',$month,'-',$day)"/>
</xsl:template>

<xsl:template name="mm-dd-yy2yyyy-mm-dd">
  <xsl:param name="date"/>
  <xsl:variable name="year"  select="substring($date,7,2)"/>
  <xsl:variable name="month" select="substring($date,1,2)"/>
  <xsl:variable name="day"   select="substring($date,3,2)"/>
 <!-- <xsl:variable name="month">
    <xsl:call-template name="DCName-to-Month">
      <xsl:with-param name='name' select='$monthname'/>
    </xsl:call-template>
  </xsl:variable>
-->  
  <xsl:value-of select="concat('20',$year,'-',$month,'-',$day)"/>
</xsl:template>

<xsl:template name="yyyy-mm-dd2dd-mmm-yy">
  <xsl:param name="date"/>
  <xsl:variable name="cent"  select="substring($date,1,2)"/>
  <xsl:variable name="year"  select="substring($date,3,2)"/>
  <xsl:variable name="month" select="substring($date,6,2)"/>
  <xsl:variable name="day"   select="substring($date,9,2)"/>
  <xsl:variable name="monthname">
    <xsl:call-template name="DCMonth-to-Name">
       <xsl:with-param name='month' select='$month'/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:value-of select="concat($day,'-',$monthname,'-',$year)"/>
</xsl:template>

<xsl:template name="yyyy-mm-dd2yyyymmdd">
  <xsl:param name="date"/>
  <xsl:variable name="cent"  select="substring($date,1,2)"/>
  <xsl:variable name="year"  select="substring($date,3,2)"/>
  <xsl:variable name="month" select="substring($date,6,2)"/>
  <xsl:variable name="day"   select="substring($date,9,2)"/>
  <xsl:value-of select="concat($cent,$year,$month,$day)"/>
</xsl:template>

<xsl:template name="yyyy-mm-dd2mmddyyyy">
  <xsl:param name="date"/>
  <xsl:variable name="cent"  select="substring($date,1,2)"/>
  <xsl:variable name="year"  select="substring($date,3,2)"/>
  <xsl:variable name="month" select="substring($date,6,2)"/>
  <xsl:variable name="day"   select="substring($date,9,2)"/>
  <xsl:value-of select="concat($month,$day,$cent,$year)"/>
</xsl:template>



<!-- ................. Fiscal Year Conversions ..................... -->

<xsl:template name="dd-mmm-yy2yyyy">
  <xsl:param name="date"/>
  <xsl:param name="fystartmonth"/>

  <xsl:variable name="datestandard">
	<xsl:call-template name="dd-mmm-yy2yyyy-mm-dd">
		<xsl:with-param name="date" select="$date"/>
	</xsl:call-template>
  </xsl:variable>
  <xsl:call-template name="yyyy-mm-dd2yyyy">
	<xsl:with-param name="date" select="$datestandard"/>
	<xsl:with-param name="fystartmonth" select="$fystartmonth"/>
  </xsl:call-template>
</xsl:template>

<xsl:template name="yyyy-mm-dd2yyyy">
  <xsl:param name="date"/>
  <xsl:param name="fystartmonth"/> 
  <xsl:variable name="year"  select="substring($date,1,4)"/>
  <xsl:variable name="month" select="number(substring($date,6,2))"/>
  <xsl:variable name="day"   select="substring($date,9,2)"/>
  <xsl:variable name="yearname">
     <xsl:choose>
	  <xsl:when test="$month >= $fystartmonth and $fystartmonth != 1">
		<xsl:value-of select="$year+1"/>
	  </xsl:when>
	  <xsl:otherwise><xsl:value-of select="$year"/></xsl:otherwise>
     </xsl:choose>   
  </xsl:variable>
  <xsl:value-of select="$yearname"/>
</xsl:template>

<!-- ................................................................. -->

<xsl:template name="DCMonth-to-Name">
  <xsl:param name="month"/>
   <xsl:choose>
        <xsl:when test="$month='01'">JAN</xsl:when>
        <xsl:when test="$month='02'">FEB</xsl:when>
        <xsl:when test="$month='03'">MAR</xsl:when>
        <xsl:when test="$month='04'">APR</xsl:when>
        <xsl:when test="$month='05'">MAY</xsl:when>
        <xsl:when test="$month='06'">JUN</xsl:when>
        <xsl:when test="$month='07'">JUL</xsl:when>
        <xsl:when test="$month='08'">AUG</xsl:when>
        <xsl:when test="$month='09'">SEP</xsl:when>
        <xsl:when test="$month='10'">OCT</xsl:when>
        <xsl:when test="$month='11'">NOV</xsl:when>
        <xsl:when test="$month='12'">DEC</xsl:when>
	<xsl:otherwise>XXX</xsl:otherwise>
     </xsl:choose>
</xsl:template>
  
<xsl:template name="DCName-to-Month">
  <xsl:param name='name'/>
     <xsl:choose>
        <xsl:when test="$name='JAN'">01</xsl:when>
        <xsl:when test="$name='FEB'">02</xsl:when>
        <xsl:when test="$name='MAR'">03</xsl:when>
        <xsl:when test="$name='APR'">04</xsl:when>
        <xsl:when test="$name='MAY'">05</xsl:when>
        <xsl:when test="$name='JUN'">06</xsl:when>
        <xsl:when test="$name='JUL'">07</xsl:when>
        <xsl:when test="$name='AUG'">08</xsl:when>
        <xsl:when test="$name='SEP'">09</xsl:when>
        <xsl:when test="$name='OCT'">10</xsl:when>
        <xsl:when test="$name='NOV'">11</xsl:when>
        <xsl:when test="$name='DEC'">12</xsl:when>
	<xsl:otherwise>99</xsl:otherwise>
     </xsl:choose>
</xsl:template>

</xsl:stylesheet>

