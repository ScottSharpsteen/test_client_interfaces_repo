<?xml version="1.0" encoding="iso-8859-1"?>
<!--    stylesheet for creating Lawson Header rollup: version $Revision: #1 $
	Author GTT
	Changes:
	04/18/02	Create to rollup items by Header Info.
			Grab the Header[1] and copy matching items in item list.	

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		        version="1.0">


<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes" />



<xsl:template match="/">
<CCIFAPGL>

       <!-- Find all unique dates for the transactions -->	
       <xsl:variable name="dates" select="//Transaction/Header[not(TransactionDate = preceding::Header/TransactionDate)]/TransactionDate"/>

       <!--Find all unique transaction[2] values-->
       <xsl:variable name="Trans2" select="//Transaction/Header[not(TransactionNumberList/TransactionNumber[2]=preceding::Header/TransactionNumberList/TransactionNumber[2])]/TransactionNumberList/TransactionNumber[2]"/>

       <!--Find all unique transaction[3] values in the transactions-->
       <xsl:variable name="Trans3" select="//Transaction/Header[not(TransactionNumberList/TransactionNumber[3]=preceding::Header/TransactionNumberList/TransactionNumber[3])]/TransactionNumberList/TransactionNumber[3]"/>

       <!--Find all unique UnitId[2] in the transactions--> 
       <xsl:variable name="Unit" select="//Transaction/Header[not(UnitIdList/UnitId[2]=preceding::Header/UnitIdList/UnitId[2])]/UnitIdList/UnitId[2]"/>

       <!--Find all unique OutgoingUnitId[5] in transactions--> 
       <xsl:variable name="OutUnit" select="//Transaction/Header[not(OutgoingUnitIdList/OutgoingUnitId[5]=preceding::Header/OutgoingUnitIdList/OutgoingUnitId[5])]/OutgoingUnitIdList/OutgoingUnitId[5]"/>


       <!--Create item records from the uniques keys above-->
         <xsl:for-each select="$dates">
           <xsl:variable name="date" select="."/>
           <xsl:for-each select="$Trans2">
              <xsl:variable name="trn2" select="."/>
                 <xsl:for-each select="$Trans3">
                     <xsl:variable name="trn3" select="."/>
                        <xsl:for-each select="$Unit">
                            <xsl:variable name="unit" select="."/>
                            <xsl:for-each select="$OutUnit">
                                <xsl:variable name="outUnit" select="."/>
                                <xsl:variable name="matches" select="//Transaction[Header/TransactionDate=$date][Header/TransactionNumberList/TransactionNumber[2]=$trn2][Header/TransactionNumberList/TransactionNumber[3]=$trn3][Header/UnitIdList/UnitId[2]=$unit][Header/OutgoingUnitIdList/OutgoingUnitId[5]=$outUnit]/ItemList/Item"/>
                                 <xsl:if test="$matches"> 
			            <xsl:call-template name="ProcessItems">
  				         <xsl:with-param name="header" select="//Transaction[Header/TransactionDate=$date][Header/TransactionNumberList/TransactionNumber[2]=$trn2][Header/TransactionNumberList/TransactionNumber[3]=$trn3][Header/UnitIdList/UnitId[2]=$unit][Header/OutgoingUnitIdList/OutgoingUnitId[5]=$outUnit]/Header"/>
  				         <xsl:with-param name="itemlist" select="//Transaction[Header/TransactionDate=$date][Header/TransactionNumberList/TransactionNumber[2]=$trn2][Header/TransactionNumberList/TransactionNumber[3]=$trn3][Header/UnitIdList/UnitId[2]=$unit][Header/OutgoingUnitIdList/OutgoingUnitId[5]=$outUnit]/ItemList"/>
			            </xsl:call-template>
                                 </xsl:if> 
                            </xsl:for-each>
                         </xsl:for-each>
                  </xsl:for-each>
             </xsl:for-each>
       </xsl:for-each> 
 </CCIFAPGL>
</xsl:template>



<!--
     Roll up Transactions by Header Info. 
-->
<xsl:template name="ProcessItems">
  <xsl:param name="header"/>
  <xsl:param name="itemlist"/>

  <xsl:if test="$itemlist">
   <Transaction>
     <xsl:copy-of select="$header[1]"/>
     <ItemList>
           <xsl:copy-of select="$itemlist/Item"/>
     </ItemList> 
    </Transaction>
  </xsl:if>
</xsl:template>


</xsl:stylesheet>

