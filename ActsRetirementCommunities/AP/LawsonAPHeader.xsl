<?xml version="1.0" encoding="iso-8859-1"?>
<!-- 	stylesheet for creating Client AP Interface messages: version $Revision: #1 $
	Changes:
	02/18/02	GTT	Added length constraints to meet Lawson Interface.
	04/11/02	GTT	Recode to use xpath mechanism of rollup
	05/24/02	CLC	Edited EF-CVI-BATCH-NUM to static value of '1', Added last 14 Lawson fields listed to output
	07/11/02	GTT	Edited EF-CVI-REC-STATUS and EF-CVI-POSTING-STATUS to static vale '1'.

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
		        version="1.0">

<!--xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes" /--> 
<xsl:output method="text" encoding="UTF-8" /> 
<xsl:include href="DateConversion.xsl"/> 


<!-- Global Variables (for convienience mostly) -->
<xsl:variable name="EndOfRec"><xsl:text>&#x0d;&#x0a;</xsl:text></xsl:variable>


<xsl:template match="/">
       <xsl:for-each select="//Transaction">

          <!--Create item records from the uniques keys above-->
          <xsl:variable name="itemRecords"> 
 	         <xsl:call-template name="ProcessItems">
		    <xsl:with-param name="header" select="./Header"/>
		    <xsl:with-param name="itemlist" select="./ItemList/Item"/>
                </xsl:call-template>
           </xsl:variable>
          <xsl:for-each select="msxsl:node-set($itemRecords)/outputRecord">
             <xsl:sort select="EF-CVI-INVOICE"/>
             <xsl:apply-templates select="."/>
          </xsl:for-each>
       </xsl:for-each>   
     
</xsl:template>

<!--
     Sum up the totals and create line item records. 
-->
<xsl:template name="ProcessItems">
  <xsl:param name="header"/>
  <xsl:param name="itemlist"/>
  
  <xsl:variable name="total">
	   <xsl:call-template name="sumPriceTimesQuantity">
  	      <xsl:with-param name="itemlist" select="$itemlist" /> 
  	      <xsl:with-param name="index" select="2" /> 
  	   </xsl:call-template>
  </xsl:variable>    
  <xsl:call-template name="outputXMLRecord">
		  <xsl:with-param name="header" select="$header"/>
                  <xsl:with-param name="amount" select="format-number($total div 100, '00000000000.00')"/>
   </xsl:call-template>

</xsl:template>

<!--
    Output record and move carriage return to the next line
-->
<xsl:template match="outputRecord">
  <xsl:apply-templates/>
  <xsl:value-of select="$EndOfRec"/>
</xsl:template> 
 
<!--
   XML recored for further processing/sorting
-->


<xsl:template name="outputXMLRecord">
  <xsl:param name="header"/>
  <xsl:param name="amount"/>

  <xsl:if test="$amount!=0">
     <outputRecord>
	<EF-CVI-COMPANY>
	   <xsl:value-of select="substring($header/FacilityIdList/FacilityId[2],1,4)"/>
           <xsl:text>,</xsl:text>
	</EF-CVI-COMPANY>

	<EF-CVI-VENDOR>
	     <xsl:value-of select="substring($header/OutgoingUnitIdList/OutgoingUnitId[5],1,9)"/>
	     <xsl:text>,</xsl:text>
        </EF-CVI-VENDOR>

	<EF-CVI-EDI-NBR>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-EDI-NBR>

	<EF-CVI-INVOICE>
	    <xsl:value-of select="substring($header/TransactionNumberList/TransactionNumber[3],1,22)"/>
            <xsl:text>,</xsl:text>
	</EF-CVI-INVOICE>

	<EF-CVI-SUFFIX>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-SUFFIX>

	<EF-CVI-BATCH-NUM>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-BATCH-NUM>

	<EF-CVI-VOUCHER-NBR>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-VOUCHER-NBR>

	<EF-CVI-AUTH-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-AUTH-CODE>


	<EF-CVI-PROC-LEVEL>
	    <xsl:value-of select="substring($header/UnitIdList/UnitId[2],1,5)"/>
	</EF-CVI-PROC-LEVEL>

	<EF-CVI-ACCR-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-ACCR-CODE>

	<EF-CVI-INVOICE-TYPE>
          <xsl:if test="$amount &lt;0"><xsl:value-of select="',C'"/></xsl:if> 
          <xsl:if test="$amount &gt;=0"><xsl:text>,</xsl:text></xsl:if> 

 	   <xsl:text>,</xsl:text>
        </EF-CVI-INVOICE-TYPE>

	<EF-CVI-OLD-VENDOR>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-OLD-VENDOR>

	<EF-CVI-INVOICE-CURRENCY>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-INVOICE-CURRENCY>
	<EF-CVI-INVOICE-DATE>
	  <xsl:call-template name="yyyy-mm-dd2mmddyyyy">
             <xsl:with-param name="date" select="$header/TransactionDate"/>
          </xsl:call-template>
	   <xsl:text>,</xsl:text>
	</EF-CVI-INVOICE-DATE>

	<EF-CVI-TAX-PNT-DATE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-TAX-PNT-DATE>

	<EF-CVI-PURCH-FR-LOC>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-PURCH-FR-LOC>

	<EF-CVI-PO-NUM>
	     <xsl:value-of select="substring($header/TransactionNumberList/TransactionNumber[2],1,7)"/>
             <xsl:text>,</xsl:text>
	</EF-CVI-PO-NUM>

	<EF-CVI-PO-RELEASE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-PO-RELEASE>

	<EF-CVI-PO-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-PO-CODE>

	<EF-CVI-AUTO-MATCH>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-AUTO-MATCH>

	<EF-CVI-DESCRIPTION>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-DESCRIPTION>


	<EF-CVI-BASE-INV-AMT>
           <xsl:if test="$amount &lt;0"/>
           <xsl:if test="$amount &gt;= 0"><xsl:text>0</xsl:text></xsl:if>
           <xsl:value-of select="$amount"/>
	   <xsl:text>,</xsl:text>
	</EF-CVI-BASE-INV-AMT>

	<EF-CVI-TRANS-INV-AMT>
           <xsl:if test="$amount &lt;0"/>
           <xsl:if test="$amount &gt;= 0"><xsl:text>0</xsl:text></xsl:if>
           <xsl:value-of select="$amount"/>
	   <xsl:text>,</xsl:text>
	</EF-CVI-TRANS-INV-AMT>

	<EF-CVI-TRAN-ALLOW-AMT>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-TRAN-ALLOW-AMT>

	<EF-CVI-TRAN-TXBL-AMT>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-TRAN-TXBL-AMT>

	<EF-CVI-TRAN-TAX-AMT>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-TRAN-TAX-AMT>

	<EF-CVI-TRAN-DISC-AMT>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-TRAN-DISC-AMT>

	<EF-CVI-ORIG-CNV-RATE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-ORIG-CNV-RATE>

	<EF-CVI-ANTICIPATION>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-ANTICIPATION>

	<EF-CVI-DISCOUNT-RT>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-DISCOUNT-RT>

	<EF-CVI-DISC-DATE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-DISC-DATE>

	<EF-CVI-DUE-DATE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-DUE-DATE>

	<EF-CVI-NBR-RECUR-PMT>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-NBR-RECUR-PMT>

	<EF-CVI-RECUR-FREQ>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-RECUR-FREQ>

	<EF-CVI-REMIT-TO-CODE>
	   <xsl:value-of select="substring($header/OutgoingUnitIdList/OutgoingUnitId[6],1,4)"/>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-REMIT-TO-CODE>

	<EF-CVI-CASH-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-CASH-CODE>

	<EF-CVI-SEP-CHK-FLAG>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-SEP-CHK-FLAG>

	<EF-CVI-PAY-IMM-FLAG>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-PAY-IMM-FLAG>

	<EF-CVI-ENCLOSURE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-ENCLOSURE>

	<EF-CVI-CURR-RECALC>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-CURR-RECALC>

	<EF-CVI-DISC-LOST-FLAG>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-DISC-LOST-FLAG>

	<EF-CVI-TAX-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-TAX-CODE>

	<EF-CVI-INCOME-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-INCOME-CODE>

	<EF-CVI-HLD-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-HLD-CODE>

	<EF-CVI-DISC-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-DISC-CODE>

	<EF-CVI-TERM-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-TERM-CODE>

	<EF-CVI-REC-STATUS>
 	   <xsl:text>1,</xsl:text>
        </EF-CVI-REC-STATUS>

	<EF-CVI-POSTING-STATUS>
 	   <xsl:text>1,</xsl:text>
        </EF-CVI-POSTING-STATUS>

	<EF-CVI-DISTRIB-DATE>
	    <!--xsl:value-of select="cbord:today()"/-->
 	    <xsl:text>,</xsl:text>
	</EF-CVI-DISTRIB-DATE>

	<EF-CVI-PAY-VENDOR>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-PAY-VENDOR>

	<!--EF-CVI-COMBINE-ASSERT>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-COMBINE-ASSERT-->

	<EF-CVI-TRANS-NBR>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-TRANS-NBR>

	<EF-CVI-BANK-INST-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-BANK-INST-CODE>

	<EF-CVI-CHECK-DATE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-CHECK-DATE>

	<EF-CVI-INVOICE-GROUP>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-INVOICE-GROUP>

	<EF-CVI-ONE-TIME-VEND>
	   <xsl:text>N,</xsl:text>
        </EF-CVI-ONE-TIME-VEND>  

	<EF-CVI-VENDOR-VNAME>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-VENDOR-VNAME>

	<EF-CVI-VENDOR-SNAME>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-VENDOR-SNAME>

	<EF-CVI-ADDR1>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-ADDR1>

	<EF-CVI-ADDR2>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-ADDR2>

	<EF-CVI-ADDR3>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-ADDR3>

	<EF-CVI-ADDR4>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-ADDR4>

	<EF-CVI-CITY-ADDR5>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-CITY-ADDR5>

	<EF-CVI-STATE-PROV>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-STATE-PROV>

	<EF-CVI-POSTAL-CODE>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-POSTAL-CODE>

	<EF-CVI-COUNTRY>
 	   <xsl:text>,</xsl:text>
        </EF-CVI-COUNTRY>

	<EF-CVI-REGION>
	   <xsl:text>,</xsl:text>
	</EF-CVI-REGION>

	<EF-CVI-INTRASTAT-FL>
	   <xsl:text>,</xsl:text>
	</EF-CVI-INTRASTAT-FL>

	<EF-CVI-SHIP-TO-CTRY>
	   <xsl:text>,</xsl:text>
	</EF-CVI-SHIP-TO-CTRY>

	<EF-CVI-SHIP-TO-REGION>
	   <xsl:text>,</xsl:text>
	</EF-CVI-SHIP-TO-REGION>

	<EF-CVI-UNLOADING-PORT>
	   <xsl:text>,</xsl:text>
	</EF-CVI-UNLOADING-PORT>

	<EF-CVI-NOTC>
	   <xsl:text>,</xsl:text>
	</EF-CVI-NOTC>

	<EF-CVI-DLVRY-TRMS>
	   <xsl:text>,</xsl:text>
	</EF-CVI-DLVRY-TRMS>

	<EF-CVI-TRANSPORT-MODE>
	   <xsl:text>,</xsl:text>
	</EF-CVI-TRANSPORT-MODE>

	<EF-CVI-DROP-SHIP-IND>
	   <xsl:text>,</xsl:text>
	</EF-CVI-DROP-SHIP-IND>

	<EF-CVI-STAT-PROC>
	   <xsl:text>,</xsl:text>
	</EF-CVI-STAT-PROC>

	<EF-CVI-BOE-CODE>
	   <xsl:text>,</xsl:text>
	</EF-CVI-BOE-CODE>

	<EF-CVI-AFFILIATION-CD>
	   <xsl:text>,</xsl:text>
	</EF-CVI-AFFILIATION-CD>

	<EF-CVI-CSA-CODE>
	   <xsl:text>,</xsl:text>
	</EF-CVI-CSA-CODE>

	<EF-CVI-SEGMENT-BLOCK>
	   <xsl:text>,</xsl:text>
	</EF-CVI-SEGMENT-BLOCK>

     </outputRecord>
  </xsl:if>
</xsl:template> 


<!--
     Helper function to create the sum over a list of price quantity pairs.
     Input is list of price quantity pairs and price quantity index
 -->

<xsl:template name="sumPriceTimesQuantity">
  <xsl:param name="itemlist"/>
  <xsl:param name="index"/>

  <xsl:choose>
    <xsl:when test="$itemlist">
       <xsl:variable name="first" select="$itemlist[1]"/>
       <xsl:variable name="others">
         <xsl:call-template name="sumPriceTimesQuantity">
           <xsl:with-param name="itemlist" select="$itemlist[position()!=1]"/>
           <xsl:with-param name="index" select="$index"/>
         </xsl:call-template>
       </xsl:variable>

       <xsl:value-of select="$others + 
                             cbord:multAndAddToSum(string($first/PriceQuantityList/PriceQuantity[$index]/Price), string($first/PriceQuantityList/PriceQuantity[$index]/Qty),2)"/>

    </xsl:when>

    <!-- empty list, return 0 -->
    <xsl:otherwise>
       <xsl:value-of select="0"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>



  <msxsl:script implements-prefix="cbord">
  <![CDATA[
   var seqNumber=0;

    // left pad a string to make the given size with the given character
    function padLeft(value,pad,size)
    {
       var padded = "" + value;
       while (padded.length < size)
       {
         padded = pad+padded;
       }
       return padded;
    }

    // right pad a string to make the given size with the given character
    function padRight(value,pad,size)
    {
       var padded = value;
       while (padded.length < size)
       {
         padded = padded+pad;
       }
       return padded;
    }

  function yesterday()
  {
    var MinMilli = 1000 * 60;
    var HrMilli = MinMilli * 60;
    var DyMilli = HrMilli * 24;

    var now = new Date();

    var dateInMilliSeconds = now.getTime();
    // go back one day
    dateInMilliSeconds = dateInMilliSeconds-DyMilli;
    now.setTime(dateInMilliSeconds);

    var tyear = now.getFullYear();
    var tmon = now.getMonth();
        tmon = tmon + 1;
    var tday = now.getDate();
    tday = new Number(tday);
    if (tday < 10)
       tday = "0"+tday;
    tmon = new Number(tmon);
    if(tmon <10)
      tmon = "0"+tmon;
     
    tdate = tyear + "/" + tmon + "/" + tday; 
    return tdate;
  
  }  
 


    function today()
    {

      var now = new Date();

      var tyear = now.getFullYear();
      var tmon = now.getMonth();
        tmon = tmon + 1;
      var tday = now.getDate();
      tday = new Number(tday);
      if (tday < 10)
        tday = "0"+tday;
       tmon = new Number(tmon);
      if(tmon <10)
      tmon = "0"+tmon;
     
      tdate = tmon +  tday + tyear; 
      return tdate;
    }

function multAndAddToSum(p,q,i)
{
   var sign = "";
   // remove the decimal point and create two integers
   var p2=p.split(".");
   var q2=q.split(".");

   var ind = p2[1].length + q2[1].length;
  
   var price=new Number(p2[0] + p2[1]);
   var qty=new Number(q2[0] + q2[1]);
       
   // perform the multiplication
   var prod=price * qty;

   //Determine the sign value
   if(prod < 0)
   { 
     prod = ""+prod;
     sign = prod.substring(0,1);
     prod = prod.substring(1, prod.length);
   }
   prod = new Number(prod);
  

   // divide by 10**ind-2 and round to get implied decimal of two places
   var prodAsMoney = Math.round(prod/Math.pow(10,ind - i));
   //Sum=Sum + new Number(sign + prodAsMoney);


   return ""+new Number(sign + prodAsMoney);

  }


 function addSequence()
    {
       seqNumber += 1;
       return ""+seqNumber;
 }

function resetSequence()
{
       seqNumber = 0;
       return "";
}




    ]]>
  </msxsl:script>

</xsl:stylesheet>

