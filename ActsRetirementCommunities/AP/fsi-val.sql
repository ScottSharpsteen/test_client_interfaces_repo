-- Receipts validation

IF NOT EXISTS (SELECT * FROM cif1000p_validation_header WHERE validation_intid = 2000) THEN
  INSERT INTO cif1000p_validation_header VALUES
  (2000,'Rcv',1,'Receipts','po','PO: ','itemtext','');
END IF;

DELETE FROM cif1002p_validation_detail WHERE validation_intid = 2000;

INSERT INTO cif1002p_validation_detail VALUES
(4001,2000,'PeriodStartDate','periodstartdate','D','Period Start Date','HDR','Y','N',NULL,5);
INSERT INTO cif1002p_validation_detail VALUES
(4002,2000,'PeriodEndDate','perioddate','D','Period End Date','HDR','Y','N',NULL,10);
INSERT INTO cif1002p_validation_detail VALUES
(4003,2000,'TransactionNumber','vendoraccountid','S','Account Number','HDR','N','N',NULL,15);
INSERT INTO cif1002p_validation_detail VALUES
(4004,2000,'TransactionNumber','po','S','PO Number','HDR','Y','N',NULL,20);
INSERT INTO cif1002p_validation_detail VALUES
(4005,2000,'TransactionNumber','invoiceid','S','Invoice Number','HDR','Y','N',NULL,25);
INSERT INTO cif1002p_validation_detail VALUES
(4006,2000,'Description','description','S','Description','HDR','N','N',NULL,30);
INSERT INTO cif1002p_validation_detail VALUES
(4007,2000,'Note','note','S','Note','HDR','N','N',NULL,35);
INSERT INTO cif1002p_validation_detail VALUES
(4008,2000,'OutgoingUnitId','externalid','S','Vendor External ID','HDR','N','N',NULL,40);
INSERT INTO cif1002p_validation_detail VALUES
(4009,2000,'OutgoingUnitId','distributorid','S','Vendor Distributor ID','HDR','N','N',NULL,45);
INSERT INTO cif1002p_validation_detail VALUES
(4010,2000,'OutgoingUnitId','receiverid','S','Vendor Received ID','HDR','N','N',NULL,50);
INSERT INTO cif1002p_validation_detail VALUES
(4011,2000,'OutgoingUnitId','opcoid','S','Vendor Opco ID','HDR','N','N',NULL,55);
INSERT INTO cif1002p_validation_detail VALUES
(4012,2000,'OutgoingUnitId','exportid1','S','Vendor Export ID 1','HDR','Y','N',NULL,60);
INSERT INTO cif1002p_validation_detail VALUES
(4013,2000,'OutgoingUnitId','exportid2','S','Vendor Export ID 2','HDR','N','N',NULL,65);
INSERT INTO cif1002p_validation_detail VALUES
(4014,2000,'OutgoingUnitId','linkingid','S','Vendor Linking ID','HDR','N','N',NULL,70);
INSERT INTO cif1002p_validation_detail VALUES
(4015,2000,'UnitId','userunitid','S','Unit User Unit ID','HDR','Y','N',NULL,75);
INSERT INTO cif1002p_validation_detail VALUES
(4016,2000,'UnitId','acctgroup','S','Unit Accounting Group','HDR','Y','N',NULL,80);
INSERT INTO cif1002p_validation_detail VALUES
(4017,2000,'UnitId','accttype','S','Unit Accounting Type','HDR','N','N',NULL,85);
INSERT INTO cif1002p_validation_detail VALUES
(4018,2000,'UnitId','usercode1','S','Unit User Code 1','HDR','N','N',NULL,90);
INSERT INTO cif1002p_validation_detail VALUES
(4019,2000,'UnitId','usercode2','S','Unit User Code 2','HDR','N','N',NULL,95);
INSERT INTO cif1002p_validation_detail VALUES
(4020,2000,'UnitId','usercode3','S','Unit User Code 3','HDR','N','N',NULL,100);
INSERT INTO cif1002p_validation_detail VALUES
(4021,2000,'UnitId','usercode4','S','Unit User Code 4','HDR','N','N',NULL,105);
INSERT INTO cif1002p_validation_detail VALUES
(4022,2000,'UnitId','usercode5','S','Unit User Code 5','HDR','N','N',NULL,110);
INSERT INTO cif1002p_validation_detail VALUES
(4023,2000,'UnitId','usercode6','S','Unit User Code 6','HDR','N','N',NULL,115);
INSERT INTO cif1002p_validation_detail VALUES
(4024,2000,'FacilityId','facuserunitid','S','Facility User Unit ID','HDR','N','N',NULL,120);
INSERT INTO cif1002p_validation_detail VALUES
(4025,2000,'FacilityId','facacctgroup','S','Facility Accounting Group','HDR','N','N',NULL,125);
INSERT INTO cif1002p_validation_detail VALUES
(4026,2000,'FacilityId','facaccttype','S','Facility Accounting Type','HDR','N','N',NULL,130);
INSERT INTO cif1002p_validation_detail VALUES
(4027,2000,'AccountNumber','debit_code','S','Product Group Account Number 1','DTL','Y','N',NULL,135);
INSERT INTO cif1002p_validation_detail VALUES
(4028,2000,'AccountNumber','credit_code','S','Product Group Account Number 2','DTL','N','N',NULL,140);
INSERT INTO cif1002p_validation_detail VALUES
(4029,2000,'AccountNumber','codetext3','S','Product Group Account Number 3','DTL','N','N',NULL,145);
INSERT INTO cif1002p_validation_detail VALUES
(4030,2000,'AccountNumber','codetext4','S','Product Group Account Number 4','DTL','N','N',NULL,150);
INSERT INTO cif1002p_validation_detail VALUES
(4031,2000,'AccountNumber','codetext5','S','Product Group Account Number 5','DTL','N','N',NULL,155);
INSERT INTO cif1002p_validation_detail VALUES
(4032,2000,'AccountNumber','codetext6','S','Product Group Account Number 6','DTL','N','N',NULL,160);
INSERT INTO cif1002p_validation_detail VALUES
(4033,2000,'AccountNumber','codetext7','S','Product Group Account Number 7','DTL','N','N',NULL,165);
INSERT INTO cif1002p_validation_detail VALUES
(4034,2000,'AccountNumber','codetext8','S','Product Group Account Number 8','DTL','N','N',NULL,170);

COMMIT;
