
--Turn on the log display
IF EXISTS (SELECT * FROM cbo0081p_treelist
WHERE tl_intid = 809) THEN
  UPDATE cbo0081p_treelist 
  SET appid = 'FMS'
  WHERE tl_intid = 809;
END IF;

COMMIT;

-- Turn on the manual account export and the accounting settings icons

IF EXISTS (SELECT * FROM cbo0081p_treelist
WHERE tl_intid = 800) THEN
  UPDATE cbo0081p_treelist 
  SET appid = 'PUR'
  WHERE tl_intid = 800;
END IF;

IF EXISTS (SELECT * FROM cbo0081p_treelist
WHERE tl_intid = 808) THEN
  UPDATE cbo0081p_treelist 
  SET appid = 'PUR'
  WHERE tl_intid = 808;
END IF;

COMMIT;


if not exists(select * from cbo0001p_parms where parmname = 'RcvDaysAfter') then
  insert into cbo0001p_parms(
    application,parmname,unitid,parmvalue,usertext1,usertext2,usertext3,
    usernum1,usernum2,parmlabel,editstyle,editdddw,
    validationrule,validationmsg,sortorder,userid,editserver,visibility)
  VALUES ('FSI','RcvDaysAfter',0,'0','AP','1980-01-01','Rcv',
    2000.00000,1,'Send Receipts After How Many Days','edit','d_cbo_dummy',
    '(Integer(GetText()) >= 0 and integer(gettext()) <=500 and isnumber(gettext()))',
    'Please enter a whole number between 0 and 500',10,'Act2','Send Receipts to Interface','visible');
ELSE
  UPDATE cbo0001p_parms SET userid = 'Act2' WHERE parmname = 'RcvDaysAfter';
end if;

COMMIT;

UPDATE cbo0001p_parms SET Parmvalue=NULL WHERE Parmname='AcctInterfaceName';

COMMIT;

UPDATE cbo0001p_parms Set Parmvalue='2001-01-01' where Parmname='AcctStartDate';

COMMIT;
