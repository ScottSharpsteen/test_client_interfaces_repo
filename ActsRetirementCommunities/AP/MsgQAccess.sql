--Command to turn on Multi-User MessageQ access on the CBORD DB residing on DB Server

update cbo0001p_parms
set parmvalue='\\\78LPML7\FSI\accountingRequestQ' 
where parmname='AcctReqQueue'

COMMIT;