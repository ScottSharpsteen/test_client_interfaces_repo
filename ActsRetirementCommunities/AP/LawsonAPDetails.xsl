<?xml version="1.0" encoding="iso-8859-1"?>
<!-- 	stylesheet for creating Client AP Interface messages: version $Revision: #1 $
	Changes:
	02/18/02	GTT	Added length constraints to meet Lawson Interface.
	04/11/02	GTT	Recode to use xpath mechanism of rollup
	05/24/02	CLC	Added/Deleted Lawson fields per requirements for import to Lawson
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
		        version="1.0">

<!--xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes" /--> 
<xsl:output method="text" encoding="UTF-8" /> 
<xsl:include href="DateConversion.xsl"/> 


<!-- Global Variables (for convienience mostly) -->
<xsl:variable name="EndOfRec"><xsl:text>&#x0d;&#x0a;</xsl:text></xsl:variable>


<xsl:template match="/">
       <xsl:for-each select="//Transaction">
           <xsl:variable name="header" select="./Header"/>
           <xsl:variable name="itemlist" select="./ItemList"/>
           <xsl:variable name="distinct" select="ItemList/Item[not(AccountNumberList/AccountNumber[1]=preceding-sibling::Item/AccountNumberList/AccountNumber[1])]/AccountNumberList/AccountNumber[1]"/>

          <!--Create item records from the uniques keys above-->
          <xsl:variable name="itemRecords"> 
            <xsl:for-each select="$distinct">
              <xsl:variable name="Acct" select="."/>
                <!--Acct><xsl:value-of select="."/></Acct-->
	        <xsl:variable name="matches" select="$itemlist/Item[AccountNumberList/AccountNumber[1]=$Acct]"/>	
 	         <xsl:call-template name="ProcessItems">
		    <xsl:with-param name="header" select="$header"/>
		    <xsl:with-param name="itemlist" select="$matches"/>
                </xsl:call-template>
           </xsl:for-each> 
          </xsl:variable>
          <xsl:for-each select="msxsl:node-set($itemRecords)/outputRecord">
             <xsl:sort select="EF-CVD-DIST-SEQ-NBR"/>
             <xsl:apply-templates select="."/>
          </xsl:for-each>
       </xsl:for-each>   
     
  <!--/Test-->
</xsl:template>

<!--
     Sum up the totals and create line item records. 
-->
<xsl:template name="ProcessItems">
  <xsl:param name="header"/>
  <xsl:param name="itemlist"/>
  
  <xsl:variable name="total">
	   <xsl:call-template name="sumPriceTimesQuantity">
  	      <xsl:with-param name="itemlist" select="$itemlist" /> 
  	      <xsl:with-param name="index" select="2" /> 
  	   </xsl:call-template>
  </xsl:variable>

  <!--Total><xsl:value-of select="$total div 100"/></Total-->
    
  <xsl:call-template name="outputXMLRecord">
		  <xsl:with-param name="header" select="$header"/>
                  <xsl:with-param name="amount" select="format-number($total div 100, '00000000000.00')"/>
                  <xsl:with-param name="item" select="$itemlist[1]"/>
   </xsl:call-template>

</xsl:template>

<!--
    Output record and move carriage return to the next line
-->
<xsl:template match="outputRecord">
  <xsl:apply-templates/>
  <xsl:value-of select="$EndOfRec"/>
</xsl:template> 
 
<!--
   XML recored for further processing/sorting
-->

<!-- Create an outputXMLRecord (as XML for further processing/sorting) -->
<xsl:template name="outputXMLRecord">
  <xsl:param name="header"/>
  <xsl:param name="amount"/>
  <xsl:param name="item"/>
  
  <outputRecord>

  <EF-CVI-COMPANY>
    <xsl:value-of select="substring($header/FacilityIdList/FacilityId[2],1,4)"/>
    <xsl:text>,</xsl:text>
  </EF-CVI-COMPANY>

  <EF-CVI-VENDOR>
     <xsl:value-of select="substring($header/OutgoingUnitIdList/OutgoingUnitId[5],1,9)"/>
     <xsl:text>,</xsl:text>    
  </EF-CVI-VENDOR>

  <EF-CVI-EDI-NBR>
     <xsl:text>,</xsl:text>
  </EF-CVI-EDI-NBR>

  <EF-CVI-INVOICE>
     <xsl:value-of select="substring($header/TransactionNumberList/TransactionNumber[3],1,22)"/>
     <xsl:text>,</xsl:text>
  </EF-CVI-INVOICE>

  <EF-CVI-SUFFIX>
     <xsl:text>,</xsl:text>
  </EF-CVI-SUFFIX>

  <EF-CVD-DIST-SEQ-NBR>
    <xsl:value-of select="cbord:padLeft(cbord:addSequence(),'0',4)"/>
    <xsl:text>,</xsl:text> 
  </EF-CVD-DIST-SEQ-NBR>

  <EF-CVD-ORIG-TRANS-AMT>
     <xsl:if test="$amount &lt;0"/>
     <xsl:if test="$amount &gt;= 0"><xsl:text>0</xsl:text></xsl:if>
     <xsl:value-of select="$amount"/>
     <xsl:text>,</xsl:text>
  </EF-CVD-ORIG-TRANS-AMT>

  <EF-CVD-TAXABLE-AMT>
     <xsl:text>,</xsl:text>
  </EF-CVD-TAXABLE-AMT>

  <EF-CVD-DIST-COMPANY>
     <xsl:text>,</xsl:text>
  </EF-CVD-DIST-COMPANY>

  <EF-CVD-TO-BASE-AMT>
     <xsl:if test="$amount &lt;0"/>
     <xsl:if test="$amount &gt;= 0"><xsl:text>0</xsl:text></xsl:if>
     <xsl:value-of select="$amount"/>
     <xsl:text>,</xsl:text>
  </EF-CVD-TO-BASE-AMT>

  <EF-CVD-DIS-ACCT-UNIT>
     <xsl:value-of select="substring($header/UnitIdList/UnitId[1],1,15)"/>
     <xsl:text>,</xsl:text>
  </EF-CVD-DIS-ACCT-UNIT>

  <EF-CVD-DIS-ACCOUNT>
     <xsl:value-of select="substring($item/AccountNumberList/AccountNumber[1],1,6)"/>
     <xsl:text>,</xsl:text>
  </EF-CVD-DIS-ACCOUNT>

  <EF-CVD-DIS-SUB-ACCT>
      <xsl:text>0000,</xsl:text>
  </EF-CVD-DIS-SUB-ACCT>

  <EF-CVD-TAX-CODE>
     <xsl:text>,</xsl:text>
  </EF-CVD-TAX-CODE>

  <EF-CVD-DESCRIPTION>
     <xsl:text>,</xsl:text>
  </EF-CVD-DESCRIPTION>

  <EF-CVD-DIST-REFERENCE>
     <xsl:text>,</xsl:text>
  </EF-CVD-DIST-REFERENCE>

  <EF-CVD-ACTIVITY>
     <xsl:text>,</xsl:text>
  </EF-CVD-ACTIVITY>

  <EF-CVD-ASSET-DESC>
     <xsl:text>,</xsl:text>
  </EF-CVD-ASSET-DESC>

  <EF-CVD-TAG-NBR>
     <xsl:text>,</xsl:text>
  </EF-CVD-TAG-NBR>

  <EF-CVD-ITEM-NBR>
     <xsl:text>,</xsl:text>
  </EF-CVD-ITEM-NBR>

  <EF-CVD-ITEM-DESC>
     <xsl:text>,</xsl:text>
  </EF-CVD-ITEM-DESC>

  <EF-CVD-ITEM-QUANTITY>
     <xsl:text>,</xsl:text>
  </EF-CVD-ITEM-QUANTITY>

  <EF-CVD-ASSET-TEMPLATE>
     <xsl:text>,</xsl:text>
  </EF-CVD-ASSET-TEMPLATE>

  <EF-CVD-INSRV-DATE>
     <xsl:text>,</xsl:text>
  </EF-CVD-INSRV-DATE>

  <EF-CVD-PURCHASE-DATE>
     <xsl:text>,</xsl:text>
  </EF-CVD-PURCHASE-DATE>

  <EF-CVD-MODEL-NUMBER>
     <xsl:text>,</xsl:text>
  </EF-CVD-MODEL-NUMBER>

  <EF-CVD-SERIAL-NUMBER>
     <xsl:text>,</xsl:text>
  </EF-CVD-SERIAL-NUMBER>

  <EF-CVD-HOLD-AM>
     <xsl:text>,</xsl:text>
  </EF-CVD-HOLD-AM>

  <EF-CVD-ASSET>
     <xsl:text>,</xsl:text>
  </EF-CVD-ASSET>

  <EF-CVD-ACCT-CATEGORY>
     <xsl:text>,</xsl:text>
  </EF-CVD-ACCT-CATEGORY>

  <EF-CVD-UNT-AMOUNT>
     <xsl:text>,</xsl:text>
  </EF-CVD-UNT-AMOUNT>

  <EF-CVD-ITEM-TAX-TRAN>
     <xsl:text>,</xsl:text>
  </EF-CVD-ITEM-TAX-TRAN>

  <EF-CVD-ASSET-GROUP>
     <xsl:text>,</xsl:text>
  </EF-CVD-ASSET-GROUP>

  <EF-CVD-COMBINE>
     <xsl:text>,</xsl:text>
  </EF-CVD-COMBINE>

  <EF-CVD-ACCT-UNIT>
     <xsl:text>,</xsl:text>
  </EF-CVD-ACCT-UNIT>

  <EF-CVD-TAX-POINT>
     <xsl:text>,</xsl:text>
  </EF-CVD-TAX-POINT>

  <EF-CVD-LINE-TYPE>
     <xsl:text>,</xsl:text>
  </EF-CVD-LINE-TYPE>

  <EF-CVD-COMMODITY>
     <xsl:text>,</xsl:text>
  </EF-CVD-COMMODITY>

  <EF-CVD-WEIGHT>
     <xsl:text>,</xsl:text>
  </EF-CVD-WEIGHT>

  <EF-CVD-SECONDARY-QTY>
     <xsl:text>,</xsl:text>
  </EF-CVD-SECONDARY-QTY>

  <EF-CVD-SEC-UOM>
     <xsl:text>,</xsl:text>
  </EF-CVD-SEC-UOM>

  <EF-CVD-SEGMENT-BLOCK>
     <xsl:text>,</xsl:text>
  </EF-CVD-SEGMENT-BLOCK>

  <EF-CVD-DIVISION>
     <xsl:text>,</xsl:text>
  </EF-CVD-DIVISION>

  <EF-CVD-LOCATION-NAME>
     <xsl:text>,</xsl:text>
  </EF-CVD-LOCATION-NAME>

  </outputRecord>

</xsl:template> 


<!--
     Helper function to create the sum over a list of price quantity pairs.
     Input is list of price quantity pairs and price quantity index
 -->

<xsl:template name="sumPriceTimesQuantity">
  <xsl:param name="itemlist"/>
  <xsl:param name="index"/>

  <xsl:choose>
    <xsl:when test="$itemlist">
       <xsl:variable name="first" select="$itemlist[1]"/>
       <xsl:variable name="others">
         <xsl:call-template name="sumPriceTimesQuantity">
           <xsl:with-param name="itemlist" select="$itemlist[position()!=1]"/>
           <xsl:with-param name="index" select="$index"/>
         </xsl:call-template>
       </xsl:variable>

       <xsl:value-of select="$others + 
                             cbord:multAndAddToSum(string($first/PriceQuantityList/PriceQuantity[$index]/Price), string($first/PriceQuantityList/PriceQuantity[$index]/Qty),2)"/>

    </xsl:when>

    <!-- empty list, return 0 -->
    <xsl:otherwise>
       <xsl:value-of select="0"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>



  <msxsl:script implements-prefix="cbord">
  <![CDATA[
   var seqNumber=0;

    // left pad a string to make the given size with the given character
    function padLeft(value,pad,size)
    {
       var padded = "" + value;
       while (padded.length < size)
       {
         padded = pad+padded;
       }
       return padded;
    }

    // right pad a string to make the given size with the given character
    function padRight(value,pad,size)
    {
       var padded = value;
       while (padded.length < size)
       {
         padded = padded+pad;
       }
       return padded;
    }

  function yesterday()
  {
    var MinMilli = 1000 * 60;
    var HrMilli = MinMilli * 60;
    var DyMilli = HrMilli * 24;

    var now = new Date();

    var dateInMilliSeconds = now.getTime();
    // go back one day
    dateInMilliSeconds = dateInMilliSeconds-DyMilli;
    now.setTime(dateInMilliSeconds);

    var tyear = now.getFullYear();
    var tmon = now.getMonth();
        tmon = tmon + 1;
    var tday = now.getDate();
    tday = new Number(tday);
    if (tday < 10)
       tday = "0"+tday;
    tmon = new Number(tmon);
    if(tmon <10)
      tmon = "0"+tmon;
     
    tdate = tyear + "/" + tmon + "/" + tday; 
    return tdate;
  
  }  
 


    function today()
    {

      var now = new Date();

      var tyear = now.getFullYear();
      var tmon = now.getMonth();
        tmon = tmon + 1;
      var tday = now.getDate();
      tday = new Number(tday);
      if (tday < 10)
        tday = "0"+tday;
       tmon = new Number(tmon);
      if(tmon <10)
      tmon = "0"+tmon;
     
      tdate = tmon +  tday + tyear; 
      return tdate;
    }

function multAndAddToSum(p,q,i)
{
   var sign = "";
   // remove the decimal point and create two integers
   var p2=p.split(".");
   var q2=q.split(".");

   var ind = p2[1].length + q2[1].length;
  
   var price=new Number(p2[0] + p2[1]);
   var qty=new Number(q2[0] + q2[1]);
       
   // perform the multiplication
   var prod=price * qty;

   //Determine the sign value
   if(prod < 0)
   { 
     prod = ""+prod;
     sign = prod.substring(0,1);
     prod = prod.substring(1, prod.length);
   }
   prod = new Number(prod);
  

   // divide by 10**ind-2 and round to get implied decimal of two places
   var prodAsMoney = Math.round(prod/Math.pow(10,ind - i));
   //Sum=Sum + new Number(sign + prodAsMoney);


   return ""+new Number(sign + prodAsMoney);

  }


 function addSequence()
    {
       seqNumber += 1;
       return ""+seqNumber;
 }

function resetSequence()
{
       seqNumber = 0;
       return "";
}




    ]]>
  </msxsl:script>

</xsl:stylesheet>

