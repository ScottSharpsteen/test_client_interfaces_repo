@ECHO OFF
rem cbordftp.BAT
rem  This batch file should be used if the customer is currently using
rem
rem %1 = ORDER or PRICE depending on which is being done
rem             This arguement is automatically assigned when the user selects
rem             either Place Order (ORDER) or Update Prices (PRICE).  There
rem             is no setup requirements.
rem %2 = Finshed.exp file to signal completion
rem             A marker file (finished.xxx) needs to always be copied to
rem             FINISHED.EXP in order for FMS to know the batch file has
rem             completed.  xxx will be assigned per vendor.
rem %3 = Working Directory for Purchasing
rem             The installation process of FMS sets up a PRIVATEDIR in the
rem             registry. Typically it is \\CBORDWIN\\WORK.
rem %4 = Account # (6 digits)
rem             ORDER = The account number is embedded in the order file
rem             PRICE = Is defined under Setup/Purchasing Settings/Vendor,
rem             Order tab, Default Cust ID#.
rem %5 = Opco ID
rem             The Opco ID is used when a specific vendor requires a unique
rem             order file layout.  SYSTRN.EXE is used to convert STORE001.POE
rem             into the specific vendor order file layout. The Opco ID# is
rem             defined in the RECEIVER ID# under Setup/Purchase Settings/
rem             Vendor/Ordering Tab.  Example in batch file:
rem             SYSTRN %5 NOTE: This is not required for Alliant.
rem
rem  FILE TYPE: The import process will check either the file name or header
rem             record to determine what type of file it is.  EX: PRICE.OUT
rem             should not be renamed to PRICE.NEW. ALKASCII.NEW becomes
rem             ALKASCII.OL0
rem             After the price update process has occured, the file is
rem             renamed to *.OL1  (LETTER 0, LETTER L, NUMBER 1)

%~dp0\OTM -P=%1 -M=%2 -D=%3 -A=%4 -S=%5
