<?xml version="1.0"?>
<!-- Generic stylesheet for transforming ADT messages. PVCS version: $Revision:   1.35  $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:cbordRM="http://cbord.com/xsltnsRM"
                xmlns:translator="urn:translatorObject"
                version="1.0">

<xsl:import href="clientRules.xsl"/>

<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<msxsl:script implements-prefix="cbord">
	<![CDATA[
		// translate a string to lower case
		function stringToLower(str)
		{
			if (str && typeof(str) == "string")
			{
				return str.toLowerCase();
			}
			return "";
		}
	]]>
</msxsl:script>

<xsl:template match="/">
		<xsl:apply-templates select="*"/>
</xsl:template>

<!-- Check for Unknown record type filter -->
<xsl:template match="//action">
	<!-- Check to filter on Unknown record type -->
	<xsl:choose>
		<xsl:when test="(. = 'Unknown')">
			<action><xsl:value-of select="."/>
				<FILTER><xsl:value-of select="concat('Message Filtered: Unknown Record Type: ', ../../OriginalMsgInfo/Type)"/></FILTER>
			</action>
		</xsl:when>
		<xsl:otherwise>
			<action><xsl:value-of select="."/></action>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- Check patient class for filter criteria based on patientClassFilter variable within clientRules.xsl -->
<!-- If transaction is to be filtered due to the patient class, insert FILTER tag within patientClass tag describing reason for filter -->
<xsl:template match="//patientClass">
	<xsl:variable name="class" select="cbord:stringToLower(string(.))"/>

	<xsl:choose>
		<xsl:when test="msxsl:node-set($patientClassFilter)!=''">
			<xsl:choose>
				<xsl:when test="msxsl:node-set($patientClassFilter)//process">
					<xsl:choose>
						<xsl:when test="msxsl:node-set($patientClassFilter)//process[cbord:stringToLower(string(.))=$class]">
							<patientClass><xsl:value-of select="."/></patientClass>
						</xsl:when>
						<xsl:otherwise>
							<patientClass><xsl:value-of select="."/>
								<FILTER><xsl:value-of select="concat('Message Filtered: Patient class is ', '(', ., ')')"/></FILTER>
							</patientClass>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="msxsl:node-set($patientClassFilter)//filter">
					<xsl:choose>
						<xsl:when test="msxsl:node-set($patientClassFilter)//filter[cbord:stringToLower(string(.))=$class]">
							<patientClass><xsl:value-of select="."/>
								<FILTER><xsl:value-of select="concat('Message Filtered: Patient class is ', '(', ., ')')"/></FILTER>
							</patientClass>
						</xsl:when>
						<xsl:otherwise>
							<patientClass><xsl:value-of select="."/></patientClass>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<patientClass><xsl:value-of select="."/></patientClass>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>



<!-- Check filter criteria based on FilterNonDisIfDisDTM variable within ClientRules.xsl -->
<!-- Filter transaction if there is a Discharge DTM, the transaction is not a Discharge and variable is set to YES -->
<!--    Insert FILTER tag within DischargeDTM tag describing reason for filter -->
<xsl:template match="//DischargeDTM">
	<xsl:choose>
		<xsl:when test="(. !='' and ../../operation/action != 'Discharge' and $FilterNonDisIfDisDTM = 'YES')">
			<DischargeDTM><xsl:value-of select="."/>
				<FILTER><xsl:value-of select="concat('Message Filtered: Non-Discharge with Discharge DTM of:  ', '(', ., ')')"/></FILTER>
			</DischargeDTM>
		</xsl:when>
		<xsl:otherwise>
			<DischargeDTM><xsl:value-of select="."/></DischargeDTM>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>



<!-- Check nursing station for filter criteria based on nursingStationFilter variable within clientRules.xsl -->
<xsl:template match="//nursingStation">
 	<xsl:variable name="station" select="cbord:stringToLower(string(.))"/>
 
 	<xsl:choose>
  		<xsl:when test="msxsl:node-set($nursingStationFilter)!=''">
   			<xsl:choose>
    				<xsl:when test="msxsl:node-set($nursingStationFilter)//process">
     					<xsl:choose>
      						<xsl:when test="msxsl:node-set($nursingStationFilter)//process[cbord:stringToLower(string(.))=$station]">
       							<nursingStation><xsl:value-of select="."/></nursingStation>
      						</xsl:when>
      						<xsl:otherwise>
       							<nursingStation><xsl:value-of select="."/>
        						<FILTER><xsl:value-of select="concat('Message Filtered: Nusing station is ', '(', ., ')')"/></FILTER>
       							</nursingStation>
      						</xsl:otherwise>
     					</xsl:choose>
    				</xsl:when>

    				<xsl:when test="msxsl:node-set($nursingStationFilter)//filter">
     					<xsl:choose>
      						<xsl:when test="msxsl:node-set($nursingStationFilter)//filter[cbord:stringToLower(string(.))=$station]">
       							<nursingStation><xsl:value-of select="."/>
        						<FILTER><xsl:value-of select="concat('Message Filtered: Nursing station is ', '(', ., ')')"/></FILTER>
       							</nursingStation>
      						</xsl:when>
      						<xsl:otherwise>
       							<nursingStation><xsl:value-of select="."/></nursingStation>
      						</xsl:otherwise>
     					</xsl:choose>
    				</xsl:when>
   			</xsl:choose>
  		</xsl:when>
  		<xsl:otherwise>
   			<nursingStation><xsl:value-of select="."/></nursingStation>
  		</xsl:otherwise>
 	</xsl:choose>
</xsl:template>



<!-- Check filter criteria based on FilterIfNoRoom variable within ClientRules.xsl -->
<!-- Filter transaction if there is No Room value in the transaction (i.e. null/blank or spaces) -->
<!--    Insert FILTER tag within Room tag describing reason for filter -->
<xsl:template match="//room">
	<xsl:choose>
		<xsl:when test="((. ='' or .=' ')and $FilterIfNoRoom = 'YES')">
			<room><xsl:value-of select="."/>
				<FILTER><xsl:value-of select="'Message Filtered: No Room Value'"/></FILTER>
			</room>
		</xsl:when>
		<xsl:otherwise>
			<room><xsl:value-of select="."/></room>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>



<!-- Handle any node not yet matched, strip out any comments or processing instructions -->
<xsl:template match="*|@*|text()">
	<xsl:copy><xsl:apply-templates select="*|@*|text()"/></xsl:copy>
</xsl:template>

</xsl:stylesheet>
