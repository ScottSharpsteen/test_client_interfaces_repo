<?xml version="1.0"?>
<!-- Generic stylesheet for transforming ADT messages. PVCS version: $Revision:   1.4  $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:cbordRM="http://cbord.com/xsltnsRM"
                version="1.0">

<!-- Option one: Leave variable without any value. This means process any patient type.
Option two: Use <process> tags. You may have as many as needed. This essentially means "process ONLY these types".
Option three: Use <filter> tags. You may have as many as needed. This essentially means "process everything BUT these types". -->
<xsl:variable name="patientClassFilter">
	<!--process>I</process-->
	<!--filter>O</filter-->
</xsl:variable>

<!-- CONFIGURE: Enable Protocol ID: Change to 'YES' to enable Protocol interfacing. -->
<xsl:variable name="usingProtocolID" select="'YES'"/>

<!-- CONFIGURE: Enable coded allergies: ('NO' or 'YES') -->
<!-- Use 'YES' to enable coded allergies with a data lookup -->
<!-- Use 'ONE2ONE' to enable allergies with translation and then a lookup -->
<xsl:variable name="usingAllergies" select="'YES'"/>

<!-- CONFIGURE: Trigger used for removal of all allergies -->
<!--  Allergies must be set to REPLACE.  Allergy code(s) sent in addition to the trigger -->
<!--  for removal of allergies, will be processed.  -->
<!--  Replace 'XXXX' with coded Allergy value used for Trigger (ie. 'NKA', ' ', '&quot;&quot;')  -->
<!--  Replace 'XXXX' with 'EMPTYFIELD' if want trigger to be nothing (null/blank) sent as an Allergy code -->
<xsl:variable name="AllergyRemovalTrigger" select="'NKA'"/>

<!-- CONFIGURE: Enable ht/wt usage: Change to 'YES' to enable ht/wt (anthro) data mappings -->
<xsl:variable name="usingAnthroData" select="'NO'"/>

<!-- CONFIGURE: Default units of measure for ht/wt -->
<!-- if using anthro data these values are used when no Units of measure in message data -->
<xsl:variable name="defaultUOFMheight" select="'CM'"/>
<xsl:variable name="defaultUOFMweight" select="'KG'"/>

<!-- CONFIGURE: Translation Table:  Default file name: translationTable.xml -->
<!-- For extra translation tables: Add a new variable with it's related translation table file -->
<!-- Directory for translation tables: \CBORDWIN\Interfaces\ADT (stored with xsl scripts) -->
<xsl:variable name="translationTableFileName" select="'translationTable.xml'"/>
<xsl:variable name="translationTableSMPFileName" select="'translationTableSMP.xml'"/>
<xsl:variable name="translationTableAllergyFileName" select="'translationTableAllergy.xml'"/>
<xsl:variable name="translationTableFileName_NursingStation_TO_Room" select="'translationTable_NursingStation_TO_Room.xml'"/>
<!-- The following table files are for the service interface -->
<xsl:variable name="translationTableSStartMealFileName" select="'translationTableSStartMeal.xml'"/>
<xsl:variable name="translationTableSEndMealFileName" select="'translationTableSEndMeal.xml'"/>
<xsl:variable name="translationTableSLocationFileName" select="'translationTableSLocation.xml'"/>
<xsl:variable name="translationTableSNSFileName" select="'translationTableSNS.xml'"/>


<!-- CONFIGURE: Option for enabling age-based diet interfacing: 
Set to 'YES' to enable age-based diet interfacing
Set to 'NO' to disable age-based diet interfacing (default is 'NO') -->
<xsl:variable name="useAgeBasedDiets" select="'NO'"/>

<!-- CONFIGURE: Option for interfacing age-based diets: 
If invalid or missing DOB is in HL7 message, and no DOB in DB:
'YES' process the order information as is
'NO' do not process the order information (default is 'NO') -->
<xsl:variable name="noDOB" select="'NO'"/>


<!-- CONFIGURE: Option for interfacing Dietary Service info: 
Information interfaced to Service tab in card file:
'YES' process dietary service information 
'NO' do not process dietary service information (default is 'NO') -->
<xsl:variable name="useDietaryService" select="'NO'"/>

<!-- CONFIGURE: ADT Service interface processing: 
Will the interface need to enter DS info in an ADT message:
'YES' process dietary service information in ADT messages
'NO' do not process dietary service information in ADT messages (default is 'NO') -->
<xsl:variable name="dietaryServiceInADT" select="'NO'"/>

<!-- CONFIGURE:  The service/location entries must use the same 
nursing station.  If the entry is '' then the service Nursing Station 
will be the same as the service/location sent to the interface.  
(default is 'service') -->
<xsl:variable name="serviceNursingStation" select="'service'"/>

<!-- CONFIGURE:  "Tray Type" input that will determine whether the data in ODT.3 
(Text Instruction) is input and placed as a Tray Ticket Message or Personal Menu Note.  
(defaults are 'MSG' and 'PMN') -->
<xsl:variable name="serviceTrayTicketMessage" select="'MSG'"/>
<xsl:variable name="servicePersonalMenuNote" select="'PMN'"/>

<!-- CONFIGURE:  If Tray Ticket Message or Personal Menu Note data is present in the 
message, do you want to prevent the flags (above) from being passed as the message's 
Service/Location? (i.e. don't pass 'MSG' as the Service/Location for the message) 
(default is 'Y')-->
<xsl:variable name="preventNoteServiceLocation" select="'Y'"/>


<!-- CONFIGURE: Option for interfacing Enteral Tube Feeding info: 
Information interfaced to Orders Enteral section in card file:
'YES' process Enteral Tube Feeding information 
'NO' do not process Enteral Tube Feeding information (default is 'NO') -->
<xsl:variable name="useEnteralOrder" select="'NO'"/>







<!-- CONFIGURE: Enable facility mappings: Change to 'YES' to use -->
<xsl:variable name="usingFacilityNames" select="'NO'"/>

<!-- CONFIGURE:  Filter all Non-Discharge transactions that have a Discharge Date/Time-->
<xsl:variable name="FilterNonDisIfDisDTM" select="'YES'"/>

<!-- CONFIGURE:  Automatically check for all possible variation for the room lookups (i.e. with/without dash, etc...) -->
<xsl:variable name="AutoCheckRoomBed" select="'YES'"/>

<!-- CONFIGURE Nursing Station lookup: possible values are NAME, ID.  Not needed if Auto check room lookup is set to YES. -->
<xsl:variable name="NursingIdOrName" select="'ID'"/>

<!-- CONFIGURE:  Nursing Station Filter
Option one: Leave variable without any value. This means process any nursing station.
Option two: Use <process> tags. You may have as many as needed. This essentially means "process ONLY these nursing stations".
Option three: Use <filter> tags. You may have as many as needed. This essentially means "process everything BUT these nursing stations". -->
<xsl:variable name="nursingStationFilter">
 	<!--process>XXX</process-->
 	<!--filter>YYY</filter-->
</xsl:variable>

<!-- CONFIGURE:  Filter all transactions that have no Room value (i.e. null/blank or spaces)  ('NO' or 'YES')-->
<xsl:variable name="FilterIfNoRoom" select="'NO'"/>

<!-- CONFIGURE: Enable Coded Race: Change to 'YES' to enable Race interfacing. -->
<xsl:variable name="usingRace" select="'NO'"/>

<!-- CONFIGURE: Enable Coded Language: Change to 'YES' to enable Language interfacing. -->
<xsl:variable name="usingLanguage" select="'NO'"/>

<!-- CONFIGURE: Enable coded diagnosis: Change to 'YES' to enable coded diagnosis. -->
<!-- Note, the admit diagnosis (a text field) is not affected by this parameter -->
<xsl:variable name="usingCodedDiag" select="'NO'"/>

<!-- CONFIGURE Coded diagnosis lookup: possible values are SHORTNAME, NAME, BILLINGID, EXTERNALID -->
<xsl:variable name="diagLookupKind" select="'SHORTNAME'"/>

<!-- CONFIGURE: Ignore patient suffix: set to 'YES' to ignore patient suffix; default is 'NO' -->
<xsl:variable name="ignorePatientSuffix" select="'NO'"/>

<!-- CONFIGURE: Strip Leading Zeros from MRN: set to 'YES' to strip leading zeros; default is 'NO' --> 
<xsl:variable name="MRNRemoveLeadZero" select="'NO'" /> 

<!-- CONFIGURE: Option for handling a diet order with an earlier DTM than the current: 
Set to 'YES' to use order start date and time sent in message (default is 'YES')
Set to 'NO' to use sytem date and time (current DTM) -->
<xsl:variable name="orderDTMBeforeNow" select="'YES'"/>




<!-- Room Map section -->
  <msxsl:script implements-prefix="cbordRM">
  <![CDATA[

    var roomMap = new ActiveXObject("Scripting.Dictionary");
    // ----------------------------------------------------------------------
    // CONFIGURE NURSING STATION/ROOMS DATA MAP HERE: 
    //    KEY = Nursing station code found in HL7 data; "*" for default translation
    //    2 = NSS Nursing station ('*' means use same value)
    //    3 = Strip leading Zeros from Room (Y or N) 
    //    4 = Strip leading Zeros from Bed (Y or N)
    //    5 = Insert string between room/bed  (can be empty)
    // (LATER)
    //    6 = Add specified string to beginning of the Room  (can be empty)
    //    7 = Add specified string to beginning of the Bed  (can be empty)
    // ----------------------------------------------------------------------
    // empty translation that concatenates room and bed
    //roomMap.Add("*","*,N,N,");     

    // empty translation (assumes using nursing station id) that inserts a dash between room and bed
    roomMap.Add("*","*,N,N,"); 

    // extract nursing station name from room map
    function getNursingStation(ns)
    {
        var NSS_ns = ns;
        if (roomMap.Exists(ns))
        {
            // extract parameters from map:
            var transControl = roomMap.Item(ns);
            var parms = transControl.split(",");
            NSS_ns = parms[0];
        }
        return NSS_ns;
    }

    // format a room and bed according to the room map
    function getRoomBed(ns,room,bed)
    {
        var transControl = "";
        if (roomMap.Exists(ns))
        {
            transControl = roomMap.Item(ns);
        }
        else if (roomMap.Exists("*"))
        {
            transControl = roomMap.Item("*");
        }
        else
        {
           transControl = "*,N,N,";
        }

        // extract parameters:
        var parms = transControl.split(",");
        var NSS_room = "";
        if (typeof(room) == "string")
          NSS_room = room;

        var NSS_bed = "";
        if (typeof(bed) == "string")
           NSS_bed = bed;
        var stripZeroRoom = parms[1];
        var stripZeroBed = parms[2];
        var betweenString = parms[3];

        var stripper = /^0*/;
        if (stripZeroBed == "Y")
        {
            NSS_bed = NSS_bed.replace(stripper,"");
        }

        if (stripZeroRoom == "Y")
        {
            NSS_room = NSS_room.replace(stripper,"");
        }

        return NSS_room + betweenString + NSS_bed;
    }
    ]]>
  </msxsl:script>

</xsl:stylesheet>
