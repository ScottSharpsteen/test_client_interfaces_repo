﻿<?xml version="1.0"?>
<!-- Generic stylesheet for transforming ADT messages. PVCS version: $Revision:   1.35  $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:cbordRM="http://cbord.com/xsltnsRM"
                xmlns:translator="urn:translatorObject"
                version="1.0">

<!-- include client rules for ADT interface: these will over ride any default rules -->
<xsl:import href="clientRules.xsl"/> 

<xsl:import href="mappingORD.xsl"/>

<!-- include service order interface templates-->
<xsl:import href="mappingSRV.xsl"/>

<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<!-- inlcude dummy translator for command line debugging -->
<!-- xsl:include href="debug.xsl"/ -->

  <msxsl:script implements-prefix="cbord">
  <![CDATA[

    // translate a string to lower case
    function stringToLower(str)
    {
        if (str && typeof(str) == "string")
        {
            return str.toLowerCase();
        }
        return "";
    }

    // Add Carriage Return to end of String
	function strAddCR(str)
	{
		var returnStr = str;

		returnStr = returnStr + '\r';

		return returnStr;
	}


    // remove hyphens from a SSN
    function removeSSNHyphens(ssn)
    {
        var res;
        res = "";

        re = /-/g;
        res = ssn.replace(re,"");
        return res;
    }

    // Startup: initialize global variables used for processing a single message
    function startup()
    {
        return '';
    }

    // get a text value HL7 style, that is remove "" to indicate erase value in DB
    function getHL7text(result)
    {
       if (result=='""')
       {
            result='';
       }
        return result;
    }

    // translate an HL7 style date/time into our format: ccyy-mm-dd hh:mm:ss
    function formatHL7DTM (where,dtm)
    {
        if (dtm.length < 8)
        {
            return "Invalid DTM '"+dtm+"' in field '"+where+"'";
        }

        var year  = parseInt(dtm.substr(0,4),10);
        var month;
        var day;
        if (dtm.substr(4,1)=='0')
            month = parseInt(dtm.substr(5,1),10);
        else
            month = parseInt(dtm.substr(4,2),10);

        if (dtm.substr(6,1)=='0')
            day = parseInt(dtm.substr(7,1),10);
        else
            day = parseInt(dtm.substr(6,2),10);

        // date validity checks: must be integers, month 1-12, day 1-31
        if (isNaN(year) || isNaN(month) || isNaN(day) )
        {
            return "Invalid DTM '"+dtm+"': year, month, or day not a number in field '"+where+"'";
        }

        if (month < 1 || month > 12)
        {
            return "Invalid DTM '"+dtm+"': bad month '"+dtm.substr(4,2)+"' in field '"+where+"'";
        }
        if (day < 1 || day > 31)
        {
            return "Invalid DTM '"+dtm+": bad day '"+dtm.substr(6,2)+"' in field '"+where+"'";
        }

        var result = dtm.substr(0,4) + "-" + dtm.substr(4,2) + "-" + dtm.substr(6,2);

        if (dtm.length < 10)
            return result;

        // add HH
        var HH;
        if (dtm.substr(8,1)=='0')
            HH = parseInt(dtm.substr(9,1),10);
        else if (dtm.substr(8,1)==' ')
		HH = 0;
        else
            HH = parseInt(dtm.substr(8,2),10);

        if (isNaN(HH))
        {
            return "Invalid DTM '"+dtm+"': bad hour (NaN) '"+dtm.substr(8,2)+"' in field '"+where+"'";
        }
        if (HH < 0 || HH > 23)
        {
            return "Invalid DTM '"+dtm+"': bad hour '"+dtm.substr(8,2)+"' in field '"+where+"'";
        }

	  if (HH == 0)
	        result = result + " " + "00";
	  else
	        result = result + " " + dtm.substr(8,2);

        if (dtm.length < 12)
           return result;

        // add MM
        var MM;
        if (dtm.substr(10,1)=='0')
            MM = parseInt(dtm.substr(11,1),10);
        else if (dtm.substr(10,1)==' ')
		MM = 0;       
	  else
            MM = parseInt(dtm.substr(10,2),10);
        if (isNaN(MM))
        {
            return "Invalid DTM '"+dtm+"': bad minutes (NaN) '"+dtm.substr(10,2)+"' in field '"+where+"'";
        }
        if (MM < 0 || MM > 59)
        {
            return "Invalid DTM '"+dtm+"': bad minutes '"+dtm.substr(10,2)+"' in field '"+where+"'";
        }

	  if (MM == 0)
	        result = result + ":" + "00";
	  else
	        result = result + ":" + dtm.substr(10,2);

        if (dtm.length < 14)
           return result;

        // add SS
        var SS;
        if (dtm.substr(12,1)=='0')
            SS = parseInt(dtm.substr(13,1),10);
        else if (dtm.substr(12,1)==' ')
		SS = 0;
        else
            SS = parseInt(dtm.substr(12,2),10);
        if (isNaN(SS))
        {
            return "Invalid DTM '"+dtm+"': bad seconds (NaN) '"+dtm.substr(12,2)+"' in field '"+where+"'";
        }
        if (SS < 0 || SS > 59)
        {
            return "Invalid DTM '"+dtm+"': bad seconds '"+dtm.substr(12,2)+"' in field '"+where+"'";
        }
	
	  if (SS == 0)
	        result = result + ":" + "00";
	  else
	        result = result + ":" + dtm.substr(12,2);

        return result;
    }

    // return the current time properly formatted
    function getNow()
    {
         var now;
         now = new Date();
         var text = "";
         text = text + now.getFullYear();
         text = text + "-";
         var month = now.getMonth();
         month += 1;
         if (month < 10)
         {
            text = text + "0";
         }
         text = text + month;
         text = text + "-";
         if (now.getDate() < 10)
         {
            text = text + "0";
         }
         text = text + now.getDate();

        // add time
         var hour = now.getHours();
         var min = now.getMinutes();
         var sec = now.getSeconds();

         var c = ":";
         text += " ";
         if (hour < 10)
            text += "0";
         text += hour + c;
         if (min < 10)
            text += "0";
         text += min + c;
         if (sec < 10)
            text += "0";
         text += sec;
  
         return text;
    }

    // remove the leading and trailing spaces from a string
    function removeWS(str)
    {
        var res;
        var tmp;

        re = /^ */;
        tmp = str.replace(re,"");
        re2 = / *$/;
        res = tmp.replace(re2,"");
        return res;
    }

    // remove the leading zeros from a string
    function removeZero(str)
    {
        var res;
        var tmp;

        re = /^0*/;
        res = str.replace(re,"");
        return res;
    }

    //Based on inbound parameters (birth year, birth month [1=Jan], and birth day) this
    //function returns days old (age in days)
    function ageInDays(bYear, bMonth, bDay)
    {
	  today = new Date();
	  birthDate = new Date();

  	  birthDate.setFullYear(bYear);
	  //JScript associates January with zero in this method (so subtract one from bMonth)
	  birthDate.setMonth(bMonth-1);
	  birthDate.setDate(bDay);

	  //Subtract the:
	  //Millisecond differenece from now and Jan. 1, 1970 (JScript default)
	  //Millisecond differenece from birthdate and Jan. 1, 1970 (JScript default)
	  interval = today.getTime() - birthDate.getTime();	//difference in ms
	  //Turn milliseconds into day figure and use normal rounding
	  ageInDays = Math.round(interval/86400000);
	
	  //Return the integer day value
	  return ageInDays;
    }

    ]]>
  </msxsl:script>

<!-- global variables -->

<xsl:variable name="msgAction">
  <xsl:value-of select="cbord:startup()"/>
  <xsl:call-template name="extractAction"/>
</xsl:variable>

<!-- START of processing: if ok to process this message, Extract Data -->
<xsl:template match="/">
  <ADTORDLIST>
  
	<xsl:for-each select="/*/PID">
		<xsl:variable name="pos" select="position()"/>
		<xsl:call-template name="extractADTORD">
			<xsl:with-param name="pos" select="$pos"/>
		</xsl:call-template>
	</xsl:for-each>
	
	<!-- extractADTORD is called again to process an ADT based 
	Service Order Message if the following conditions are met. -->
	<xsl:if test="/*/MSH/MSH.9/CM_MSG_TYPE.2='A08' and /*/ADT_A08.LST.OBX/OBX/OBX.5.LST/OBX.5/CE.1='RS' 
			and $useDietaryService='YES' and $dietaryServiceInADT='YES'">
		<xsl:variable name="posServ" select="position()"/>
		<xsl:variable name="adtServiceOrder" select="'YES'"/>
		<xsl:call-template name="extractADTORD">
			<xsl:with-param name="pos" select="$posServ"/>
			<xsl:with-param name="adtServiceOrder" select="$adtServiceOrder"/>
		</xsl:call-template>
	</xsl:if>
	
  </ADTORDLIST>
</xsl:template>

<xsl:template name="extractADTORD">
  <xsl:param name="pos"/>
  <xsl:param name="adtServiceOrder"/>
  <ADTORD>
	<OriginalMsgInfo>
		<!-- EXTRACT HEADER INFO FOR logging -->	
		<!-- Extract Message DTM -->	
		<DTM><xsl:value-of select="/*/MSH/MSH.7"/></DTM>

		<!-- Extract Message Type -->
		<msgType><xsl:value-of select="/*/MSH/MSH.9/CM_MSG_TYPE.1"/></msgType>

		<!-- Extract Event Type -->
		<Type><xsl:value-of select="/*/MSH/MSH.9/CM_MSG_TYPE.2"/></Type>

		<!-- Extract Sequence Number -->
		<SequenceNumber><xsl:value-of select="/*/MSH/MSH.13"/></SequenceNumber>
	</OriginalMsgInfo>
	<operation>			
		<!-- Extract Message Action -->
		<!-- Extracts the message action  and checks if this is the second run for a Service ADT message -->
		<xsl:choose>
			<xsl:when test="$adtServiceOrder='YES'">
				<action><xsl:value-of select="'OrderEnter'"/></action>
			</xsl:when>
			<xsl:otherwise>
				<action><xsl:value-of select="$msgAction"/></action>
			</xsl:otherwise>
		</xsl:choose>

		<!-- Extract Facility Name -->
		<!-- Extract facility name if used and valued, otherwise set to 'NONE' -->
		<xsl:choose>
			<xsl:when test="$usingFacilityNames='YES' and /*/MSH/MSH.6/HD.1">
				<facility><xsl:value-of select="/*/MSH/MSH.6/HD.1"/></facility>
			</xsl:when>
			<xsl:otherwise>
				<facility><xsl:value-of select="'NONE'"/></facility>
			</xsl:otherwise>
		</xsl:choose>

		<!-- Extract Transaction DTM -->
		<xsl:if test="/*/MSH/MSH.7">
			<transactionDTM>
				<xsl:variable name="transactionDTM" select="cbord:formatHL7DTM('Message Header DTM (MSH.7)',string(/*/MSH/MSH.7))"/>
				<xsl:choose>
					<xsl:when test="contains($transactionDTM, 'Invalid')">
						<WARNING><xsl:value-of select="$transactionDTM"/></WARNING>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$transactionDTM"/>
					</xsl:otherwise>
				</xsl:choose>
			</transactionDTM>		
		</xsl:if>
	</operation>
	<filterInfo>
		<!-- Extract Sending Aplication -->
		<sendingApplication><xsl:value-of select="/*/MSH/MSH.3/HD.1"/></sendingApplication>

		<!-- Extract Sending Facility -->
		<sendingFacility><xsl:value-of select="/*/MSH/MSH.4/HD.1"/></sendingFacility>

		<!-- Extract Receiving Aplication -->
		<receivingApplication><xsl:value-of select="/*/MSH/MSH.5/HD.1"/></receivingApplication>

		<!-- Extract Receiving Facility -->
		<receivingFacility><xsl:value-of select="/*/MSH/MSH.6/HD.1"/></receivingFacility>

		<!-- Extract Patient Class -->
		<patientClass><xsl:value-of select="/*/PV1[position()=$pos]/PV1.2"/></patientClass>

		<!-- Extract Allergy Type/Types -->
		<!-- Extract allergy type if using allergies, message type is not A17, and AL1 segment (default) exists -->
		<xsl:if test="$usingAllergies='YES' and /*/MSH/MSH.9/CM_MSG_TYPE.2!='A17' and //AL1.2">
			<!-- Create an Allergy Type List if allergy type data exists in AL1.2 (default) -->
			<AllergyTypeList>
				<!-- Extract Allergy Type for every given AL1 (default) segment -->
				<xsl:for-each select="//AL1.2">
					<AllergyType><xsl:value-of select="."/></AllergyType>
				</xsl:for-each>
			</AllergyTypeList>
		</xsl:if>

		<!-- Extract Merge Assigning Facility -->
		<xsl:if test="/*/MRG/MRG.1/CX.6">
			<mrgAssigningFacility><xsl:value-of select="/*/MRG/MRG.1/CX.6"/></mrgAssigningFacility>
		</xsl:if>
	</filterInfo>
	<personInfo>
		<!-- Extract Gender -->
		<xsl:if test="/*/PID[position()=$pos]/PID.8">
			<sex><xsl:value-of select="/*/PID[position()=$pos]/PID.8"/></sex>
		</xsl:if>

		<!-- Extract Lactating -->
		<!-- Might have multiple OBX (default) Lists (swap) : For each OBX (default) list -->
		<xsl:for-each select="//*[OBX][position()=$pos]">
			<!-- For each OBX within the OBX list -->
			<xsl:for-each select="OBX">
				<!-- Value of 'lactating' if OBX.3/CE.1 contains string 'lactating' -->
				<xsl:if test="cbord:stringToLower(string(./OBX.3/CE.1))='lactating'">
					<lactating><xsl:value-of select="'Yes'"/></lactating>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>

		<!-- Extract Pregnant -->
		<pregnant><xsl:value-of select="/*/PV1[position()=$pos]/PV1.15.LST/PV1.15"/></pregnant>

		<!-- Extract VIP -->
		<vip><xsl:value-of select="/*/PV1[position()=$pos]/PV1.16"/></vip>

		<!-- Extract Insulin -->
		<insulin><xsl:value-of select="/*/PV1[position()=$pos]/PV1.38/CE.1"/></insulin>

		<!-- Extract MRN -->
		<MRN>
			<xsl:if test="$MRNRemoveLeadZero='YES'">
				<xsl:value-of select="cbord:removeZero(string(/*/PID[position()=$pos]/PID.3.LST/PID.3/CX.1))"/>
			</xsl:if>
			<xsl:if test="$MRNRemoveLeadZero='NO'">
				<xsl:value-of select="/*/PID[position()=$pos]/PID.3.LST/PID.3/CX.1"/>
			</xsl:if>
		</MRN>

		<!-- Extract Name -->
		<name>
			<!-- Extract last name -->
			<xsl:if test="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.1">
				<lastName><xsl:value-of select="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.1"/></lastName>
			</xsl:if>

			<!-- Extract first name -->
			<xsl:if test="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.2">
				<firstName><xsl:value-of select="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.2"/></firstName>
			</xsl:if>

			<!-- Extract middle name -->
			<xsl:if test="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.3">
				<middleName><xsl:value-of select="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.3"/></middleName>
			</xsl:if>
		
			<!-- Extract patient suffix if not ignored -->
			<xsl:if test="$ignorePatientSuffix='NO'">
				<xsl:if test="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.4">
					<suffix><xsl:value-of select="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.4"/></suffix>
				</xsl:if>
			</xsl:if>

			<!-- Extract nick name -->
			<xsl:if test="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.5">
				<nickName><xsl:value-of select="/*/PID[position()=$pos]/PID.5.LST/PID.5/XPN.5"/></nickName>
			</xsl:if>
		</name>

		<!-- Extract Date of Birth -->
		<xsl:if test="/*/PID[position()=$pos]/PID.7">
			<DOB>
				<xsl:variable name="dobDTM" select="cbord:formatHL7DTM('Date of Birth (PID.7)',string(/*/PID[position()=$pos]/PID.7))"/>
				<xsl:choose>
					<xsl:when test="contains($dobDTM, 'Invalid')">
						<WARNING><xsl:value-of select="$dobDTM"/></WARNING>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$dobDTM"/>
					</xsl:otherwise>
				</xsl:choose>
			</DOB>
		</xsl:if>

		<!-- If using age-based diets and message is ORM, determine age in days (based on dob) -->
		<xsl:if test="$useAgeBasedDiets='YES' and /ORM_O01/*/ORC[1]/ORC.1='NW'">
			<xsl:variable name="dobDTM" select="cbord:formatHL7DTM('Date of Birth (PID.7)',string(/*/PID[position()=$pos]/PID.7))"/>

			<xsl:choose>
				<!-- No DOB or invalid DOB in HL7 message -->
				<xsl:when test="not(/*/PID[position()=$pos]/PID.7) or contains($dobDTM, 'Invalid')">
					<ageIntervals>
						<WARNING><xsl:value-of select="'Age-based interfacing enabled. Invalid or missing date of birth in HL7 order message.'"/></WARNING>
	
						<!-- attempt db lookup for dob -->
						<!-- db lookup for age requires facility code and MRN -->
						<!-- Ensure MRN is correctly formatted with or without leading zeros in lookup -->
						<xsl:variable name="mrn">
							<xsl:if test="$MRNRemoveLeadZero='YES'">
								<xsl:value-of select="cbord:removeZero(string(/*/PID[position()=$pos]/PID.3.LST/PID.3/CX.1))"/>
							</xsl:if>
							<xsl:if test="$MRNRemoveLeadZero='NO'">
								<xsl:value-of select="/*/PID[position()=$pos]/PID.3.LST/PID.3/CX.1"/>
							</xsl:if>	
						</xsl:variable>

						<xsl:variable name="facility">
							<xsl:choose>
								<xsl:when test="/*/MSH/MSH.6/HD.1">
									<xsl:value-of select="/*/MSH/MSH.6/HD.1"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'NONE'"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>

						<xsl:variable name="ageInDays" select="translator:age(string($facility), string($mrn))"/>	

						<xsl:choose>	
							<!-- db returned age in days -->
							<xsl:when test="$ageInDays!=''">
								<!-- associate ageInDays with appropriate age intervals -->
								<!-- This variable holds the age intervals node set (defined in ageIntervals.xml) -->
								<xsl:variable name="ageTable" select="document('ageIntervals.xml')"/>
			
								<!-- Create a node set in the xml output that will hold 
								the age intervals associated with the patient age -->
									<xsl:for-each select="$ageTable/*/interval">
										<xsl:if test="($ageInDays &gt;= ./@low) and ($ageInDays &lt;= ./@high)">
											<ageInterval><xsl:value-of select="."/></ageInterval>
										</xsl:if>
									</xsl:for-each>
							</xsl:when>

							<!-- dob in db is not valued -->								
							<xsl:otherwise>	
								<ageInterval><xsl:value-of select="'noDateOfBirth'"/></ageInterval>
								<WARNING><xsl:value-of select="'Age-based interfacing enabled. No date of birth valued in db.'"/></WARNING>
								<!-- If the order information is not going to be processed because of no available dob 
								(noDOB is set to 'NO'), then produce the extra warning -->
								<xsl:if test="$noDOB='NO'">
									<WARNING>
										<xsl:value-of select="'Order with order ID: '"/>
										<xsl:value-of select="/ORM_O01/*/ORC/ORC.2/EI.1"/>
										<xsl:value-of select="' was not proccessed.'"/>
									</WARNING>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</ageIntervals>
				</xsl:when>
		
				<!-- Use the valid DOB in HL7 message -->
				<xsl:otherwise>
					<!-- This variable holds the birth year -->
					<xsl:variable name="bYear" select="substring(/*/PID[position()=$pos]/PID.7, 1, 4)"/>
					<!-- This variable holds the birth month -->
					<xsl:variable name="bMonth" select="substring(/*/PID[position()=$pos]/PID.7, 5, 2)"/>
					<!-- This variable holds the birth day -->
					<xsl:variable name="bDay" select="substring(/*/PID[position()=$pos]/PID.7, 7, 2)"/>

					<!-- This variable holds the result of the age calculation (days old) -->
					<xsl:variable name="ageInDays" select="cbord:ageInDays($bYear, $bMonth, $bDay)"/>

					<!-- This variable holds the age intervals node set (defined in ageIntervals.xml) -->
					<xsl:variable name="ageTable" select="document('ageIntervals.xml')"/>

					<!-- Create a node set in the xml output that will hold 
					the age intervals associated with the patient age -->
					<ageIntervals>
						<xsl:for-each select="$ageTable/*/interval">
							<xsl:if test="($ageInDays &gt;= ./@low) and ($ageInDays &lt;= ./@high)">
								<ageInterval><xsl:value-of select="."/></ageInterval>
							</xsl:if>
						</xsl:for-each>
					</ageIntervals>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<!-- Extract SSN -->
		<SSN><xsl:value-of select="cbord:removeSSNHyphens(string(/*/PID[position()=$pos]/PID.19))"/></SSN>

		<!-- Extract Language -->
		<!-- Extract coded language if usingLanguage = 'YES' (in clientRules.xsl) -->
		<xsl:if test="$usingLanguage='YES' and //PID">
			<xsl:if test="/*/PID[position()=$pos]/PID.15/CE.1">
				<CodedLanguage><xsl:value-of select="/*/PID[position()=$pos]/PID.15/CE.1"/></CodedLanguage>
			</xsl:if>
		</xsl:if>
		
	</personInfo>
	<visitInfo>
		<!-- Extract BillingID -->
		<BillingID><xsl:value-of select="/*/PID[position()=$pos]/PID.18/CX.1"/></BillingID>

		<!-- Extract Patient Location -->
		<patientLocation>
			<nursingStation><xsl:value-of select="/*/PV1[position()=$pos]/PV1.3/PL.1"/></nursingStation>
			<room><xsl:value-of select="/*/PV1[position()=$pos]/PV1.3/PL.2"/></room>
			<bed><xsl:value-of select="/*/PV1[position()=$pos]/PV1.3/PL.3"/></bed>
		</patientLocation>

		<!-- Extract Physician -->
		<Physician>
			<xsl:value-of select="/*/PV1[position()=$pos]/PV1.7.LST/PV1.7/XCN.2"/>
			<xsl:if test="/*/PV1[position()=$pos]/PV1.7.LST/PV1.7/XCN.3">
				<xsl:value-of select="', '"/>
				<xsl:value-of select="/*/PV1[position()=$pos]/PV1.7.LST/PV1.7/XCN.3"/>
			</xsl:if>
		</Physician>

		<!-- Extract Admit DTM -->
		<xsl:choose>
			<xsl:when test="/*/PV1[position()=$pos]/PV1.44">
				<AdmitDTM>
					<xsl:variable name="admitPV1DTM" select="cbord:formatHL7DTM('Admit DTM (PV1.44)',string(/*/PV1[position()=$pos]/PV1.44))"/>
					<xsl:variable name="admitEVNDTM" select="cbord:formatHL7DTM('Admit DTM (EVN.2)',string(/*/EVN/EVN.2))"/>
					<xsl:choose>
						<!-- msgAction is 'Admit' and PV1 DTM is Invalid and EVN DTM is valid: create Admit tag with event DTM, and report WARNING -->
						<xsl:when test="contains($admitPV1DTM, 'Invalid') and not(contains($admitEVNDTM, 'Invalid')) and $msgAction='Admit'">
							<Admit><xsl:value-of select="$admitEVNDTM"/></Admit>
							<WARNING><xsl:value-of select="$admitPV1DTM"/></WARNING>
						</xsl:when>
						<!-- msgAction is 'Admit' and PV1 DTM is Invalid and EVN DTM is Invalid: create Admit tag with system DTM, and report WARNING -->
						<xsl:when test="contains($admitPV1DTM, 'Invalid') and contains($admitEVNDTM, 'Invalid') and $msgAction='Admit'">
							<Admit><xsl:value-of select="cbord:getNow()"/></Admit>
							<WARNING><xsl:value-of select="$admitPV1DTM"/></WARNING>
							<WARNING><xsl:value-of select="$admitEVNDTM"/></WARNING>
						</xsl:when>
						<!-- msgAction is not 'Admit' and DTM is Invalid: report WARNING -->
						<xsl:when test="contains($admitPV1DTM, 'Invalid') and $msgAction!='Admit'">
							<WARNING><xsl:value-of select="$admitPV1DTM"/></WARNING>
						</xsl:when>
						<!-- DTM is valid: use it -->
						<xsl:otherwise>
							<xsl:value-of select="$admitPV1DTM"/>
						</xsl:otherwise>
					</xsl:choose>
				</AdmitDTM>
			</xsl:when>
			<xsl:when test="/*/EVN/EVN.2 and $msgAction='Admit'">
				<AdmitDTM>
					<xsl:variable name="admitEVNDTM" select="cbord:formatHL7DTM('Admit DTM (EVN.2)',string(/*/EVN/EVN.2))"/>
					<xsl:choose>
						<!-- msgAction is 'Admit' and EVN DTM is Invalid: create Admit tag with system DTM, and report WARNING -->
						<xsl:when test="contains($admitEVNDTM, 'Invalid') and $msgAction='Admit'">
							<Admit><xsl:value-of select="cbord:getNow()"/></Admit>
							<WARNING><xsl:value-of select="$admitEVNDTM"/></WARNING>
						</xsl:when>
						<!-- DTM is valid: use it -->
						<xsl:otherwise>
							<xsl:value-of select="$admitEVNDTM"/>
						</xsl:otherwise>
					</xsl:choose>
				</AdmitDTM>
			</xsl:when>
			<xsl:when test="$msgAction='Admit'">
				<AdmitDTM>
					<xsl:value-of select="cbord:getNow()"/>
				</AdmitDTM>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
			
		<!-- Extract Discharge DTM -->		
		<xsl:choose>
			<xsl:when test="/*/PV1[position()=$pos]/PV1.45">
				<DischargeDTM>
					<xsl:variable name="dischargePV1DTM" select="cbord:formatHL7DTM('Discharge DTM (PV1.45)',string(/*/PV1[position()=$pos]/PV1.45))"/>
					<xsl:variable name="dischargeEVNDTM" select="cbord:formatHL7DTM('Discharge DTM (EVN.2)',string(/*/EVN/EVN.2))"/>
					<xsl:choose>
						<!-- msgAction is 'Discharge' and PV1 DTM is Invalid and EVN DTM is valid: create Discharge tag with event DTM, and report WARNING -->
						<xsl:when test="contains($dischargePV1DTM, 'Invalid') and not(contains($dischargeEVNDTM, 'Invalid')) and $msgAction='Discharge'">
							<Discharge><xsl:value-of select="$dischargeEVNDTM"/></Discharge>
							<WARNING><xsl:value-of select="$dischargePV1DTM"/></WARNING>
						</xsl:when>
						<!-- msgAction is 'Discharge' and PV1 DTM is Invalid and EVN DTM is Invalid: create Discharge tag with system DTM, and report WARNING -->
						<xsl:when test="contains($dischargePV1DTM, 'Invalid') and contains($dischargeEVNDTM, 'Invalid') and $msgAction='Discharge'">
							<Discharge><xsl:value-of select="cbord:getNow()"/></Discharge>
							<WARNING><xsl:value-of select="$dischargePV1DTM"/></WARNING>
							<WARNING><xsl:value-of select="$dischargeEVNDTM"/></WARNING>
						</xsl:when>
						<!-- msgAction is not 'Discharge' and DTM is Invalid: report WARNING -->
						<xsl:when test="contains($dischargePV1DTM, 'Invalid') and $msgAction!='Discharge'">
							<WARNING><xsl:value-of select="$dischargePV1DTM"/></WARNING>
						</xsl:when>
						<!-- DTM is valid: use it -->
						<xsl:otherwise>
							<xsl:value-of select="$dischargePV1DTM"/>
						</xsl:otherwise>
					</xsl:choose>
				</DischargeDTM>
			</xsl:when>
			<xsl:when test="/*/EVN/EVN.2 and $msgAction='Discharge'">
				<DischargeDTM>
					<xsl:variable name="dischargeEVNDTM" select="cbord:formatHL7DTM('Discharge DTM (EVN.2)',string(/*/EVN/EVN.2))"/>
					<xsl:choose>
						<!-- msgAction is 'Discharge' and EVN DTM is Invalid: create Discharge tag with system DTM, and report WARNING -->
						<xsl:when test="contains($dischargeEVNDTM, 'Invalid') and $msgAction='Discharge'">
							<Discharge><xsl:value-of select="cbord:getNow()"/></Discharge>
							<WARNING><xsl:value-of select="$dischargeEVNDTM"/></WARNING>
						</xsl:when>
						<!-- DTM is valid: use it -->
						<xsl:otherwise>
							<xsl:value-of select="$dischargeEVNDTM"/>
						</xsl:otherwise>
					</xsl:choose>
				</DischargeDTM>
			</xsl:when>
			<xsl:when test="$msgAction='Discharge'">
				<DischargeDTM>
					<xsl:value-of select="cbord:getNow()"/>
				</DischargeDTM>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>

		<!-- Extract Admit Diagnosis (free text) -->
		<xsl:if test="//DG1/DG1.4">
			<AdmitDiag><xsl:value-of select="cbord:getHL7text(string(//DG1/DG1.4))"/></AdmitDiag>
		</xsl:if>

		<!-- Extract Diagnosis Start Date and Time -->
		<!-- Extract DiagStartDTM if usingCodedDiag = 'YES' (in clientRules.xsl) and DG1 segment in message -->
		<xsl:if test="$usingCodedDiag='YES' and //DG1">
			<!-- Choose where to pull DiagStartDTM from -->
			<xsl:choose>
				<!-- Extract from Default (DG1.5) if valued -->
				<xsl:when test="//DG1[1]/DG1.5">
					<DiagStartDTM>
						<xsl:variable name="diagStartDTM" select="cbord:formatHL7DTM('Diagnoiss Start DTM (DG1.5)',string(//DG1[1]/DG1.5))"/>
						<xsl:variable name="diagStartMSHDTM" select="cbord:formatHL7DTM('Diagnoiss Start DTM (MSH.7)',string(/*/MSH/MSH.7))"/>
						<xsl:choose>
							<xsl:when test="contains($diagStartDTM, 'Invalid') and not(contains($diagStartMSHDTM, 'Invalid'))">
								<DiagStart><xsl:value-of select="$diagStartMSHDTM"/></DiagStart>
								<WARNING><xsl:value-of select="$diagStartDTM"/></WARNING>
							</xsl:when>
							<xsl:when test="contains($diagStartDTM, 'Invalid') and contains($diagStartMSHDTM, 'Invalid')">
								<DiagStart><xsl:value-of select="cbord:getNow()"/></DiagStart>
								<WARNING><xsl:value-of select="$diagStartDTM"/></WARNING>
								<WARNING><xsl:value-of select="$diagStartMSHDTM"/></WARNING>
							</xsl:when>
							<xsl:otherwise>
								<DiagStart><xsl:value-of select="$diagStartDTM"/></DiagStart>
							</xsl:otherwise>
						</xsl:choose>
					</DiagStartDTM>
				</xsl:when>

				<!-- DiagStartDTM not valued, use Message DTM (default location = MSH.7) -->
				<xsl:when test="/*/MSH/MSH.7">
					<DiagStartDTM>
						<xsl:variable name="diagStartDTM" select="cbord:formatHL7DTM('Diagnoiss Start DTM (MSH.7)',string(/*/MSH/MSH.7))"/>
						<xsl:choose>
							<xsl:when test="contains($diagStartDTM, 'Invalid')">
								<DiagStart><xsl:value-of select="cbord:getNow()"/></DiagStart>
								<WARNING><xsl:value-of select="$diagStartDTM"/></WARNING>
							</xsl:when>
							<xsl:otherwise>
								<DiagStart><xsl:value-of select="$diagStartDTM"/></DiagStart>
							</xsl:otherwise>
						</xsl:choose>
					</DiagStartDTM>
				</xsl:when>
				
				<!-- DiagStartDTM not valued, Message DTM not valued, Use System DTM -->
				<xsl:otherwise>
					<DiagStartDTM>
						<DiagStart><xsl:value-of select="cbord:getNow()"/></DiagStart>
					</DiagStartDTM>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<!-- Extract Coded Diagnosis -->
		<!-- Extract Coded Diagnosis list if usingCodedDiag = 'YES' (in clientRules.xsl) -->
		<xsl:if test="$usingCodedDiag='YES' and //DG1">
			<CodedDiagList>
				<!-- Extract Diagnosis Code for each DG1 segment -->
				<xsl:for-each select="//DG1">
					<!-- Extract coded diagnosis if Diagnosis Code location contains data within DG1 segment -->
					<xsl:if test="./DG1.3/CE.1">
						<CodedDiag><xsl:value-of select="./DG1.3/CE.1"/></CodedDiag>
					</xsl:if>
				</xsl:for-each>
			</CodedDiagList>
		</xsl:if>
			
		<!-- Extract ProtocolID -->
		<!-- Extract ProtocolID list if usingProtocolID = 'YES' (in clientRules.xsl) -->
		<xsl:if test="$usingProtocolID='YES' and //PV2">
			<!-- Extract ProtocolID for each PV2 segment -->
			<!-- (suspended rule) xsl:for-each select="//PV2"-->
			<!-- Extract coded diagnosis if Protocol Code location contains data within DG1 segment -->
			<xsl:if test="/*/PV2[position()=$pos]/PV2.12/CE.1">
				<ProtocolID><xsl:value-of select="/*/PV2[position()=$pos]/PV2.12/CE.1"/></ProtocolID>
			</xsl:if>
			<!-- /xsl:for-each-->
		</xsl:if>
		
		<!-- Extract Coded Allergies -->
		<!-- Extract allergy code if using allergies, message type is not swap, and AL1 segment (default) exists -->
		<!-- Allows multiple allergies within a single AL1, seperated by repetition character -->
		<xsl:if test="$usingAllergies='YES' and /*/MSH/MSH.9/CM_MSG_TYPE.2!='A17' and //AL1">
		     <AllergyList>
		          <!-- Extract coded allergy/allergies for each AL1 segment -->
		          <xsl:for-each select="//AL1">
			       <xsl:choose>
				 <!-- Extract coded allergy if allergy code location contains data within AL1 segment -->
		                 <xsl:when test="cbord:stringToLower(string(./AL1.2.LST/AL1.2))='fa' and ./AL1.3.LST/AL1.3/CE.1">
			           <xsl:for-each select="./AL1.3.LST/AL1.3/CE.1">
					<xsl:choose>
						<xsl:when test=".=$AllergyRemovalTrigger">
							<Allergy>-1</Allergy>
						</xsl:when>
						<xsl:otherwise>
							<Allergy><xsl:value-of select="."/></Allergy>
						</xsl:otherwise>
						</xsl:choose>
					 </xsl:for-each>	    
				   </xsl:when>
  				   <xsl:otherwise>
					<xsl:if test="$AllergyRemovalTrigger='EMPTYFIELD'">
						<Allergy>-1</Allergy>
					</xsl:if>
				   </xsl:otherwise>
				</xsl:choose>
		          </xsl:for-each>
		     </AllergyList>
		</xsl:if>

		<!-- Extract Anthro Info -->
		<xsl:for-each select="//*[OBX][position()=$pos]">
		     <!-- Extract Anthro Info if observation identifier of either 'ht' or 'wt' is found and Anthro Data is used according to clientRules.xsl -->
		     <xsl:if test="(//OBX.5[cbord:stringToLower(string(CE.1))='ht'] or //OBX.5[cbord:stringToLower(string(CE.1))='wt']) and $usingAnthroData='YES'">
			  <anthroInfo>
			     <Frame><xsl:value-of select="'Medium'"/></Frame>

				<!-- Extract Anthro DTM -->
				<!-- If no value is provided in OBX.14 (default), check EVN.2 (default Event DTM) -->
				<!-- If no value is provided in EVN.2 (default Event DTM), use System DTM -->
				<xsl:choose>
					<!-- Extract anthro dtm when OBX (default) has 'ht' or 'wt' observation identifier, and OBX.14 (default) exists -->
					<xsl:when test="(cbord:stringToLower(string(./OBX/OBX.5.LST/OBX.5/CE.1))='ht' or cbord:stringToLower(string(./OBX/OBX.5.LST/OBX.5/CE.1))='wt') and ./OBX/OBX.14">
						<anthroStartDTM>
							<xsl:variable name="anthroStartDTM" select="cbord:formatHL7DTM('Observation DTM (OBX.14)',string(./OBX/OBX.14))"/>
							<xsl:choose>
								<xsl:when test="contains($anthroStartDTM, 'Invalid')">
									<WARNING><xsl:value-of select="$anthroStartDTM"/></WARNING>
									<xsl:choose>
										<xsl:when test="/*/EVN/EVN.2">
											<xsl:variable name="anthroStartDTMEVN" select="cbord:formatHL7DTM('Observation DTM (EVN.2)',string(/*/EVN/EVN.2))"/>
											<xsl:choose>
												<xsl:when test="contains($anthroStartDTMEVN, 'Invalid')">
													<WARNING><xsl:value-of select="$anthroStartDTMEVN"/></WARNING>
													<StartDTM><xsl:value-of select="cbord:getNow()"/></StartDTM>
												</xsl:when>
												<xsl:otherwise>
													<StartDTM><xsl:value-of select="$anthroStartDTMEVN"/></StartDTM>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:otherwise><StartDTM><xsl:value-of select="cbord:getNow()"/></StartDTM></xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<StartDTM><xsl:value-of select="$anthroStartDTM"/></StartDTM>
								</xsl:otherwise>
							</xsl:choose>
						</anthroStartDTM>	
					</xsl:when>
					<!-- Use event dtm (EVN.2 default) is valued -->
					<xsl:when test="/*/EVN/EVN.2">
						<anthroStartDTM>
							<xsl:variable name="anthroStartDTM" select="cbord:formatHL7DTM('Observation DTM (EVN.2)',string(/*/EVN/EVN.2))"/>
							<xsl:choose>
								<xsl:when test="contains($anthroStartDTM, 'Invalid')">
									<WARNING><xsl:value-of select="$anthroStartDTM"/></WARNING>
									<StartDTM><xsl:value-of select="cbord:getNow()"/></StartDTM>
								</xsl:when>
								<xsl:otherwise>
									<StartDTM><xsl:value-of select="$anthroStartDTM"/></StartDTM>
								</xsl:otherwise>
							</xsl:choose>
						</anthroStartDTM>	
					</xsl:when>							
					<!-- Give up, Use System DTM -->
					<xsl:otherwise>
						<anthroStartDTM><StartDTM><xsl:value-of select="cbord:getNow()"/></StartDTM></anthroStartDTM>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:for-each select="OBX">
				   <xsl:if test="cbord:stringToLower(string(./OBX.5.LST/OBX.5/CE.1))='ht'">
					<Height>
						<!-- Extract height unit of measure -->
						<!-- Extract height unit of measure to be the height tag attribute -->
						<!-- If no value is specified in OBX.6/CE.1 or OBX.6/CE.2 (default locations), use default in clientRules.xsl -->
						<xsl:attribute name="HeightUOFM">
							<xsl:choose>
								<!-- Use 'IN' if the OBX.6/CE.1 (default) contains 'in' or 'inch' or 'inches' -->
								<xsl:when test="cbord:stringToLower(string(./OBX.6/CE.1)) = 'in' or cbord:stringToLower(string(./OBX.6/CE.1)) = 'inch' or cbord:stringToLower(string(./OBX.6/CE.1)) = 'inches'">
									<xsl:value-of select="'IN'"/>
								</xsl:when>
								<!-- OBX.6/CE.2 is another default location -->
								<xsl:when test="cbord:stringToLower(string(./OBX.6/CE.2)) = 'in' or cbord:stringToLower(string(./OBX.6/CE.2)) = 'inch' or cbord:stringToLower(string(./OBX.6/CE.2)) = 'inches'">
									<xsl:value-of select="'IN'"/>
								</xsl:when>
								<!-- Use 'CM' if the OBX.6/CE.1 (default) contains 'cm' or 'centimeters' -->
								<xsl:when test="cbord:stringToLower(string(./OBX.6/CE.1)) = 'cm' or cbord:stringToLower(string(./OBX.6/CE.1)) = 'centimeters'">
									<xsl:value-of select="'CM'"/>
								</xsl:when>
								<!-- OBX.6/CE.2 is another default location -->
								<xsl:when test="cbord:stringToLower(string(./OBX.6/CE.2)) = 'cm' or cbord:stringToLower(string(./OBX.6/CE.2)) = 'centimeters'">
									<xsl:value-of select="'CM'"/>
								</xsl:when>
								<!-- Give up, use clientRules.xsl value -->
								<xsl:otherwise>
									<xsl:value-of select="$defaultUOFMheight"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:value-of select="./OBX.5.LST/OBX.5/CE.3"/>
					</Height>
				</xsl:if>

				<!-- Extract Weight -->
				<!-- Extract weight data for observation identifiers of 'wt' -->
				<xsl:if test="cbord:stringToLower(string(./OBX.5.LST/OBX.5/CE.1))='wt'">
					<Weight>
						<!-- Extract weight unit of measure -->
						<!-- Extract weight unit of measure to be the weight tag attribute -->
						<!-- If no value is specified in OBX.6/CE.1 or OBX.6/CE.2 (default locations), use default in clientRules.xsl -->
						<xsl:attribute name="WeightUOFM">
							<xsl:choose>
								<!-- Use 'LB' if the OBX.6/CE.1 (default) contains 'lb' or 'pounds' -->
								<xsl:when test="cbord:stringToLower(string(./OBX.6/CE.1)) = 'lb' or cbord:stringToLower(string(./OBX.6/CE.1)) = 'pounds'">
									<xsl:value-of select="'LB'"/>
								</xsl:when>
								<!-- OBX.6/CE.2 is another default location -->
								<xsl:when test="cbord:stringToLower(string(./OBX.6/CE.2)) = 'lb' or cbord:stringToLower(string(./OBX.6/CE.2)) = 'pounds'">
									<xsl:value-of select="'LB'"/>
								</xsl:when>
								<!-- Use 'KG' if the OBX.6/CE.1 (default) contains 'kg' or 'kilograms' -->
								<xsl:when test="cbord:stringToLower(string(./OBX.6/CE.1)) = 'kg' or cbord:stringToLower(string(./OBX.6/CE.1)) = 'kilograms'">
									<xsl:value-of select="'KG'"/>
								</xsl:when>
								<!-- OBX.6/CE.2 is another default location -->
								<xsl:when test="cbord:stringToLower(string(./OBX.6/CE.2)) = 'kg' or cbord:stringToLower(string(./OBX.6/CE.2)) = 'kilograms'">
									<xsl:value-of select="'KG'"/>
								</xsl:when>
								<!-- Give up, use clientRules.xsl value -->
								<xsl:otherwise>
									<xsl:value-of select="$defaultUOFMweight"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:value-of select="./OBX.5.LST/OBX.5/CE.3"/>
					</Weight>
				</xsl:if>
			   </xsl:for-each>
			</anthroInfo>
		</xsl:if>
         </xsl:for-each>
	</visitInfo>

	<!-- Extract Merge Info -->
	<!-- Extract Merge Info if MRG (default) is in message -->
  	<xsl:if test="/*/MRG">
		<mergeInfo>
			<xsl:choose>		
				<!-- Extract Prior MRN -->
				<!-- Extract Prior MRN if data is valued in MRG.1/CX.1 (default) -->
				<xsl:when test="/*/MRG/MRG.1/CX.1">
					<priorMRN>
						<xsl:if test="$MRNRemoveLeadZero='YES'">
							<xsl:value-of select="cbord:removeZero(string(/*/MRG[position()=$pos]/MRG.1/CX.1))"/>
						</xsl:if>	
						<xsl:if test="$MRNRemoveLeadZero='NO'">
							<xsl:value-of select="/*/MRG[position()=$pos]/MRG.1/CX.1"/>
						</xsl:if>
					</priorMRN>
				</xsl:when>

				<!-- Alternate location for Prior MRN -->
				<xsl:when test="/*/MRG/MRG.4/CX.1">
					<priorMRN>
						<xsl:if test="$MRNRemoveLeadZero='YES'">
							<xsl:value-of select="cbord:removeZero(string(/*/MRG[position()=$pos]/MRG.4/CX.1))"/>
						</xsl:if>
						<xsl:if test="$MRNRemoveLeadZero='NO'">
							<xsl:value-of select="/*/MRG[position()=$pos]/MRG.4/CX.1"/>
						</xsl:if>
					</priorMRN>
				</xsl:when>
	
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>

			<!-- Extract Prior BillingID -->
			<!-- Extract Prior BillingID if data is valued in MRG.3/CX.1 (default) -->
			<xsl:if test="/*/MRG/MRG.3/CX.1">
				<priorBillingID><xsl:value-of select="/*/MRG/MRG.3/CX.1"/></priorBillingID>
			</xsl:if>

		</mergeInfo>
	</xsl:if>


	<!-- extract order information.  Note, for ADT only interfaces this will always be empty -->
	<xsl:if test="/ORM_O01 or /ORM_O09 or /OMP_O09">
		<xsl:call-template name="extractOrderInfo">
			<xsl:with-param name="MSGList" select=".."/>
		</xsl:call-template>	
	</xsl:if>

	<!-- Enter a Dietary Service Order from an ADT message -->
	<xsl:if test="$adtServiceOrder='YES'">
		<xsl:call-template name="serviceOrderADT"/>
	</xsl:if>

  </ADTORD>
</xsl:template>

<!-- ADT: Map a message root tag into a NSS update action -->
<xsl:template name="extractAction">
  <xsl:choose>
    <!-- Admit message types -->
    <xsl:when test="boolean(/ADT_A01) or boolean(/ADT_A04) or boolean(/ADT_A05) or
                    boolean(/ADT_A06) or boolean(/ADT_A22) or boolean(/ADT_A10)">
      <xsl:value-of select="'Admit'"/>
    </xsl:when>
    <!-- Transfer message types -->
    <!-- Swap message (A17) treated as two transfers -->
    <xsl:when test="boolean(/ADT_A02) or boolean(/ADT_A12) or boolean(/ADT_A17)">
      <xsl:value-of select="'Transfer'"/>
    </xsl:when>
    <!-- Discharge message types -->
    <xsl:when test="boolean(/ADT_A03) or boolean(/ADT_A07) or boolean(/ADT_A11) or
                    boolean(/ADT_A21) or boolean(/ADT_A09) or boolean(/ADT_A32)">
      <xsl:value-of select="'Discharge'"/>
    </xsl:when>
    <!-- Cancel Discharge message types -->
    <xsl:when test="boolean(/ADT_A13) or boolean(/ADT_A33)">
      <xsl:value-of select="'CancelDischarge'"/>
    </xsl:when>
    <!-- Update message types -->
    <xsl:when test="boolean(/ADT_A08) or boolean(/ADT_A31)">
      <xsl:value-of select="'Update'"/>
    </xsl:when>
    <!-- Update MRN/BillingID message types -->
    <xsl:when test="boolean(/ADT_A18) or boolean(/ADT_A34) or boolean(/ADT_A35) or boolean(/ADT_A36)">
      <xsl:value-of select="'UpdateMRN'"/>
    </xsl:when>

    <!-- otherwise either filter the message or map ORDER messages, depending on implementation -->
    <xsl:otherwise>
      <xsl:call-template name="extractActionORD"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
