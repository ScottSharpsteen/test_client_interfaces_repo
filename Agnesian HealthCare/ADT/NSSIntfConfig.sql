--
-- ADT Interfaces Configuration
-- File Name:  NSSintfConfig_mss.sql
-- $Revision:   1.18  $
--
--////////////////////////////////////////////////
--
--  Converted by BW   14-Jan-2004   Initial
--  Boilerplate Configuration Data...  Do not change
--
--/////////////////////////////////////////////////

BEGIN
  DECLARE @s_facility_name VARCHAR(250)
  DECLARE @l_unitId INTEGER

  -- MUST USE UNIT 0 NAME!!  ALSO must use UNIT 0 name in Registry arg 4
  -- Someday, this may change, but for now the use of unit 0 is required
  SELECT @s_facility_name = UnitName FROM cbord.cbo3001p_bizunit WHERE UnitId=0

  -- find our unit id based on facility name
  SELECT @l_unitId = UnitId FROM cbord.cbo3001p_bizunit WHERE Unitname=@s_facility_name AND unittype='F'

  -- delete old configuration for this interface (note this relies on a cascading delete of all other entries)
  DELETE FROM cbord.cif0001p_ConfigurationSet WHERE appid='ADT' AND unitid=@l_unitId

  --////////////////////////////////////////////////
  --
  --  Configurable DB Parameters
  --
  --/////////////////////////////////////////////////
  --AutoAdmit
  --	valid parmvalues:  ALWAYS, OFF, IFNODISCHARGE, IFROOM, IFORDER  (Y, N, IFDIETORDER still work but are deprecated)
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='AutoAdmit' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms where parmname='AutoAdmit' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
       VALUES                ('AutoAdmit','IFNODISCHARGE','ADT',      @l_unitId)

  --AutoReAdmit
  --  valid parmvalues:  OFF, ALWAYS, IFNODISCHARGE, IFROOM, IFNEWBILLINGID, or IFORDER (IFDIETORDER deprecated)
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='AutoReAdmit' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms where parmname='AutoReAdmit' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
       VALUES                ('AutoReAdmit','IFNODISCHARGE','ADT',      @l_unitId)

  -- AccountNbrDefinesVisit.
  -- Set to 'Y' the interface will attempt to match on MRN/BillingID
  -- and reject transactions that do not match both.  Default is 'N'.
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='AccountNbrDefinesVisit' AND application='ADT')
  DELETE FROM cbord.cbo0001p_parms where parmname='AccountNbrDefinesVisit' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (Parmname,             parmvalue, application, unitid)
  VALUES                     ('AccountNbrDefinesVisit','Y',     'ADT',       @l_unitId)

  -- AccountNbrDefinesVisitDischarge.
  -- MRN/BillingID matching only for discharge transactions.
  -- Set to 'Y' the interface will attempt to match on MRN/BillingID
  -- and reject discharge transactions that do not match both.  Default is 'N'.
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='AccountNbrDefinesVisitDischarge' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms where parmname='AccountNbrDefinesVisitDischarge' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (Parmname,             parmvalue, application, unitid)
  VALUES ('AccountNbrDefinesVisitDischarge', 'N', 'ADT', @l_unitId)

  -- UpdatesToInactiveBillingID.
  -- Set to 'Y' or 'N'.
  -- If a tx is received that the MRN/BillingID correlates to an inactive visit,
  -- Y will allow that visit's admission information to be updated, N will reject that transaction.
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='UpdatesToInactiveBillingID' AND application='ADT')
  DELETE FROM cbord.cbo0001p_parms where parmname='UpdatesToInactiveBillingID' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (Parmname,             parmvalue, application, unitid)
  VALUES                     ('UpdatesToInactiveBillingID','N',     'ADT',       @l_unitId)

  --Coded Allergy Handling
  --     Valid Parmvalues = UNION or REPLACE
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='AllergyUpdate' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms where parmname='AllergyUpdate' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
      VALUES                ('AllergyUpdate','REPLACE','ADT',      @l_unitId)

  -- Critical data changes
  --    Valid Parmvalues = WARN, NOWARN, SUPRESS
  --    Default is WARN
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='CriticalDataUpdate' AND application='ADT')
  DELETE FROM cbord.cbo0001p_parms where parmname='CriticalDataUpdate' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (Parmname,             parmvalue, application, unitid)
  VALUES                     ('CriticalDataUpdate','WARN',     'ADT',       @l_unitId)

  -- ClearDischargedtmOnCancelDischarge: flag to control behavior of CancelDischarge message
  -- 'Y' --> clear the Discharge DTM (set to null)
  -- 'N' --> leave the Discharge DTM alone
  -- Default to 'Y'
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='ClearDischargedtmOnCancelDischarge' AND application='ADT')
      DELETE FROM cbord.cbo0001p_parms where parmname='ClearDischargedtmOnCancelDischarge' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
       VALUES                ('ClearDischargedtmOnCancelDischarge','Y','ADT',      @l_unitId)

  -- DietRestrictionsOnReadmit:  controls what happens to diet orders on a readmit
  -- 'CANCEL' - Orders are effectively canceled, left with previous admission, default
  -- 'KEEP' - Orders are copied from prior admission, status intact
  -- 'KEEPIFBILLINGIDMATCH' - Orders are copied from prior admission, status intact, only
  --                          if their is a BillingID match
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='DietRestrictionsOnReadmit' AND application='ADT')
      DELETE FROM cbord.cbo0001p_parms where parmname='DietRestrictionsOnReadmit' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname,       parmvalue,application,unitid)
        VALUES                ('DietRestrictionsOnReadmit','CANCEL','ADT',      @l_unitId)

  -- SupplementOrderSuspend:  controls whether the interface suspends all supplements.
  -- 'Never' - no suspend
  -- 'DietOrderChange' - suspend with any diet order change
  -- 'DietOrderThresholdChange' - suspend when a new order is received or cancelled that has a different
  --                              consistency selection threshold than the current effective order
  -- 'DietOrderThresholdIncrease' - suspend when a new order is received that has a higher consistency
  --                                selection threshold than the current effective order, or an order
  --                                is cancelled with a higher threshold than the current
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms WHERE parmname='SupplementOrderSuspend' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms WHERE parmname='SupplementOrderSuspend' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname, parmvalue, application, unitid)
  VALUES ('SupplementOrderSuspend', 'NEVER', 'ADT', @l_unitid)

  -- SupplementOrderSuspendAllergy:  controls whether the interface suspends all supplements on an allergy change.
  -- 'N' - Do not suspend supplements when the allergy changes
  -- 'Y' - Suspend supplements when the allergy changes
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms WHERE parmname='SupplementOrderSuspendAllergy' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms WHERE parmname='SupplementOrderSuspendAllergy' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname, parmvalue, application, unitid)
  VALUES ('SupplementOrderSuspendAllergy', 'N', 'ADT', @l_unitid)

  -- RejectDuplicateDietOrders:  controls whether the interface checks the transmitted diet order against the
  --                             currently active (non-cancelled, non-expired) diet orders for the patient,
  --                             if the ID and restriction set matches an existing order, the transmitted order
  --                             is ignored with a warning.
  -- 'N' - Do not check for diet order duplicates (default)
  -- 'Y' - Check for transmission of a duplicate order
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms WHERE parmname='RejectDuplicateDietOrders' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms WHERE parmname='RejectDuplicateDietOrders' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname, parmvalue, application, unitid)
  VALUES ('RejectDuplicateDietOrders', 'N', 'ADT', @l_unitid)

  -- DuplicateDietOrderSearchList:  When RejectDuplicateDietOrders is activated, this controls what orders are searched
  --                                for duplicates
  -- 'ALLACTIVE' - All active (non-cancelled, non-expired) orders (default)
  -- 'CURRENTEFFECTIVE' - Only the currently effective order is checked.
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms WHERE parmname='DuplicateDietOrderSearchList' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms WHERE parmname='DuplicateDietOrderSearchList' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname, parmvalue, application, unitid)
  VALUES ('DuplicateDietOrderSearchList', 'ALLACTIVE', 'ADT', @l_unitid)

  -- CancelDietOrderCancelsManualDietOrders:  When CancelDietOrderCancelsManualDietOrders is activated, all manually entered
  --                                          diet orders are canceled upon receipt of an OrderCancel for a diet order
  -- 'N' - Do not cancel manually entered diet orders (Default)
  -- 'Y' - Cancel manually entered diet orders
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms WHERE parmname='CancelDietOrderCancelsManualDietOrders' AND application='ADT')
    DELETE FROM cbord.cbo0001p_parms WHERE parmname='CancelDietOrderCancelsManualDietOrders' AND application='ADT'

  INSERT INTO cbord.cbo0001p_parms (parmname, parmvalue, application, unitid)
  VALUES ('CancelDietOrderCancelsManualDietOrders', 'N', 'ADT', @l_unitid)

  -- /////////////////////////////////////////
  -- // EOD Settings for Clinical Interfaces
  -- /////////////////////////////////////////

  -- Frequency of system even log purge  (Default:  0 = Daily)
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='PurgeFreq_sysevent' AND application='EOD')
    UPDATE cbord.cbo0001p_parms set parmvalue='0'
    WHERE parmname='PurgeFreq_sysevent' and application='EOD'
  ELSE
    INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
    VALUES ('PurgeFreq_sysevent','0','EOD',0)

  -- Frequency of application event log purge  (Default:  0 = Daily)
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='PurgeFreq_applog' AND application='EOD')
    UPDATE cbord.cbo0001p_parms set parmvalue='0'
    WHERE parmname='PurgeFreq_applog' and application='EOD'
  ELSE
    INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
    VALUES ('PurgeFreq_applog','0','EOD',0)

  -- Purge oldest ADT interface logs after how many entries  (Default:  '')
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='Genpurgeapplogadt2' AND application='EOD')
    UPDATE cbord.cbo0001p_parms set parmvalue=''
    WHERE parmname='Genpurgeapplogadt2' and application='EOD'
  ELSE
    INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
    VALUES ('Genpurgeapplogadt2','','EOD',0)

  -- Purge ADT interface logs after how many days  (Default:  7)
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='Genpurgeapplogadt' AND application='EOD')
    UPDATE cbord.cbo0001p_parms set parmvalue='7'
    WHERE parmname='Genpurgeapplogadt' and application='EOD'
  ELSE
    INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
    VALUES ('Genpurgeapplogadt','7','EOD',0)

  -- Purge oldest ADT system even logs after how many entries  (Default:  '')
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='GenPurgesyseventadt2' AND application='EOD')
    UPDATE cbord.cbo0001p_parms set parmvalue=''
    WHERE parmname='GenPurgesyseventadt2' and application='EOD'
  ELSE
    INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
    VALUES ('GenPurgesyseventadt2','','EOD',0)

  -- Purge ADT system event logs after how many days  (Default:  7)
  IF EXISTS (SELECT * FROM cbord.cbo0001p_parms where parmname='GenPurgesyseventadt' AND application='EOD')
    UPDATE cbord.cbo0001p_parms set parmvalue='7'
    WHERE parmname='GenPurgesyseventadt' and application='EOD'
  ELSE
    INSERT INTO cbord.cbo0001p_parms(parmname,parmvalue,application,unitid)
    VALUES ('GenPurgesyseventadt','7','EOD',0)

END
GO
