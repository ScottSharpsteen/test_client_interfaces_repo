﻿<?xml version="1.0"?>
<!-- Generic stylesheet for transforming ADT messages. PVCS version: $Revision:   1.35  $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:cbordRM="http://cbord.com/xsltnsRM"
                xmlns:translator="urn:translatorObject"
                version="1.0">

<xsl:import href="clientRules.xsl"/>

<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<xsl:variable name="translationTable" select="document($translationTableFileName)"/>
<xsl:variable name="translationTableAllergy" select="document($translationTableAllergyFileName)"/>
<xsl:variable name="translationTable_NursingStation_TO_Room" select="document($translationTableFileName_NursingStation_TO_Room)"/>
<xsl:variable name="translationTableSStartMeal" select="document($translationTableSStartMealFileName)"/>
<xsl:variable name="translationTableSEndMeal" select="document($translationTableSEndMealFileName)"/>
<xsl:variable name="translationTableSLocation" select="document($translationTableSLocationFileName)"/>
<xsl:variable name="translationTableSNS" select="document($translationTableSNSFileName)"/>


<!--Enabling of Translations (Valid Values:  YES or NO) -->
<xsl:variable name="Restriction_To_Restriction" select="'NO'"/>
<xsl:variable name="NursingStation_TO_NursingStation" select="'NO'"/>
<xsl:variable name="NursingStation_TO_Room" select="'NO'"/>
<xsl:variable name="Restriction_and_Allergy_TO_Restriction" select="'NO'"/>
<xsl:variable name="Allergy_TO_Allergy" select="'YES'"/>
<xsl:variable name="Supplement_TO_Supplement" select="'NO'"/>
<xsl:variable name="SupplementMealPeriod_TO_SupplementMealPeriod" select="'NO'"/>
<xsl:variable name="ServiceLocation_TO_ServiceLocation" select="'NO'"/>
<xsl:variable name="ServiceNS_TO_ServiceNS" select="'NO'"/>
<xsl:variable name="ServiceMeal_TO_ServiceMeal" select="'NO'"/>
<xsl:variable name="CodedRace_To_CodedRace" select="'NO'"/>



<xsl:template match="/">
	<xsl:choose>
		<xsl:when test="//FILTER">
			<xsl:call-template name="filterMsg"/>
		</xsl:when>
		<xsl:otherwise>
				<xsl:apply-templates select="*"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="ADTORD">
	<ADTORD>
		<xsl:apply-templates select="*"/>
	</ADTORD>
</xsl:template>

<xsl:template match="CodedRestrictionList">
	<xsl:choose>
		<!-- Translation: from Restriction information, to Restriction information -->
   		<xsl:when test="$Restriction_To_Restriction='YES'">
			<xsl:choose>
			<xsl:when test="$useAgeBasedDiets='NO'">
				<CodedRestrictionList>
					<xsl:copy-of select="translator:xlateNtoN(*, $translationTable)"/>
				</CodedRestrictionList>
			</xsl:when>
			<xsl:when test="$useAgeBasedDiets='YES'">
				<xsl:variable name="constantTable" select="document('xlateConstants.xml')/constantCodes/*"/>
				<CodedRestrictionList>
					<xsl:copy-of select="translator:xlateNtoNConst(* | ../../../../personInfo/ageIntervals/ageInterval, $translationTable, $constantTable)"/>
				</CodedRestrictionList>
			</xsl:when>
			</xsl:choose>
	   	</xsl:when>


		<!-- Translation: from Restriction and Allergy information, to Restriction information-->
		<xsl:when test="$Restriction_and_Allergy_TO_Restriction='YES'">
			<CodedRestrictionList>
				<xsl:copy-of select="translator:xlateNtoN(* | ../../../../visitInfo/AllergyList/*, $translationTable)"/>
			</CodedRestrictionList>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>



<!-- Translation: from Nursing Station information, to Nursing Station information -->
<xsl:template match="nursingStation">
	<xsl:choose>
   		<xsl:when test="$NursingStation_TO_NursingStation='YES'">
			<xsl:copy-of select="translator:xlateNtoN(., $translationTable)"/>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>



<!-- Translation: If No Room or Bed data in transaction, assign a RoomBed based on the Nursing Station.
	     Recommend using the same RoomBed value for all entries.  This would then allow using 
	     End of Day (EOD) to purge patients from those RoomBeds.  EOD is based on RoomBed name
	     and can have the same RoomBed name under multiple Nursing Stations. -->
<xsl:template match="room">

	<xsl:variable name="ROOM" select="."/>
	<xsl:variable name="BED" select="../bed"/>
 
	<xsl:choose>
		<xsl:when test="$NursingStation_TO_Room='YES' and $ROOM = '' and $BED = ''">

                                <xsl:copy-of select="translator:xlateNtoN(../nursingStation, $translationTable_NursingStation_TO_Room)"/>
                                <bed></bed>
		</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
			<xsl:copy-of select="../bed"/>
		</xsl:otherwise>
        </xsl:choose>
</xsl:template>
<xsl:template match="bed"/>



<!-- Translation: from Allergy information, to Allergy information-->
<xsl:template match="AllergyList">
	<xsl:choose>
   		<xsl:when test="$Allergy_TO_Allergy='YES'">
			<AllergyList>
				<xsl:copy-of select="translator:xlateNtoN(*, $translationTableAllergy)"/>
			</AllergyList>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- Translation: from Supplement information, to Supplement information -->
<xsl:template match="CodedSupplementList">
	<xsl:choose>
   		<xsl:when test="$Supplement_TO_Supplement='YES'">
			<CodedSupplementList>
			<xsl:variable name="SuppList">
				<CodedSupplementInfo>
					<xsl:for-each select="CodedSupplementInfo/CodedItemUofM">
						<xsl:copy-of select="translator:xlateNtoN(., $translationTable)"/>
					</xsl:for-each>
					<Quantity><xsl:value-of select="CodedSupplementInfo/Quantity"/></Quantity>
				</CodedSupplementInfo>
			</xsl:variable>
			<xsl:for-each select="msxsl:node-set($SuppList)//CodedItemUofM">
				<CodedSupplementInfo>
					<CodedItemUofM><xsl:value-of select="."/></CodedItemUofM>
					<Quantity><xsl:value-of select="../Quantity"/></Quantity>
				</CodedSupplementInfo>
			</xsl:for-each>
		</CodedSupplementList>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- Translation:  from Supplement Meal Period information, to Supplement Meal Period information -->
<xsl:template match="CodedMealRuleList">
	<xsl:choose>
   		<xsl:when test="$SupplementMealPeriod_TO_SupplementMealPeriod='YES'">
			<CodedMealRuleList>
				<xsl:copy-of select="translator:xlateNtoN(*, $translationTable)"/>
			</CodedMealRuleList>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Translation:  from Service Meal Period information, to Service Meal Period information -->
<!-- Translation:  from Service Location information, to Service Location information -->
<xsl:template match="codedStartMeal">
	<xsl:choose>
   		<xsl:when test="$ServiceMeal_TO_ServiceMeal='YES'">
			<xsl:copy-of select="translator:xlateNtoN(., $translationTableSStartMeal)"/>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="codedEndMeal">
	<xsl:choose>
   		<xsl:when test="$ServiceMeal_TO_ServiceMeal='YES'">
			<xsl:copy-of select="translator:xlateNtoN(., $translationTableSEndMeal)"/>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- Translation:  from Service Location information, to Service Location information -->
<xsl:template match="serviceLocationInfo">
		<xsl:choose>
	   		<xsl:when test="$ServiceLocation_TO_ServiceLocation='YES'">
				<xsl:copy-of select="translator:xlateNtoN(., $translationTableSLocation)"/>
		   	</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
</xsl:template>
<xsl:template match="serviceNS">
	<xsl:choose>
   		<xsl:when test="$ServiceNS_TO_ServiceNS='YES'">
			<xsl:copy-of select="translator:xlateNtoN(., $translationTableSNS)"/>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Translation: from Coded Race to Coded Race -->
<xsl:template match="CodedRace">
	<xsl:choose>
   		<xsl:when test="$CodedRace_To_CodedRace='YES'">
			<xsl:copy-of select="translator:xlateNtoN(., $translationTable)"/>
	   	</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- Handle any node not yet matched, strip out any comments or processing instructions -->
<xsl:template match="*|@*|text()">
	<xsl:copy><xsl:apply-templates select="*|@*|text()"/></xsl:copy>
</xsl:template>

<!-- Message Filtered -->
<xsl:template name="filterMsg">
	<ADTORDLIST></ADTORDLIST>
</xsl:template>

</xsl:stylesheet>