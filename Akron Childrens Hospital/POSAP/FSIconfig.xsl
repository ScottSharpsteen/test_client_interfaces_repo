﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:cbord="http://cbord.com/xsltns"  version="1.0">
<!--
	GTT: Modified for ESPNZONE POS Interface.
	06/23/03	POSUnitIDs will be in the format 1-1, 1-2, 1-3 ect.
			Other Fomats that are currently supported are 1_1, 1.1, 1.
			To accommodate other formats, add the checks in the UniId template 
 
-->
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>

<xsl:variable name="path" select="cbord:path(string(//PosPath))"/>
<xsl:template match='/'>
<configuration>
	<productName>FSI</productName>
        <xsl:copy-of select="document('FSISetup.xml')//LogItConfig"/> 
	<baseInfo>
		<maxMessageSize></maxMessageSize>
		<diagnosticLevel>DEBUG</diagnosticLevel>
		<auditPurgeDelay/>
		<incomingConfig>
			<numIOThreads>4</numIOThreads>
			<IOconcurrency>0</IOconcurrency>
			<numClientThreads>4</numClientThreads>
			<ClientConcurrency>0</ClientConcurrency>
		</incomingConfig>
		<outgoingConfig>
			<messageQname>\\.\accountingRequestQ</messageQname>
			<numIOThreads>2</numIOThreads>
			<numCommandThreads>1</numCommandThreads>
		</outgoingConfig>
	</baseInfo>
	<targetSet>
		<!--target>
	   		<id>CbordDB</id>
	   		<bzStart/>
	   		<bzEnd/>
	   		<CbordDB>
				<DSN>cbord_asa7</DSN>
				<progId>CBORDCIF.FSI_Update</progId>
				<FacilityName><xsl:value-of select="//Facility"/></FacilityName>
				<Unitid>0</Unitid>
	   		</CbordDB>
		</target-->
		<target>
			<id>PLUTRANSLATIONTARGET</id>
			<bzStart />
			<bzEnd />
			<Channel>
				<id>PLUTRANSLATIONUOTPUT</id>
			</Channel>
		</target>
     <target>
       <id>CbordDB</id>
        <bzStart />
        <bzEnd />
        <CbordDB>
            <Config NoCount="OFF">
	        <DBMS>ODBC</DBMS> 
  	        <Lock>RC</Lock> 
                <DBParm>DisableBind=0,Block=10,ConnectOption='SQL_DRIVER_CONNECT,SQL_DRIVER_NOPROMPT'</DBParm> 
	        <DSN>cbord_asa8</DSN>
                <Userid>accounting_interface</Userid> 
                <Password>sup$23@act</Password>
           </Config>
           <DSN>cbord_asa8</DSN> 
           <progId>CBORDCIF.FSI_Update</progId> 
           <FacilityName><xsl:value-of select="//Facility"/></FacilityName> 
           <Unitid>0</Unitid>
           <hasLookup>true</hasLookup>
      </CbordDB>
    </target>

</targetSet>

       	<configSet>

        <!-- PLU Translation Interface -->

    <connection>
     <name>PLU Translation Table</name> 
     <id>PLU_Translation_Table</id> 
     <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
     <format>2</format> 
     <timeout>1</timeout> 
     <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
     <kind>
     <fileIO>
     <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir>
     <retentionDays>14</retentionDays> 
     <inFile>
    <searchOrder>CREATE</searchOrder> 
  <pattern><xsl:value-of select="$path"/>\work\intf\inbound\*Pluxlt.txt</pattern> 
  </inFile>
 <outFile>
  <existsOption /> 
  <pattern /> 
  </outFile>
  </fileIO>
  </kind>
  <diagnosticLevel>4</diagnosticLevel> 
  <connectAtStartup>true</connectAtStartup> 
  <direction>INCOMING</direction> 
  <continuous>true</continuous> 
  <dbId>-1</dbId> 
  <inboundType>BYTES</inboundType> 
<extendedInfo>
<targetMapping>
<targetMap>
<msgData></msgData>
<xPath></xPath>
<id>CbordDB</id>
</targetMap>
</targetMapping>
<processPath>
<splitter>
<path>
<channel>PLUTARGETCONNECTION</channel>
<processPath>
<xlate>
<script><xsl:value-of select="$path"/>\Interfaces\FSI\identity.xsl</script>
<outputFormat>ByteArrayText</outputFormat>
<usesLookup>false</usesLookup>
<auditInfo>
<prefix>posPLU</prefix>
<path><xsl:value-of select="$path"/>\work\intfLogs</path>
<tag>posPLU</tag>
</auditInfo>
</xlate>
</processPath>
</path>
</splitter>
<fixedRecordParser>
<parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\Pluxlt.xml</parseTablePath>
<auditInfo>
<tag>List</tag>
<prefix>PLUTable</prefix>
<path><xsl:value-of select="$path"/>\work\IntfLogs</path>
</auditInfo>
</fixedRecordParser>
</processPath>
</extendedInfo>
</connection>



<connection>
  <name>PLUTARGETCONNECTION</name> 
  <id>PLUTARGETCONNECTION</id> 
  <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
  <format>1</format> 
  <timeout>5</timeout> 
  <rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath> 
  <kind>
  <fileIO>
  <outFile>
  <pattern /> 
  </outFile>
  <inFile>
  <pattern /> 
  </inFile>
  </fileIO>
  </kind>
  <diagnosticLevel>4</diagnosticLevel> 
  <connectAtStartup>false</connectAtStartup> 
  <direction>incoming</direction> 
  <continuous>false</continuous> 
  <extendedInfo>
  <targetMapping>
  <targetMap>
  <msgData /> 
  <id>PLUTRANSLATIONTARGET</id> 
  </targetMap>
  </targetMapping>
  </extendedInfo>
  </connection>


 <connection>
  <name>PLUTRANSLATIONUOTPUT</name> 
  <id>PLUTRANSLATIONUOTPUT</id> 
  <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
  <format>1</format> 
  <timeout>5</timeout> 
  <rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath> 
  <kind>
  <fileIO>
  <outFile>
	<pattern><xsl:value-of select="$path"/>\interfaces\FSI\posPluxlt.xml</pattern>
	<existsOption>OVERWRITE</existsOption> 
  </outFile>
  <inFile>
  <pattern /> 
  </inFile>
  </fileIO>
  </kind>
  <diagnosticLevel>4</diagnosticLevel> 
  <connectAtStartup>false</connectAtStartup> 
  <direction>outgoing</direction> 
  <continuous>false</continuous> 
 <extendedInfo>
 <targetMapping>
 <targetMap>
  <msgData /> 
  <id>CbordDB</id> 
  </targetMap>
  </targetMapping>
  </extendedInfo>
  </connection>

<!-- Interface Config -->
<connection>
<name>IntfConfig</name>
<id>ReconfigPOS</id>
<frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
<format>1</format>
<timeout>5</timeout>
<rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath>
<kind>
<fileIO>
<archiveDir><xsl:value-of select="$path"/>\work\intfLogs\archive</archiveDir>
<retentionDays>14</retentionDays>
<outFile>
<existsOption>OVERWRITE</existsOption>
<pattern><xsl:value-of select="$path"/>\interfaces\fsi\ConfigDataExport.xml</pattern>
</outFile>
<inFile><pattern /></inFile>
</fileIO>
</kind>
<diagnosticLevel>4</diagnosticLevel>
<connectAtStartup>false</connectAtStartup>
<direction>outgoing</direction>
<continuous>false</continuous>
<dbId>-1</dbId>
<inboundType>BYTES</inboundType>
<extendedInfo>
<targetMapping>
<targetMap>
<msgData />
<xPath />
<id>CbordDB</id>
</targetMap>
</targetMapping>
<processPath>
<splitter>
<path>
<channel>POSTranslationTable</channel>
<processPath>
<xlate>
<script><xsl:value-of select="$path"/>\Interfaces\FSI\POSConfigData.xsl</script>
<outputFormat>ByteArrayText</outputFormat>
<usesLookup>false</usesLookup>
</xlate>
</processPath>
</path>
<path>
<channel>POSConfiguration</channel>
<processPath>
<xlate>
<script><xsl:value-of select="$path"/>\Interfaces\FSI\FSIconfig.xsl</script>
<outputFormat>ByteArrayText</outputFormat>
<usesLookup>false</usesLookup>
</xlate>
</processPath>
</path>
</splitter>
</processPath>
</extendedInfo>
</connection>

<connection>
<name>POSConfiguration</name>
<id>POSConfiguration</id>
<frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
<format>1</format>
<timeout>1</timeout>
<rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath>
<kind>
<fileIO>
<outFile>
<pattern><xsl:value-of select="$path"/>\work\setup\FSIConfig.xml</pattern>
<existsOption>OVERWRITE</existsOption>
</outFile>
<inFile><pattern /></inFile>
</fileIO>
</kind>
<diagnosticLevel>4</diagnosticLevel>
<connectAtStartup>false</connectAtStartup>
<direction>outgoing</direction>
<continuous>false</continuous>
<extendedInfo>
<targetMapping>
<targetMap>
<msgData></msgData>
<id>CbordDB</id>
</targetMap>
</targetMapping>
</extendedInfo>
</connection>

<connection>
<name>POSTranslationTable</name>
<id>POSTranslationTable</id>
<frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
<format>1</format>
<timeout>1</timeout>
<rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath>
<kind>
<fileIO>
<outFile>
<pattern><xsl:value-of select="$path"/>\Interfaces\FSI\ConfigData.xml</pattern>
<existsOption>OVERWRITE</existsOption>
</outFile>
<inFile><pattern /></inFile>
</fileIO>
</kind>
<diagnosticLevel>4</diagnosticLevel>
<connectAtStartup>false</connectAtStartup>
<direction>outgoing</direction>
<continuous>false</continuous>
<extendedInfo>
<targetMapping>
<targetMap>
<msgData></msgData>
<id>CbordDB</id>
</targetMap>
</targetMapping>
</extendedInfo>
</connection>

<!-- POS Menu Updates -->
<connection>
   <name>Menu1</name>
   <id>CBORDMENU_UPDATE1</id>
   <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
   <format>1</format>
   <timeout>5</timeout>
   <rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath>
   <kind>
      <fileIO>
        <outFile><pattern/></outFile>
        <inFile><pattern/></inFile>
      </fileIO>
   </kind>
   <diagnosticLevel>4</diagnosticLevel>
   <connectAtStartup>false</connectAtStartup>
   <direction>incoming</direction>
   <continuous>false</continuous>
   <extendedInfo>
      <targetMapping>
         <targetMap><msgData></msgData><id>CbordDB</id></targetMap>
      </targetMapping>
   </extendedInfo>
</connection>

<connection>
          <name>Menu2</name>
          <id>CBORDMENU_UPDATE2</id>
          <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
          <format>1</format>
          <timeout>5</timeout>
          <rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath>
          <kind>
            <fileIO>
              <outFile><pattern/></outFile>
              <inFile><pattern/></inFile>
            </fileIO>
          </kind>
          <diagnosticLevel>4</diagnosticLevel>
          <connectAtStartup>false</connectAtStartup>
          <direction>incoming</direction>
          <continuous>false</continuous>
	  <extendedInfo>
            <targetMapping>
              <targetMap><msgData></msgData><id>CbordDB</id></targetMap>
            </targetMapping>
	  </extendedInfo>
</connection>

<connection>
          <name>Menu3</name>
          <id>CBORDMENU_UPDATE3</id>
          <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
          <format>1</format>
          <timeout>5</timeout>
          <rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath>
          <kind>
            <fileIO>
              <outFile><pattern/></outFile>
              <inFile><pattern/></inFile>
            </fileIO>
          </kind>
          <diagnosticLevel>4</diagnosticLevel>
          <connectAtStartup>false</connectAtStartup>
          <direction>incoming</direction>
          <continuous>false</continuous>
	  <extendedInfo>
            <targetMapping>
              <targetMap><msgData></msgData><id>CbordDB</id></targetMap>
            </targetMapping>
	  </extendedInfo>
</connection>

   <xsl:apply-templates select='//PosUnit' mode='main'/>
   <xsl:apply-templates select="document('FSISetup.xml')//Intf">
       <xsl:with-param name="path" select="$path"/> 
   </xsl:apply-templates> 
</configSet>
</configuration>
</xsl:template>



<xsl:template match='PosUnit' mode='main'>
  <xsl:variable name='type' select='PosType'/>
  <xsl:if test='generate-id((//PosUnit[PosType=$type])[1]) = generate-id(.)'>
     <xsl:apply-templates select='//PosUnit[PosType=$type][position() &gt; 1]' mode='channels'/>
     <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='base'/>
     <xsl:apply-templates select='.' mode='mainprocess'/>

    <!--connection>
      <name><xsl:value-of select='$type'/></name>
      <id><xsl:value-of select="$type"/></id>
      <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
      <format>2</format>
      <timeout>1</timeout>
      <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath>
      <kind>
        <fileIO>
          <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir>
          <retentionDays>14</retentionDays>
          <inFile>
            <searchOrder>CREATE</searchOrder>
            <pattern>
               <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='pattern'/>
	   </pattern>
          </inFile>
          <outFile><existsOption/><pattern/></outFile>
        </fileIO>
      </kind>
      <diagnosticLevel>4</diagnosticLevel>
      <connectAtStartup>true</connectAtStartup>
      <direction>INCOMING</direction>
      <continuous>true</continuous>
      <dbId>-1</dbId>
      <inboundType>BYTES</inboundType>
      <extendedInfo>
        <targetMapping>
          <targetMap>
            <msgData></msgData>
            <xPath></xPath>
            <id>CbordDB</id>
          </targetMap>
        </targetMapping>
        <processPath>
            <splitter>
			<path>
				<channel>CBORDMENU_UPDATE1</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate1.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE2</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate2.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE3</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate3.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>

	  </splitter>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\PosValidator.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Update</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupItems.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupItems</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupHeaders.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupHeaders</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSxlateAndFilter.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSxlateAndFiltered</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSCombine.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSCombined</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\union.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Union</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>Data</tag> 
            </auditInfo>
          </xlate>
          <merge>
            <xsl:apply-templates select='//PosUnit[PosType=$type][position() &gt; 1]' mode='perunit'/>
          </merge>
          <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='entry'/>
        </processPath>
      </extendedInfo>
    </connection-->
  </xsl:if>
</xsl:template>

<xsl:template match="PosUnit[PosType='M2700']" mode="mainprocess">
  <xsl:variable name='type' select='PosType'/>
    <connection>
      <name><xsl:value-of select='$type'/></name>
      <id><xsl:value-of select="$type"/></id>
      <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
      <format>2</format>
      <timeout>1</timeout>
      <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath>
      <kind>
        <fileIO>
          <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir>
          <retentionDays>14</retentionDays>
          <inFile>
            <searchOrder>CREATE</searchOrder>
            <pattern>
               <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='pattern'/>
	   </pattern>
          </inFile>
          <outFile><existsOption/><pattern/></outFile>
        </fileIO>
      </kind>
      <diagnosticLevel>4</diagnosticLevel>
      <connectAtStartup>true</connectAtStartup>
      <direction>INCOMING</direction>
      <continuous>true</continuous>
      <dbId>-1</dbId>
      <inboundType>BYTES</inboundType>
      <extendedInfo>
        <targetMapping>
          <targetMap>
            <msgData></msgData>
            <xPath></xPath>
            <id>CbordDB</id>
          </targetMap>
        </targetMapping>
        <processPath>
            <splitter>
			<path>
				<channel>CBORDMENU_UPDATE1</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate1.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE2</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate2.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE3</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate3.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>

	  </splitter>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\PosValidator.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Update</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupItems.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupItems</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupHeaders.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupHeaders</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSxlateAndFilter.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSxlateAndFiltered</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSCombine.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSCombined</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\union.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Union</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>Data</tag> 
            </auditInfo>
          </xlate>
          <merge>
            <xsl:apply-templates select='//PosUnit[PosType=$type][position() &gt; 1]' mode='perunit'/>
          </merge>
          <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='entry'/>
        </processPath>
      </extendedInfo>
    </connection>
</xsl:template>

<xsl:template match="PosUnit[PosType='CBOP37']" mode="mainprocess">
  <xsl:variable name='type' select='PosType'/>
    <connection>
      <name><xsl:value-of select='$type'/></name>
      <id><xsl:value-of select="$type"/></id>
      <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
      <format>2</format>
      <timeout>1</timeout>
      <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath>
      <kind>
        <fileIO>
          <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir>
          <retentionDays>14</retentionDays>
          <inFile>
            <searchOrder>CREATE</searchOrder>
            <pattern>
               <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='pattern'/>
	   </pattern>
          </inFile>
          <outFile><existsOption/><pattern/></outFile>
        </fileIO>
      </kind>
      <diagnosticLevel>4</diagnosticLevel>
      <connectAtStartup>true</connectAtStartup>
      <direction>INCOMING</direction>
      <continuous>true</continuous>
      <dbId>-1</dbId>
      <inboundType>BYTES</inboundType>
      <extendedInfo>
        <targetMapping>
          <targetMap>
            <msgData></msgData>
            <xPath></xPath>
            <id>CbordDB</id>
          </targetMap>
        </targetMapping>
        <processPath>
            <splitter>
			<path>
				<channel>CBORDMENU_UPDATE1</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate1.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE2</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate2.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE3</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate3.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>

	  </splitter>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\PosValidator.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Update</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupItems.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupItems</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupHeaders.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupHeaders</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSxlateAndFilter.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSxlateAndFiltered</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSCombine.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSCombined</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\union.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Union</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>Data</tag> 
            </auditInfo>
          </xlate>
          <merge>
            <xsl:apply-templates select='//PosUnit[PosType=$type][position() &gt; 1]' mode='perunit'/>
          </merge>
          <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='entry'/>
        </processPath>
      </extendedInfo>
    </connection>
</xsl:template>

<xsl:template match="PosUnit[PosType='M8700']" mode="mainprocess">
  <xsl:variable name='type' select='PosType'/>
    <connection>
      <name><xsl:value-of select='$type'/></name>
      <id><xsl:value-of select="$type"/></id>
      <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
      <format>2</format>
      <timeout>1</timeout>
      <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath>
      <kind>
        <fileIO>
          <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir>
          <retentionDays>14</retentionDays>
          <inFile>
            <searchOrder>CREATE</searchOrder>
            <pattern>
               <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='pattern'/>
	   </pattern>
          </inFile>
          <outFile><existsOption/><pattern/></outFile>
        </fileIO>
      </kind>
      <diagnosticLevel>4</diagnosticLevel>
      <connectAtStartup>true</connectAtStartup>
      <direction>INCOMING</direction>
      <continuous>true</continuous>
      <dbId>-1</dbId>
      <inboundType>BYTES</inboundType>
      <extendedInfo>
        <targetMapping>
          <targetMap>
            <msgData></msgData>
            <xPath></xPath>
            <id>CbordDB</id>
          </targetMap>
        </targetMapping>
        <processPath>
            <splitter>
			<path>
				<channel>CBORDMENU_UPDATE1</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate1.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE2</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate2.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE3</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate3.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>

	  </splitter>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\PosValidator.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Update</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupItems.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupItems</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupHeaders.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupHeaders</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSxlateAndFilter.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSxlateAndFiltered</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSCombine.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSCombined</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\union.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Union</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>Data</tag> 
            </auditInfo>
          </xlate>
          <merge>
            <xsl:apply-templates select='//PosUnit[PosType=$type][position() &gt; 1]' mode='perunit'/>
          </merge>
          <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='entry'/>
        </processPath>
      </extendedInfo>
    </connection>
</xsl:template>

<xsl:template match="PosUnit[PosType='InfoGenesis']" mode="mainprocess">
  <xsl:variable name='type' select='PosType'/>
    <connection>
      <name><xsl:value-of select='$type'/></name>
      <id><xsl:value-of select="$type"/></id>
      <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
      <format>2</format>
      <timeout>1</timeout>
      <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath>
      <kind>
        <fileIO>
          <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir>
          <retentionDays>14</retentionDays>
          <inFile>
            <searchOrder>CREATE</searchOrder>
            <pattern>
               <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='pattern'/>
	   </pattern>
          </inFile>
          <outFile><existsOption/><pattern/></outFile>
        </fileIO>
      </kind>
      <diagnosticLevel>4</diagnosticLevel>
      <connectAtStartup>true</connectAtStartup>
      <direction>INCOMING</direction>
      <continuous>true</continuous>
      <dbId>-1</dbId>
      <inboundType>BYTES</inboundType>
      <extendedInfo>
        <targetMapping>
          <targetMap>
            <msgData></msgData>
            <xPath></xPath>
            <id>CbordDB</id>
          </targetMap>
        </targetMapping>
        <processPath>
            <splitter>
			<path>
				<channel>CBORDMENU_UPDATE1</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate1.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE2</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate2.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE3</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate3.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>

	  </splitter>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\PosValidator.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Update</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupItems.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupItems</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupHeaders.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupHeaders</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSxlateAndFilter.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSxlateAndFiltered</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSCombine.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSCombined</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\union.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Union</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>Data</tag> 
            </auditInfo>
          </xlate>
          <merge>
            <xsl:apply-templates select='//PosUnit[PosType=$type][position() &gt; 1]' mode='perunit'/>
          </merge>
          <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='entry'/>
        </processPath>
      </extendedInfo>
    </connection>
</xsl:template>

<xsl:template match="PosUnit[PosType='Revelation']" mode="mainprocess">
  <xsl:variable name='type' select='PosType'/>
    <connection>
      <name><xsl:value-of select='$type'/></name>
      <id><xsl:value-of select="$type"/></id>
      <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
      <format>2</format>
      <timeout>1</timeout>
      <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath>
      <kind>
        <fileIO>
          <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir>
          <retentionDays>14</retentionDays>
          <inFile>
            <searchOrder>CREATE</searchOrder>
            <pattern>
               <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='pattern'/>
	   </pattern>
          </inFile>
          <outFile><existsOption/><pattern/></outFile>
        </fileIO>
      </kind>
      <diagnosticLevel>4</diagnosticLevel>
      <connectAtStartup>true</connectAtStartup>
      <direction>INCOMING</direction>
      <continuous>true</continuous>
      <dbId>-1</dbId>
      <inboundType>BYTES</inboundType>
      <extendedInfo>
        <targetMapping>
          <targetMap>
            <msgData></msgData>
            <xPath></xPath>
            <id>CbordDB</id>
          </targetMap>
        </targetMapping>
        <processPath>
            <splitter>
			<path>
				<channel>CBORDMENU_UPDATE1</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate1.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE2</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate2.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>
			<path>
				<channel>CBORDMENU_UPDATE3</channel>
				<processPath>
				   <xlate>
             				<script><xsl:value-of select="$path"/>\Interfaces\fsi\MenuUpdate3.xsl</script>
             				<outputFormat>ByteArrayText</outputFormat>
             				<auditInfo>
                				<tag>MenuMessage</tag>
                				<prefix>Menu</prefix>
                				<path><xsl:value-of select="$path"/>\work\intflogs</path>
             				</auditInfo>
             				<usesLookup>false</usesLookup>
         			   </xlate>
				</processPath>
			</path>

	  </splitter>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\PosValidator.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Update</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupItems.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupItems</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSrollupHeaders.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSrollupHeaders</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSxlateAndFilter.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSxlateAndFiltered</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\POSCombine.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>POSCombined</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>POSList</tag> 
            </auditInfo>
          </xlate>
          <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\union.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>Union</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>Data</tag> 
            </auditInfo>
          </xlate>
          <merge>
            <xsl:apply-templates select='//PosUnit[PosType=$type][position() &gt; 1]' mode='perunit'/>
          </merge>
          <xsl:apply-templates select='//PosUnit[PosType=$type][position() = 1]' mode='entry'/>
        </processPath>
      </extendedInfo>
    </connection>
</xsl:template>


<!-- InfoGenesis Interface -->
<xsl:template match="PosUnit[PosType='InfoGenesis']" mode="entry">
      <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\InfoGenesisRaw.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>pcmraw_</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>List</tag> 
            </auditInfo>
          </xlate>          
           <fixedRecordParser>
              <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\InfoGenesisDef.xml</parseTablePath> 
               <auditInfo>
                 <tag>POScsv</tag> 
                 <prefix>POScsv</prefix> 
                 <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              </auditInfo>
          </fixedRecordParser>
</xsl:template>

<xsl:template match='PosUnit[PosType="InfoGenesis"]' mode='base'/>

<xsl:template match='PosUnit[PosType="InfoGenesis"]' mode='channels'>
  <!-- Do not create channels for this POS Type -->
</xsl:template>

<xsl:template match='PosUnit[PosType="InfoGenesis"]' mode='perunit'>
   <!-- Ignore this unit -->
</xsl:template>

<xsl:template match='PosUnit[PosType="InfoGenesis"]' mode='pattern'>
  <xsl:value-of select="$path"/>\work\intf\inbound\InfoGenesis.txt
</xsl:template>

<!-- Revelation Interface -->
<xsl:template match="PosUnit[PosType='Revelation']" mode="entry">
      <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\RevelationRaw.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>pcmraw_</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>List</tag> 
            </auditInfo>
          </xlate>          
           <fixedRecordParser>
              <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\RevelationDef.xml</parseTablePath> 
               <auditInfo>
                 <tag>POScsv</tag> 
                 <prefix>POScsv</prefix> 
                 <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              </auditInfo>
          </fixedRecordParser>
</xsl:template>

<xsl:template match='PosUnit[PosType="Revelation"]' mode='base'/>

<xsl:template match='PosUnit[PosType="Revelation"]' mode='channels'>
  <!-- Do not create channels for this POS Type -->
</xsl:template>


<xsl:template match='PosUnit[PosType="Revelation"]' mode='perunit'>
   <!-- Ignore this unit -->
</xsl:template>

<xsl:template match='PosUnit[PosType="Revelation"]' mode='pattern'>
   <xsl:value-of select="$path"/>\work\intf\inbound\Revelation.txt
</xsl:template>

<!-- M2700 Interface -->

<xsl:template match="PosUnit[PosType='M2700']" mode="entry">
      <xlate>
        <script><xsl:value-of select="$path"/>\interfaces\fsi\m2700Combo.xsl</script> 
        <outputFormat>IUnknown</outputFormat> 
        <usesLookup>false</usesLookup> 
        <auditInfo>
          <prefix>2700PostLookup</prefix> 
          <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
          <tag>Data</tag> 
        </auditInfo>
      </xlate>

      <merge>
        <path>
          <channel>Menudef<xsl:value-of select='format-number(PosUnitId, "00")'/></channel> 
          <processPath>
            <fixedRecordParser>
              <parseTablePath><xsl:value-of select="$path"/>\interfaces\fsi\m2700MenuDefs.xml</parseTablePath> 
              <auditInfo>
                <prefix>MenuDefs</prefix> 
                <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
                <tag>MenuDefs</tag> 
              </auditInfo>
            </fixedRecordParser>
          </processPath>
        </path>
      </merge>
      <xlate>
        <script><xsl:value-of select="$path"/>\interfaces\fsi\m2700FilterEJournal.xsl</script> 
        <outputFormat>IUnknown</outputFormat> 
        <usesLookup>false</usesLookup> 
          <auditInfo>
            <prefix>m2700FilteredEJ</prefix> 
            <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
            <tag>Ejournaloutput</tag> 
          </auditInfo>
        </xlate>
        <micros2700EjournalParser>
          <configFilePath><xsl:value-of select="$path"/>\interfaces\FSI\m2700EJournalParserDefn.xml</configFilePath> 
          <auditInfo>
          <prefix>m2700RawEJ_</prefix> 
          <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
          <tag>Ejournaloutput</tag> 
        </auditInfo>
      </micros2700EjournalParser>
</xsl:template>

<xsl:template match='PosUnit[PosType="M2700"]' mode='base'>
   <connection>
    <name>Menudef<xsl:value-of select='format-number(PosUnitId, "00")'/></name> 
    <id>Menudef<xsl:value-of select='format-number(PosUnitId, "00")'/></id> 
    <frame><boundedFrame><STX/><ETX/></boundedFrame></frame>
    <format>2</format> 
    <timeout>1</timeout> 
    <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
    <kind>
      <fileIO>
        <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir> 
        <retentionDays>14</retentionDays> 
        <inFile>
          <searchOrder>CREATE</searchOrder> 
          <pattern><xsl:value-of select="$path"/>\work\intf\inbound\Menudef.<xsl:value-of select='format-number(PosUnitId, "00")'/></pattern> 
        </inFile>
        <outFile>
          <existsOption /> 
          <pattern /> 
        </outFile>
      </fileIO>
    </kind>
    <diagnosticLevel>4</diagnosticLevel> 
    <connectAtStartup>true</connectAtStartup> 
    <direction>INCOMING</direction> 
    <continuous>true</continuous> 
    <dbId>-1</dbId> 
    <inboundType>BYTES</inboundType> 
  </connection>
</xsl:template>

<xsl:template match='PosUnit[PosType="M2700"]' mode='channels'>
  <xsl:variable name='id' select='format-number(PosUnitId, "00")'/>
  <connection>
    <name>PRNT<xsl:value-of select='$id'/></name> 
    <id>PRNT<xsl:value-of select='$id'/></id> 
    <frame><boundedFrame><STX/><ETX/></boundedFrame></frame>
    <format>2</format> 
    <timeout>1</timeout> 
    <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
    <kind>
      <fileIO>
        <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir> 
        <retentionDays>14</retentionDays> 
        <inFile>
          <searchOrder>CREATE</searchOrder> 
          <pattern><xsl:value-of select="$path"/>\work\intf\inbound\PRNT*.J<xsl:value-of select='$id'/></pattern> 
        </inFile>
        <outFile><existsOption /> <pattern /> </outFile>
      </fileIO>
    </kind>
    <diagnosticLevel>4</diagnosticLevel> 
    <connectAtStartup>true</connectAtStartup> 
    <direction>INCOMING</direction> 
    <continuous>true</continuous> 
    <dbId>-1</dbId> 
    <inboundType>BYTES</inboundType> 
  </connection>
  <connection>
    <name>Menudef<xsl:value-of select='$id'/></name> 
    <id>Menudef<xsl:value-of select='$id'/></id> 
    <frame><boundedFrame><STX/><ETX/></boundedFrame></frame>
    <format>2</format> 
    <timeout>1</timeout> 
    <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
    <kind>
      <fileIO>
        <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir> 
        <retentionDays>14</retentionDays> 
        <inFile>
          <searchOrder>CREATE</searchOrder> 
          <pattern><xsl:value-of select="$path"/>\work\intf\inbound\Menudef.<xsl:value-of select='$id'/></pattern> 
        </inFile>
        <outFile>
          <existsOption /> 
          <pattern /> 
        </outFile>
      </fileIO>
    </kind>
    <diagnosticLevel>4</diagnosticLevel> 
    <connectAtStartup>true</connectAtStartup> 
    <direction>INCOMING</direction> 
    <continuous>true</continuous> 
    <dbId>-1</dbId> 
    <inboundType>BYTES</inboundType> 
  </connection>
</xsl:template>


<xsl:template match='PosUnit[PosType="M2700"]' mode='perunit'>
  <xsl:variable name='id' select='format-number(PosUnitId, "00")'/>
  <path>
    <channel>PRNT<xsl:value-of select='$id'/></channel> 
    <processPath>
      <xlate>
        <script><xsl:value-of select="$path"/>\interfaces\fsi\m2700Combo.xsl</script> 
        <outputFormat>IUnknown</outputFormat> 
        <usesLookup>false</usesLookup> 
        <auditInfo>
          <prefix>2700PostLookup</prefix> 
          <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
          <tag>Data</tag> 
        </auditInfo>
      </xlate>
      <merge>
        <path>
          <channel>Menudef<xsl:value-of select='$id'/></channel> 
          <processPath>
            <fixedRecordParser>
              <parseTablePath><xsl:value-of select="$path"/>\interfaces\fsi\m2700MenuDefs.xml</parseTablePath> 
              <auditInfo>
                <prefix>MenuDefs</prefix> 
                <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
                <tag>MenuDefs</tag> 
              </auditInfo>
            </fixedRecordParser>
          </processPath>
        </path>
      </merge>
      <xlate>
        <script><xsl:value-of select="$path"/>\interfaces\fsi\m2700FilterEJournal.xsl</script> 
        <outputFormat>IUnknown</outputFormat> 
        <usesLookup>false</usesLookup> 
          <auditInfo>
            <prefix>m2700FilteredEJ</prefix> 
            <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
            <tag>Ejournaloutput</tag> 
          </auditInfo>
        </xlate>
        <micros2700EjournalParser>
          <configFilePath><xsl:value-of select="$path"/>\interfaces\FSI\m2700EJournalParserDefn.xml</configFilePath> 
          <auditInfo>
          <prefix>m2700RawEJ_</prefix> 
          <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
          <tag>Ejournaloutput</tag> 
        </auditInfo>
      </micros2700EjournalParser>
    </processPath>
  </path>
</xsl:template>

<xsl:template match='PosUnit[PosType="M2700"]' mode='pattern'>
   <xsl:value-of select="$path"/>\work\intf\inbound\PRNT*.J<xsl:value-of select='format-number(PosUnitId, "00")'/>
</xsl:template>


<!-- CBOP37 Interface -->
<xsl:template match="PosUnit[PosType='CBOP37']" mode="entry">
      <xlate>
            <script><xsl:value-of select="$path"/>\interfaces\fsi\posraw.xsl</script> 
            <outputFormat>IUnknown</outputFormat> 
            <usesLookup>false</usesLookup> 
            <auditInfo>
              <prefix>pcmraw_</prefix> 
              <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              <tag>List</tag> 
            </auditInfo>
          </xlate>          
           <fixedRecordParser>
              <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\Heimlich.xml</parseTablePath> 
               <auditInfo>
                 <tag>POScsv</tag> 
                 <prefix>POScsv</prefix> 
                 <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
              </auditInfo>
          </fixedRecordParser>
</xsl:template>

<xsl:template match='PosUnit[PosType="CBOP37"]' mode='base'/>

<xsl:template match='PosUnit[PosType="CBOP37"]' mode='channels'>
  <!-- Do not create channels for this POS Type -->
</xsl:template>

<xsl:template match='PosUnit[PosType="CBOP37"]' mode='perunit'>
   <!-- Ignore this unit -->
</xsl:template>

<xsl:template match='PosUnit[PosType="CBOP37"]' mode='pattern'>
    <!-- For Disney the file name is PCMIMPRT.txt -->
        <xsl:value-of select="$path"/>\work\intf\inbound\PCMIMPRT.txt
</xsl:template>


<!-- M8700 Inteterface -->
<xsl:template match="PosUnit[PosType='M8700']" mode="entry">
<xlate>
<script><xsl:value-of select="$path"/>\interfaces\FSI\M8700CombineItems.xsl</script>
<outputFormat>IUnknown</outputFormat>
<usesLookup>false</usesLookup>
<auditInfo>
<prefix>m8700CombineItems</prefix>
<path><xsl:value-of select="$path"/>\work\IntfLogs</path>
<tag>Data</tag>
</auditInfo>
</xlate>
 <xlate>
  <script><xsl:value-of select="$path"/>\interfaces\FSI\m8700Raw.xsl</script> 
  <outputFormat>IUnknown</outputFormat> 
  <usesLookup>false</usesLookup> 
  <auditInfo>
  <prefix>m8700Combine_</prefix> 
  <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
  <tag>Data</tag> 
  </auditInfo>
  </xlate>
  <merge>
  <path>
     <xsl:variable name='id'>
      <xsl:call-template name='UnitId'>
	  <xsl:with-param name="Id" select="PosUnitId"/>
      </xsl:call-template> 
     </xsl:variable>

  <channel>MENUITEMTOTALS<xsl:value-of select='format-number($id, "0")'/></channel> 
  <processPath>
  <fixedRecordParser>
         <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\m8700mitot.xml</parseTablePath> 
  </fixedRecordParser>
  </processPath>
  </path>
  <path>
  <channel>CLOSEDCHECKTOTALS<xsl:value-of select='format-number($id, "0")'/></channel> 
  <processPath>
  <fixedRecordParser>
        <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\m8700cctot.xml</parseTablePath> 
  </fixedRecordParser>
  </processPath>
  </path>
  </merge>
  <fixedRecordParser>
       <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\m8700ccdet.xml</parseTablePath> 
  </fixedRecordParser>
</xsl:template>

<xsl:template match='PosUnit[PosType="M8700"]' mode='base'>
<xsl:variable name='id'>
      <xsl:call-template name='UnitId'>
	  <xsl:with-param name="Id" select="PosUnitId"/>
      </xsl:call-template> 
 </xsl:variable>

 <connection>
  <name>MENUITEMTOTALS<xsl:value-of select='$id'/></name> 
  <id>MENUITEMTOTALS<xsl:value-of select='$id'/></id> 
  <frame><boundedFrame><STX/><ETX /></boundedFrame></frame>
  <format>2</format> 
  <timeout>1</timeout> 
  <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
  <kind>
  <fileIO>
  <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir> 
  <retentionDays>14</retentionDays> 
  <inFile>
  <searchOrder>CREATE</searchOrder> 
  <pattern><xsl:value-of select="$path"/>\work\intf\inbound\MITOTALS.<xsl:value-of select='$id'/></pattern> 
  </inFile>
  <outFile>
  <existsOption /> 
  <pattern /> 
  </outFile>
  </fileIO>
  </kind>
  <diagnosticLevel>4</diagnosticLevel> 
  <connectAtStartup>true</connectAtStartup> 
  <direction>INCOMING</direction> 
  <continuous>true</continuous> 
  <dbId>-1</dbId> 
  <inboundType>BYTES</inboundType> 
  </connection>
  <connection>
  <name>CLOSEDCHECKTOTALS<xsl:value-of select='$id'/></name> 
  <id>CLOSEDCHECKTOTALS<xsl:value-of select='$id'/></id> 
  <frame><boundedFrame><STX/><ETX/></boundedFrame></frame>
  <format>2</format> 
  <timeout>1</timeout> 
  <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
  <kind>
  <fileIO>
  <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir> 
  <retentionDays>14</retentionDays> 
  <inFile>
  <searchOrder>CREATE</searchOrder> 
  <pattern><xsl:value-of select="$path"/>\work\intf\inbound\CCTOT.<xsl:value-of select='$id'/></pattern> 
  </inFile>
  <outFile>
  <existsOption /> 
  <pattern /> 
  </outFile>
  </fileIO>
  </kind>
  <diagnosticLevel>4</diagnosticLevel> 
  <connectAtStartup>true</connectAtStartup> 
  <direction>INCOMING</direction> 
  <continuous>true</continuous> 
  <dbId>-1</dbId> 
  <inboundType>BYTES</inboundType> 
  </connection>
</xsl:template>


<xsl:template match='PosUnit[PosType="M8700"]' mode='channels'>
<xsl:variable name='id'>
      <xsl:call-template name='UnitId'>
	  <xsl:with-param name="Id" select="PosUnitId"/>
      </xsl:call-template> 
 </xsl:variable>
  <connection>
  <name>CLOSEDCHECKDETAILS<xsl:value-of select='$id'/></name> 
  <id>CLOSEDCHECKDETAILS<xsl:value-of select='$id'/></id> 
  <frame><boundedFrame><STX/><ETX/></boundedFrame></frame>
  <format>2</format> 
  <timeout>1</timeout> 
  <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
  <kind>
  <fileIO>
  <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir> 
  <retentionDays>14</retentionDays> 
  <inFile>
  <searchOrder>CREATE</searchOrder> 
  <pattern><xsl:value-of select="$path"/>\work\intf\inbound\CCDET.<xsl:value-of select='$id'/></pattern> 
  </inFile>
  <outFile>
  <existsOption /> 
  <pattern /> 
  </outFile>
  </fileIO>
  </kind>
  <diagnosticLevel>4</diagnosticLevel> 
  <connectAtStartup>true</connectAtStartup> 
  <direction>INCOMING</direction> 
  <continuous>true</continuous> 
  <dbId>-1</dbId> 
  <inboundType>BYTES</inboundType> 
  </connection>
  <connection>
  <name>MENUITEMTOTALS<xsl:value-of select='$id'/></name> 
  <id>MENUITEMTOTALS<xsl:value-of select='$id'/></id> 
  <frame><boundedFrame><STX/><ETX/></boundedFrame></frame>
  <format>2</format> 
  <timeout>1</timeout> 
  <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
  <kind>
  <fileIO>
  <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir> 
  <retentionDays>14</retentionDays> 
  <inFile>
  <searchOrder>CREATE</searchOrder> 
  <pattern><xsl:value-of select="$path"/>\work\intf\inbound\MITOTALS.<xsl:value-of select='$id'/></pattern> 
  </inFile>
  <outFile>
  <existsOption /> 
  <pattern /> 
  </outFile>
  </fileIO>
  </kind>
  <diagnosticLevel>4</diagnosticLevel> 
  <connectAtStartup>true</connectAtStartup> 
  <direction>INCOMING</direction> 
  <continuous>true</continuous> 
  <dbId>-1</dbId> 
  <inboundType>BYTES</inboundType> 
  </connection>
  <connection>
  <name>CLOSEDCHECKTOTALS<xsl:value-of select='$id'/></name> 
  <id>CLOSEDCHECKTOTALS<xsl:value-of select='$id'/></id> 
  <frame><boundedFrame><STX/><ETX/></boundedFrame></frame>
  <format>2</format> 
  <timeout>1</timeout> 
  <rawDataPath><xsl:value-of select="$path"/>\work\IntfLogs</rawDataPath> 
  <kind>
  <fileIO>
  <archiveDir><xsl:value-of select="$path"/>\work\intf\inbound\archive</archiveDir> 
  <retentionDays>14</retentionDays> 
  <inFile>
  <searchOrder>CREATE</searchOrder> 
  <pattern><xsl:value-of select="$path"/>\work\intf\inbound\CCTOT.<xsl:value-of select='$id'/></pattern> 
  </inFile>
  <outFile>
  <existsOption /> 
  <pattern /> 
  </outFile>
  </fileIO>
  </kind>
  <diagnosticLevel>4</diagnosticLevel> 
  <connectAtStartup>true</connectAtStartup> 
  <direction>INCOMING</direction> 
  <continuous>true</continuous> 
  <dbId>-1</dbId> 
  <inboundType>BYTES</inboundType> 
  </connection>
</xsl:template>

<xsl:template match='PosUnit[PosType="M8700"]' mode='perunit'>
  <xsl:variable name='id'>
      <xsl:call-template name='UnitId'>
	  <xsl:with-param name="Id" select="PosUnitId"/>
      </xsl:call-template> 
 </xsl:variable>
  <path>
  <channel>CLOSEDCHECKDETAILS<xsl:value-of select='$id'/></channel> 
  <processPath>
  <xlate>
	<script><xsl:value-of select="$path"/>\interfaces\FSI\M8700CombineItems.xsl</script>
	<outputFormat>IUnknown</outputFormat>
	<usesLookup>false</usesLookup>
	<auditInfo>
	<prefix>m8700CombineItems</prefix>
	<path><xsl:value-of select="$path"/>\work\IntfLogs</path>
	<tag>Data</tag>
	</auditInfo>
</xlate>
<xlate>
  <script><xsl:value-of select="$path"/>\interfaces\FSI\m8700Raw.xsl</script> 
  <outputFormat>IUnknown</outputFormat> 
  <usesLookup>false</usesLookup> 
  <auditInfo>
  <prefix>m8700Combine_</prefix> 
  <path><xsl:value-of select="$path"/>\work\IntfLogs</path> 
  <tag>Data</tag> 
  </auditInfo>
  </xlate>
   <merge>
  <path>
  <channel>MENUITEMTOTALS<xsl:value-of select='$id'/></channel> 
  <processPath>
  <fixedRecordParser>
  <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\m8700mitot.xml</parseTablePath> 
  </fixedRecordParser>
  </processPath>
  </path>
  <path>
  <channel>CLOSEDCHECKTOTALS<xsl:value-of select='$id'/></channel> 
  <processPath>
  <fixedRecordParser>
  <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\m8700cctot.xml</parseTablePath> 
  </fixedRecordParser>
  </processPath>
  </path>
  </merge>
  <fixedRecordParser>
  <parseTablePath><xsl:value-of select="$path"/>\interfaces\FSI\m8700ccdet.xml</parseTablePath> 
  </fixedRecordParser>
  </processPath>
  </path>
</xsl:template>

<xsl:template match='PosUnit[PosType="M8700"]' mode='pattern'>
  <xsl:variable name='id'>
      <xsl:call-template name='UnitId'>
	  <xsl:with-param name="Id" select="PosUnitId"/>
      </xsl:call-template> 
  </xsl:variable>

  <xsl:value-of select="concat($path,'\work\intf\inbound\CCDET.',$id)"/>
</xsl:template>

<!-- Unsupported Type -->

<xsl:template match='PosUnit' mode='mainprocess'>
   <!-- Ignore this unit -->
</xsl:template>

<xsl:template match='PosUnit' mode='entry'>
     <!-- Ignore this unit -->
</xsl:template>

<xsl:template match='PosUnit' mode='base'>
     <!-- Ignore this unit -->
</xsl:template>

<xsl:template match='PosUnit' mode='perunit'>
    <!-- Ignore this unit -->
</xsl:template>

<xsl:template match='PosUnit' mode='channels'>
</xsl:template>

<xsl:template match='PosUnit' mode='pattern'>
   <!-- Ignore this unit -->
</xsl:template>

<!-- GL Interface -->
<xsl:template match="Intf[Type='GL']">
   <xsl:param name="path"/>
   <connection>
      <name>GL_Financial_Interface</name>
      <id>cbord_gl</id>
      <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
      <format>1</format>
      <timeout>5</timeout>
      <rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath>
      <kind>
            <fileIO>
              <outFile>
                <!-- change here to rename CSV file -->
                <pattern><xsl:value-of select="$path"/>\Interfaces\FSI\FSSExport.xml</pattern>
                <existsOption>OVERWRITE</existsOption>
              </outFile>
              <inFile><pattern/></inFile>
            </fileIO>
      </kind>
      <diagnosticLevel>4</diagnosticLevel>
      <connectAtStartup>false</connectAtStartup>
      <direction>outgoing</direction>
      <continuous>true</continuous>
      <dbId>-1</dbId>
      <inboundType>BYTES</inboundType>
      <extendedInfo>
         <targetMapping>
	     <targetMap>
	         <msgData/>
	         <xPath/>
	         <id>CbordDB</id>
	     </targetMap>
         </targetMapping>
         <processPath>
	     <xlate>
	         <script><xsl:value-of select="$path"/>\interfaces\FSI\Splitter.xsl</script>
	         <outputFormat>ByteArrayText</outputFormat>
	         <usesLookup>false</usesLookup>
	      </xlate>

	      <xlate>
	         <script><xsl:value-of select="$path"/>\interfaces\FSI\SapRecords.xsl</script>
	         <outputFormat>IUnknown</outputFormat>
	         <usesLookup>false</usesLookup>
	         <auditInfo>
		    <prefix>Records</prefix>
		    <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		    <tag>MsgList</tag>
	         </auditInfo>
                </xlate>

	       <xlate>
	          <script><xsl:value-of select="$path"/>\interfaces\FSI\GroupItems.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <prefix>ItemGroup</prefix>
		     <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		     <tag>MsgList</tag>
    	          </auditInfo>
	       </xlate>

	       <xlate>
	          <script><xsl:value-of select="$path"/>\interfaces\FSI\GroupHeaders.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <prefix>UnitGroup</prefix>
		     <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		     <tag>MsgList</tag>
    	          </auditInfo>
	       </xlate>

	       <xlate>
	          <script><xsl:value-of select="$path"/>\interfaces\FSI\groupByOp.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <prefix>operation</prefix>
		     <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		     <tag>MsgList</tag>
    	          </auditInfo>
	       </xlate>

	       <xlate>
	          <script><xsl:value-of select="$path"/>\interfaces\FSI\groupByLocale.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <prefix>Locale</prefix>
		     <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		     <tag>MsgList</tag>
    	          </auditInfo>
	       </xlate>

	       <xlate>
	          <script><xsl:value-of select="$path"/>\interfaces\FSI\Key.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <prefix>Key</prefix>
		     <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		     <tag>MsgList</tag>
	          </auditInfo>
	       </xlate>

	       <xlate>
	          <script><xsl:value-of select="$path"/>\Interfaces\FSI\Filter.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <tag>MsgList</tag>
		     <prefix>Filter</prefix>
		     <path><xsl:value-of select="$path"/>\work\intflogs</path>
	          </auditInfo>
	       </xlate>
         </processPath>
      </extendedInfo>
</connection>
</xsl:template>

<!-- GL Interface -->
<xsl:template match="Intf[Type='AP']">
   <xsl:param name="path"/>
   <connection>
      <name>AP_Financial_Interface</name>
      <id>cbord_ap</id>
      <frame><boundedFrame><STX></STX><ETX></ETX></boundedFrame></frame>
      <format>1</format>
      <timeout>5</timeout>
      <rawDataPath><xsl:value-of select="$path"/>\work\intfLogs</rawDataPath>
      <kind>
            <fileIO>
              <outFile>
                <pattern><xsl:value-of select="$path"/>\work\intf\inbound\archive\FSSapExport.txt</pattern>
                <existsOption>OVERWRITE</existsOption>
              </outFile>
              <inFile><pattern/></inFile>
            </fileIO>
      </kind>
      <diagnosticLevel>4</diagnosticLevel>
      <connectAtStartup>false</connectAtStartup>
      <direction>outgoing</direction>
      <continuous>true</continuous>
      <dbId>-1</dbId>
      <inboundType>BYTES</inboundType>
      <extendedInfo>
         <targetMapping>
	     <targetMap>
	         <msgData/>
	         <xPath/>
	         <id>CbordDB</id>
	     </targetMap>
         </targetMapping>
         <processPath>
	     <xlate>
	         <script><xsl:value-of select="$path"/>\interfaces\FSI\Splitter.xsl</script>
	         <outputFormat>ByteArrayText</outputFormat>
	         <usesLookup>false</usesLookup>
	      </xlate>

	      <xlate>
	         <script><xsl:value-of select="$path"/>\interfaces\FSI\SapRecords.xsl</script>
	         <outputFormat>IUnknown</outputFormat>
	         <usesLookup>false</usesLookup>
	         <auditInfo>
		    <prefix>Records</prefix>
		    <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		    <tag>MsgList</tag>
	         </auditInfo>
                </xlate>

	       <xlate>
	          <script><xsl:value-of select="$path"/>\interfaces\FSI\GroupItems.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <prefix>ItemGroup</prefix>
		     <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		     <tag>MsgList</tag>
    	          </auditInfo>
	       </xlate>


	       <xlate>
	          <script><xsl:value-of select="$path"/>\interfaces\FSI\GroupHeaders.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <prefix>UnitGroup</prefix>
		     <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		     <tag>MsgList</tag>
    	          </auditInfo>
	       </xlate>

	       <xlate>
	          <script><xsl:value-of select="$path"/>\interfaces\FSI\Key.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <prefix>Key</prefix>
		     <path><xsl:value-of select="$path"/>\work\IntfLogs</path>
		     <tag>MsgList</tag>
	          </auditInfo>
	       </xlate>
	       <xlate>
	          <script><xsl:value-of select="$path"/>\Interfaces\FSI\Filter.xsl</script>
	          <outputFormat>IUnknown</outputFormat>
	          <usesLookup>false</usesLookup>
	          <auditInfo>
		     <tag>MsgList</tag>
		     <prefix>Filter</prefix>
		     <path><xsl:value-of select="$path"/>\work\intflogs</path>
	          </auditInfo>
	       </xlate>
         </processPath>
      </extendedInfo>
   </connection>

</xsl:template>

<xsl:template name='UnitId'>
   <xsl:param name="Id"/>
    <xsl:choose>
              <xsl:when test="contains($Id, '-')">
	           <xsl:value-of select = "number(substring-after($Id,'-'))"/> 
              </xsl:when>
              <xsl:when test="contains($Id, '_')">
	           <xsl:value-of select = "number(substring-after($Id,'_'))"/> 
              </xsl:when>
              <xsl:when test="contains($Id, '.')">
	           <xsl:value-of select = "number(substring-after($Id,'.'))"/> 
              </xsl:when>
              <xsl:otherwise>
	           <xsl:value-of select = "number($Id)"/> 
	      </xsl:otherwise>
    </xsl:choose>
</xsl:template>

   <msxsl:script implements-prefix="cbord">
     <![CDATA[

	function path(str)
	{
              var path = str.split("\\");
   	      return path[0]+"\\"+path[1];
        }

    ]]>  </msxsl:script>

</xsl:stylesheet>
