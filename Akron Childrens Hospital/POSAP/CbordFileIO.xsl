<!-- 	stylesheet for creating detail ouput records for the CSV APGL Interface: version $Revision:   1.5  $
     Changes:
     10/08/03	SGM	Ouput the detail records.
     11/20/03	GTT	Modified to handle any output format
     			Modified to handle options for opening a file:
     				ForReading = for future use.
     				ForWriting = overwrte to file.
     				ForAppending = append to the file. 
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:cbord="http://cbord.com/xsltns"
  version="1.0">
  
  <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" /> 
<xsl:preserve-space elements="field"/>
   
<msxsl:script implements-prefix="cbord">
  <![CDATA[
   /* mode = 1: ForReading mode = 2: ForWriting mode = 8;ForAppending */
   var TristateUseDefault = -2, TristateTrue = -1, TristateFalse = 0;

    //create an output file
    function createOutputFile(xmlData,xslFile,fileName, mode)
    {
          var objXSL = new ActiveXObject('MSXML2.DOMDocument.3.0');          
          var newNode;
          var strResult;
          var fso, outfile, tempFile;


          newNode = xmlData.nextNode();
 
          //set the parameter properties
          objXSL.validateOnParse = true;
          objXSL.preserveWhiteSpace = true;
     
          //load the xsl stylesheet and check for errors
          objXSL.load(xslFile);
                 
          //all must be ok, so perform transformation
          strResult = newNode.transformNode(objXSL);

	  //prepare to write results of transformation to outFile
	  var fso = new ActiveXObject("Scripting.FileSystemObject")
	  if(fso.FileExists(fileName))
       	    ;
   	  else
      	    fso.CreateTextFile(fileName ,1); 

          outfile = fso.GetFile(fileName).OpenAsTextStream(mode, TristateUseDefault);
	  outfile.Write(strResult)

	  //close the outfile
	  outfile.Close();	  

          return fileName;
    } 
    
  //Get the path to the interface main directory
  function CbordwinPath(file, folder)
  {
    var fso = new ActiveXObject("Scripting.FileSystemObject")
    var path = fso.GetAbsolutePathName(file)
    path = path.split('\\');
    var retPath = path[0];
    var i=0;
     
    for(i=1; i<path.length; i++)
       if(path[i].toUpperCase() == folder.toUpperCase())
       { 
           retPath +='\\\\' + path[i];
           break; 
       }
       else
	     retPath += '\\\\' + path[i];
    return retPath;
  }
 

]]>
  </msxsl:script>
</xsl:stylesheet>

