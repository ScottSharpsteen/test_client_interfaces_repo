<?xml version="1.0" encoding="iso-8859-1"?>

<!-- POS: Combine duplicate transactions and removed cancelled and in training : $Revision:   1.0  $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<!-- Group headers by TRANSNUM for combining duplicate transactions, using Muenchian Method -->
<xsl:key name="group-by-xact" match="HEADER" use="concat(UNITID, TRANSNUM)"/>

<!-- Group items by TRANSNUM and PLU for combining duplicate items, using Muenchian Method -->
<xsl:key name="group-items" match="ITEM" use="concat(PLU, ../../HEADER/UNITID, ../../HEADER/TRANSNUM)"/>


<xsl:template match="/">
  <CTalkServiceMenuImport>
  <xsl:apply-templates mode="combine"
                       select="//TRANSACTION/HEADER[count(. | key('group-by-xact',concat(UNITID,TRANSNUM))[1])=1]"/>
  <xsl:copy-of select="document('ConfigData.xml')//PosUnitList"/>

  </CTalkServiceMenuImport>
</xsl:template>

<!-- called once for each unique TRANSNUM -->
<xsl:template match="HEADER" mode="combine">
  <xsl:call-template name="combineActive">
    <xsl:with-param name="headers" select="key('group-by-xact',concat(UNITID,TRANSNUM))"/>
  </xsl:call-template>
</xsl:template>

<!-- called once for each unique PLU,TRANSNUM pair -->
<xsl:template match="ITEM" mode="combine">
  <xsl:call-template name="combineItems">
    <xsl:with-param name="items" select="key('group-items',concat(PLU,../../HEADER/UNITID, ../../HEADER/TRANSNUM))"/>
  </xsl:call-template>
</xsl:template>

<!-- combine all ACTIVE transactions: (1) combine duplicate items (2) remove void items (3) Union the rest -->
<xsl:template name="combineActive">
  <xsl:param name="headers"/>

  <xsl:choose>
    <!-- filter cancelled, in training, added to, and Open transactions -->
    <xsl:when test="$headers/STATUSLIST[CANCELLED='Y']"/>
    <xsl:when test="$headers/STATUSLIST[TRAINING='Y']"/>
    <xsl:when test="$headers/STATUSLIST[ADDEDTO='Y']"/>
    <xsl:when test="$headers/STATUSLIST[OPENCHK='Y']"/>

    <!-- no duplicates, just copy the transaction (except for void items) -->
    <xsl:when test="count($headers)=1">
      <xsl:apply-templates select="$headers[1]/.."/>
    </xsl:when>

    <!-- combine duplicate items -->
    <xsl:otherwise>
      <TRANSACTION>
      <!-- copy one of the headers: always copy last (latest) header -->
      <xsl:copy-of select="$headers[count($headers)]"/>
        <ITEMLIST>
          <xsl:apply-templates
               mode="combine"
               select="*/TRANSACTION/ITEMLIST/ITEM[count(. | key('group-items',concat(PLU,$headers[1]/UNITID,$headers[1]/TRANSNUM))[1])=1]"/>
        </ITEMLIST>
      </TRANSACTION>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- combine all duplicate ITEMS: (1) combine duplicate items (2) remove void items (3) Union the rest -->
<xsl:template name="combineItems">
  <xsl:param name="items"/>

  <xsl:choose>
    <!-- filter cancelled, in training, and added to transactions -->
    <xsl:when test="$items[VOID='v']"/>

    <!-- no duplicates, just copy the transaction (except for void items) -->
    <xsl:when test="count($items)=1">
      <xsl:apply-templates select="$items[1]/.."/>
    </xsl:when>

    <!-- combine duplicate items: by summing the quantities -->
    <xsl:otherwise>
      <ITEM>
        <xsl:copy-of select="ITEMNAME"/>
        <xsl:copy-of select="UNITID"/>
        <QUANTITY><xsl:value-of select="sum($items/QUANTITY)"/></QUANTITY>
        <xsl:copy-of select="PRICE"/>
        <xsl:copy-of select="DISCOUNTPRICE"/>
        <xsl:copy-of select="SOLDBYWT"/>
        <xsl:copy-of select="PLU"/>
      </ITEM>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>



<!-- ============= COPY RULES ========================================================== -->
<!-- Handle any node not yet matched, strip out any comments or processing instructions -->
<xsl:template match="*|@*|text()">
  <xsl:copy><xsl:apply-templates select="*|@*|text()"/></xsl:copy>
</xsl:template>

<!-- Filter void items -->
<xsl:template match="ITEM[VOID='Y']"/>
<!--xsl:template match="ITEM[PRICE=0]"/-->

</xsl:stylesheet>
