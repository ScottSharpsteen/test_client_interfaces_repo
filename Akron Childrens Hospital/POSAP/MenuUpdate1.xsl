<?xml version="1.0" encoding="iso-8859-1"?>
<!-- 
	stylesheet for validating raw generic POS messages: version $Revision:   1.1  $
	Author GTT
	Changes:


-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                version="1.0">


<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>
<xsl:variable name="yesterday" select="cbord:yesterday()"/>

<xsl:template match="/">
<CTalkServiceMenuImport>
  <xsl:variable name="cnt1" select="count(//ServiceMenu) div 3"/>

  <xsl:apply-templates select="//ServiceMenu[position() &lt;= $cnt1]" /> 
</CTalkServiceMenuImport>
</xsl:template>

<xsl:template match="ServiceMenu">
 <ServiceMenu> 
      <xsl:copy-of select="./Header"/>
      <xsl:copy-of select="./ItemList"/> 
 </ServiceMenu> 
</xsl:template>


<msxsl:script implements-prefix="cbord">
  <![CDATA[

  var tdate = 0; 
  var validDate = ""; 

  function yesterday()
  {
    var MinMilli = 1000 * 60;
    var HrMilli = MinMilli * 60;
    var DyMilli = HrMilli * 24;

    var now = new Date();

    var dateInMilliSeconds = now.getTime();
    // go back one day
    dateInMilliSeconds = dateInMilliSeconds-DyMilli;
    now.setTime(dateInMilliSeconds);

    var tyear = now.getFullYear();
    var tmon = now.getMonth();
        tmon = tmon + 1;
    var tday = now.getDate();
    tday = new Number(tday);
    if (tday < 10)
       tday = "0"+tday;
    tmon = new Number(tmon);
    if(tmon <10)
      tmon = "0"+tmon;
     
    tdate = tyear + "/" + tmon + "/" + tday; 
    return tdate;
  
  }  


  ]]>
</msxsl:script>

</xsl:stylesheet>

