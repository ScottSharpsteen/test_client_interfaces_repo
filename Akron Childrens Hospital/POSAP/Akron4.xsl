
<xsl:stylesheet   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  version="1.0">

 <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />
<xsl:variable name="EndOfRec"><xsl:text>&#x0d;&#x0a;</xsl:text></xsl:variable>

 <xsl:template match="/">
 <POS> 
     <xsl:apply-templates select="//Transaction">
	  <xsl:sort select="*/VendorId"/>
	  <xsl:sort select="*/TransactionDate"/>
	  <xsl:sort select="*/DeliveryDate"/>
	  <xsl:sort select="*/PRNumber"/>
     </xsl:apply-templates>     
  </POS>   
 </xsl:template>

<xsl:template match='Transaction'>
   <PORecord>
         <xsl:apply-templates select="ItemList" mode="PO"/> 
   </PORecord>   
 </xsl:template>

<xsl:template match="Operation">
  <xsl:copy>
    <xsl:value-of select="."/>
  </xsl:copy>
</xsl:template>

<xsl:template match="TransmissionId">
  <xsl:copy>
    <xsl:value-of select="."/>
  </xsl:copy>
</xsl:template>


<xsl:template match="ItemList" mode="PO">
      <xsl:apply-templates select="Item[1]" mode="first">
	  <xsl:with-param name="total" select="sum(*/Total)"/>
	  <xsl:with-param name="tax" select="sum(*/TaxTotal)"/>
      </xsl:apply-templates> 
      <xsl:apply-templates select="Item[position() != 1]" mode="other">
	  <xsl:with-param name="total" select="sum(*/Total)"/>
	  <xsl:with-param name="tax" select="0"/>
      </xsl:apply-templates> 
</xsl:template>

<xsl:template match="Item" mode="first">
   <xsl:param name="total"/>
   <xsl:param name="tax"/>
   <Record>
      <xsl:value-of select="../../*/Today"/>
      <xsl:value-of select="../../*/VendorId"/>
      <xsl:value-of select="../../*/InviceNumber"/>
      <xsl:value-of select="../../*/PRNumber"/>
      <xsl:value-of select="../../*/DeliveryDate"/> 
      <xsl:value-of select="../../*/TransactionDate"/>
      <xsl:value-of select="format-number($total,'00000000+;00000000-')"/>
      <xsl:value-of select="format-number(Total,'00000000+;00000000-')"/>
      <!--xsl:if test="Total &lt; 0">0000000-</xsl:if-->
      <!--xsl:if test="Total &gt; 0">0000000+</xsl:if-->
      <!--xsl:value-of select="format-number($tax,'0000000+;0000000-')"/-->
      <!--xsl:value-of select="'  '"/-->
      <xsl:value-of select="AccountNumber"/>
      <xsl:value-of select="$EndOfRec"/>
   </Record>   
 </xsl:template>
 
 <xsl:template match="Item" mode="other">
   <xsl:param name="total"/>
   <xsl:param name="tax"/>
   <Record>
      <xsl:value-of select="../../*/Today"/>
      <xsl:value-of select="../../*/VendorId"/>
      <xsl:value-of select="../../*/InviceNumber"/>
      <xsl:value-of select="../../*/PRNumber"/>
      <xsl:value-of select="../../*/DeliveryDate"/>
      <xsl:value-of select="../../*/TransactionDate"/>
      <xsl:value-of select="format-number($total,'00000000+;00000000-')"/>
      <xsl:value-of select="format-number(Total,'00000000+;00000000-')"/>
      <!--xsl:if test="Total &lt; 0">0000000-</xsl:if-->
      <!--xsl:if test="Total &gt; 0">0000000+</xsl:if-->
      <!--xsl:if test="Total &lt; 0">0000000-</xsl:if-->
      <!--xsl:if test="Total &gt; 0">0000000+</xsl:if-->
      <!--xsl:value-of select="'  '"/-->
      <xsl:value-of select="AccountNumber"/>
      <xsl:value-of select="$EndOfRec"/>
   </Record>   
 </xsl:template>

</xsl:stylesheet>
