<?xml version="1.0" encoding="iso-8859-1"?>
<!--    
	stylesheet for grouping POS messages: version $Revision:   1.1  $
	Author: PDA
	Changes:
	02/15/2002	GTT	Add PeriodId for sorting later.
	     
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>


<xsl:key name="group-transactions" match="HEADER" use="concat(ServiceUnitExtId,MEALPERIOD,BUSINESSDATE)" />

<xsl:template match="/">

<CTalkServiceMenuImport>

  <xsl:for-each select="//TRANSACTION/HEADER[count(. | key('group-transactions', 
                                                    concat(ServiceUnitExtId,MEALPERIOD,BUSINESSDATE))[1])=1]">
    <xsl:sort select="concat(ServiceUnitExtId,MEALPERIOD,BUSINESSDATE)" data-type="text" order="ascending"/>
    <xsl:variable name="keyVal" select="concat(ServiceUnitExtId,MEALPERIOD,BUSINESSDATE)"/>
    <ServiceMenu>
      <Header type="Update">
	    <ServiceDate><xsl:value-of select="./BUSINESSDATE"/></ServiceDate>
	    <MealPeriod><xsl:value-of select="./MEALPERIOD"/></MealPeriod>
	    <ServiceUnitExtId><xsl:value-of select="./ServiceUnitExtId"/></ServiceUnitExtId>
	    <CustomerCount>
                  <xsl:choose>
		    <xsl:when test="sum( key('group-transactions',$keyVal)/COVERCNT) &gt; 0">
                          <xsl:value-of select="sum( key('group-transactions',$keyVal)/COVERCNT)"/>
                    </xsl:when>
                    <xsl:otherwise>100</xsl:otherwise>
                   </xsl:choose>
            </CustomerCount>
        <xsl:copy-of select="BIZRULES"/>
      </Header>

	  <ItemList>
        <xsl:apply-templates select="key('group-transactions',$keyVal)/../ITEMLIST/ITEM"/>
      </ItemList>
    </ServiceMenu>
  </xsl:for-each>
</CTalkServiceMenuImport>
</xsl:template>

<xsl:template match="ITEM">
  <ITEM>
	<xsl:copy-of select="./ITEMNAME"/>
	<xsl:copy-of select="./KEYVALUE"/>         
	<xsl:copy-of select="./QUANTITY"/>
	<xsl:copy-of select="./PRICE"/>
	<xsl:copy-of select="./VOID"/>
	<xsl:copy-of select="./PLU"/>
   </ITEM>
</xsl:template>

<!-- Handle any node not yet matched, strip out any comments or processing instructions -->
<xsl:template match="*|@*|text()">
  <xsl:copy><xsl:apply-templates select="*|@*|text()"/></xsl:copy>
</xsl:template>

</xsl:stylesheet>
