<xsl:stylesheet   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />
  <xsl:key name="group-items" match="Item" use="ItemKey"/>

  
 <xsl:template match="/">
 <CCIFAPGL> 
      <xsl:apply-templates select="//Operation"/>   
      <xsl:apply-templates select="//TransmissionId"/>   
      <xsl:apply-templates select="//Transaction">   
	  <xsl:sort select="*/VendorId"/>
	  <xsl:sort select="*/TransactionDate"/>
	  <xsl:sort select="*/DeliveryDate"/>
	  <xsl:sort select="*/PRNumber"/>
      </xsl:apply-templates>       
  </CCIFAPGL>   
 </xsl:template>

<xsl:template match='Transaction'>
  <xsl:if test="sum(*/*/Total) != 0">
       <Transaction>
          <xsl:apply-templates select="Header"/>
          <xsl:apply-templates select="ItemList"/>
       </Transaction>  
   </xsl:if> 
 </xsl:template>


<xsl:template match='ItemList'>
   <ItemList>
      <xsl:apply-templates select="Item[generate-id(.)=generate-id(key('group-items',ItemKey))]"> 
	  <xsl:sort select="AccountNumber"/>
       </xsl:apply-templates> 
   </ItemList>   
 </xsl:template>

<xsl:template match='Item'>
         <Item>
	      <xsl:apply-templates select="*"/>
         </Item>    
 </xsl:template>


<xsl:template match='Total'>
   <Total>
      <xsl:variable name="valKey" select="../ItemKey"/>
      <xsl:value-of select="sum(key('group-items', $valKey)/Total)"/>
   </Total>   
 </xsl:template>

<xsl:template match='TaxTotal[1]'>
   <TaxTotal>
      <xsl:variable name="valKey" select="../ItemKey"/>
      <xsl:value-of select="sum(key('group-items', $valKey)/TaxTotal[1])"/>
   </TaxTotal>   
 </xsl:template>

<xsl:template match='Header'><Header><xsl:apply-templates select="*"/></Header></xsl:template>
<xsl:template match='AccountNumber'><AccountNumber><xsl:value-of select="."/></AccountNumber></xsl:template>
<xsl:template match='VendorId'><VendorId><xsl:value-of select="."/></VendorId></xsl:template>
<xsl:template match='InviceNumber'><InviceNumber><xsl:value-of select="."/></InviceNumber></xsl:template>
<xsl:template match='Today'><Today><xsl:value-of select="."/></Today></xsl:template>
<xsl:template match='DeliveryDate'><DeliveryDate><xsl:value-of select="."/></DeliveryDate></xsl:template>
<xsl:template match='TransactionDate'><TransactionDate><xsl:value-of select="."/></TransactionDate></xsl:template>
<xsl:template match='PRNumber'><PRNumber><xsl:value-of select="."/></PRNumber></xsl:template>
<xsl:template match='Operation'><Operation><xsl:value-of select="."/></Operation></xsl:template>
<xsl:template match='TransmissionId'><TransmissionId><xsl:value-of select="."/></TransmissionId></xsl:template>
<xsl:template match='Description'><Description><xsl:value-of select="."/></Description></xsl:template>
<xsl:template match='Operation'><Operation><xsl:value-of select="."/></Operation></xsl:template>
<xsl:template match='TransmissionId'><TransmissionId><xsl:value-of select="."/></TransmissionId></xsl:template>
<xsl:template match='UnitId'><UnitId><xsl:value-of select="."/></UnitId></xsl:template>
<xsl:template match='ItemKey'/>

</xsl:stylesheet>
