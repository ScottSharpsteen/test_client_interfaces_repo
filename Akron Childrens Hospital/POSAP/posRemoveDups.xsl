<?xml version="1.0" encoding="UTF-8"?>
<!-- 	stylesheet for removing duplicate Raw 3700 line items: version $Revision:   1.1 
	Author: GTT		
	Date	10/29/04
	Changes:
		10/29/04	Need to handle items in a multi key states.

 $-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<xsl:key name="group-trans" match="Transaction" use="concat(FixedPeriod,BUSINESSDATE,UNITID,STOREID,MENUITEMNUM)" />

<xsl:template match="/">
<CTalkRawPOSTransactions>
  <xsl:for-each select="//Transaction[count(. | key('group-trans', concat(FixedPeriod,BUSINESSDATE,UNITID,STOREID,MENUITEMNUM))[1])=1]">
        <xsl:apply-templates select="."/>
  </xsl:for-each>
</CTalkRawPOSTransactions>
</xsl:template>

<xsl:template match="Transaction">
<Transaction>
    <xsl:apply-templates select="*"/>
</Transaction>
</xsl:template>


<xsl:template match="COVERCNT">
<COVERCNT>
   <xsl:variable name="transCnt" select="sum(key('group-trans', concat(../FixedPeriod,../BUSINESSDATE,../UNITID,../STOREID,../MENUITEMNUM))/TRANSCNT)"/>
   <xsl:variable name="coverCnt" select="sum(key('group-trans', concat(../FixedPeriod,../BUSINESSDATE,../UNITID,../STOREID,../MENUITEMNUM))/COVERCNT)"/>
   <xsl:choose>
      <xsl:when test="$transCnt &gt; 0">
          <xsl:value-of select="$transCnt"/>
      </xsl:when>
      <xsl:otherwise>
          <xsl:value-of select="$coverCnt"/>
      </xsl:otherwise>
   </xsl:choose> 
</COVERCNT>
</xsl:template>
<xsl:template match="TRANSCNT"/>

<!-- Handle any node not yet matched, strip out any comments or processing instructions -->
<xsl:template match="*|@*|text()">
  <xsl:copy><xsl:apply-templates select="*|@*|text()"/></xsl:copy>
</xsl:template>


</xsl:stylesheet>
