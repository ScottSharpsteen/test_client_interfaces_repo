<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:cbord="http://cbord.com/xsltns"
  version="1.0">

  <msxsl:script implements-prefix="cbord">
  <![CDATA[
    var seqNumber=0;
    var date;
    var timezone;
    var time;
    var year;
    var month;
    var day;
    var hour;
    var minute;
    var second;
    var millisecond;
    var position = 0;
    var files = new Array();
    var lk = 0;

   var days = new Array(7);
   days[1] = "Sunday";
   days[2] = "Monday";
   days[3] = "Tuesday"; 
   days[4] = "Wednesday";
   days[5] = "Thursday";
   days[6] = "Friday";
   days[7] = "Saturday";

function getDayName(date)
{
   var today = new Date();
   var year = new Number(date.substr(0,4));
   var mon = new Number(date.substr(5,2));
    // date objects use 0-11 as months
    mon = mon -1;
    var day = new Number(date.substr(8,2));

    // intialize date object to this date
    var date = new Date(year,mon,day);
   
    return days[date.getDay()+1];
    
}

  function Round(val1, val2, power)
  {

    //Val2 and power are optional. If val2 is empty assume 1, if power is empty assume 0
    var tempVal2 = val2
    tempVal2 = (!tempVal2 ? 0 : tempVal2)
    val2 = (!power ? 1 : val2);
    power = (!power ? tempVal2 : power);
    return Math.round(val1 * val2 * Math.pow(10,power))
  }


// left pad a string to make the given size with the given character
function padLeft(value,pad,size)
{
        var padded = "" + value;
        while (padded.length < size)
        {
           padded = pad+padded;
        }
        return padded;
}

// right pad a string to make the given size with the given character
function padRight(value,pad,size)
{
       var padded = value;
       while (padded.length < size)
       {
          padded = padded+pad;
       }
       return padded;
}

function yesterday()
{
      var MinMilli = 1000 * 60;
      var HrMilli = MinMilli * 60;
      var DyMilli = HrMilli * 24;
      var now = new Date();
      var dateInMilliSeconds = now.getTime();

    // go back one day
    dateInMilliSeconds = dateInMilliSeconds-DyMilli;
    now.setTime(dateInMilliSeconds);
    var tyear = now.getFullYear();
    var tmon = now.getMonth();
    tmon = tmon + 1;
    var tday = now.getDate();
    tday = new Number(tday);
    if (tday < 10)
    tday = "0"+tday;
    tmon = new Number(tmon);
    if(tmon <10)
    tmon = "0"+tmon;
    tdate = tyear + "/" + tmon + "/" + tday; 
    return tdate;
}

//Get today's date
function today(fChar)
{
    //intialize date object to this date
    var date = new Date();

    //translate back to a string
    var year = date.getFullYear();
    var mon = date.getMonth() + 1;
    var day = date.getDate();
    if (mon < 10) mon = "0" + mon;
    else
      mon = "" + mon;
    if (day < 10) day = "0" + day;
    else  day = "" + day;

    return ""+ year + fChar + mon + fChar + day;
}  

function getTime(x)
{
    var now = new Date();
    var tHour = now.getHours() + x;
    tHour = new Number(tHour);
    if (tHour < 10)
       tHour = "0"+tHour;
    else
       tHour=""+tHour;

    var tMin = now.getMinutes();
    tMin = new Number(tMin);
     if (tMin < 10)
        tMin = "0"+tMin;
     else
        tMin = ""+tMin;

    var tSec = now.getSeconds();
    tSec = new Number(tSec);
    if (tSec < 10)
        tSec = "0"+tSec;
    else
        esec = ""+tSec;

    return tHour+tMin+tSec;
    
}    


function multAndAddToSum(p,q,i)
{
     var sign = "";
     var sp = ""+p;
     var sq = ""+q;
     var spindex = sp.indexOf(".");
     var sqindex = sq.indexOf(".");
     if(spindex  < 0) sp += ".0";
     if(sqindex  < 0) sq += ".0";
     
    // remove the decimal point and create two integers

    var p2=sp.split(".");
    var q2=sq.split(".");
    var pmant = p2[1]+"";
    var pchar = p2[0]+"";
    var qmant = q2[1]+"";
    var qchar = q2[0]+"";
    var ind = pmant.length +qmant.length;
    var price=new Number(pchar + pmant);
    var qty=new Number(qchar + qmant);
 
   // perform the multiplication
    var prod=price * qty;
    var sprod = ""+prod;

    //Determine the sign value
    if(prod < 0)
    { 
       sign = sprod.substring(0,1);
       sprod = sprod.substring(1, sprod.length);
    }
    var nprod = new Number(sprod);

   // divide by 10**ind-2 and round to get implied decimal of two places
    var prodAsMoney = Math.round(nprod/Math.pow(10,ind - i));
   //Sum=Sum + new Number(sign + prodAsMoney);

    return ""+new Number(sign + prodAsMoney);
    //return ""+price+","+qty+","+prod+","+ind;

}

function isStrEqNoCase(str1,str2)
{
     return str1.toUpperCase()==str2.toUpperCase();
}

function StrToUpper(str)
{
     return str.toUpperCase();
}


function strReplace(str1, repChar, str2)
{
     return str1.replace(repChar, str2);
}


function hasChar(str, char)
{
     return str.search(char);
}


function setDateTime()
{
    var now = new Date();
    var tyear = now.getFullYear();

    var tmon = now.getMonth();
    tmon = tmon + 1;
    tmon = new Number(tmon);
    if(tmon <10)
    tmon = "0"+tmon;

    var tday = now.getDate();
    tday = new Number(tday);
    if (tday < 10)
    tday = "0"+tday;

    var tHour = now.getHours();
    tHour = new Number(tHour);
    if (tHour < 10)
    tHour = "0"+tHour;

    var tMin = now.getMinutes();
    tMin = new Number(tMin);
    if (tMin < 10)
    tMin = "0"+tMin;

    var tSec = now.getSeconds();
    tSec = new Number(tSec);
    if (tSec < 10)
    tSec = "0"+tSec;


    var tInSec1 = now.getTime();
    var tInSec2 = getSecs(tInSec1 /1000);
    var tbMills = tInSec1 - tInSec2 * 1000;    
    year = ""+tyear;
    month = "" + tmon;
    day = "" + tday;
    hour = ""+ tHour;
    minute = "" + tMin;
    second = "" + tSec;
    millisecond = padLeft(new String(tbMills),'0', 4);

    return "";
    
} 

   function getSecs(time)
   {  
      var time = time+ "";
      var myTime = time.split('.');
      return new Number(myTime[0]);
   }  


   function setTimeZone()
   {
       /*
           Note: We will return these values for:
           EST = -0500
           PST = -0800
           CST = -0600
           MST = -0700 
       */
      var thatdate = new Date(20010101); //for No daylight savings
      var thisdate=new Date;

     if (thisdate.getTimezoneOffset()==thatdate.getTimezoneOffset())
        zone = thisdate.getTimezoneOffset() / 60;
     else
        zone = thatdate.getTimezoneOffset() / 60;

       timezone = '-0'+zone+'00';   
       return "";
   } 

 function WeekeEndDate(cdate)
 {
    
    cdate = ""+cdate;
    var year = new Number(cdate.substr(0,4));
    var mon = new Number(cdate.substr(5,2));
    // date objects use 0-11 as months
    mon = mon -1;
    var day = new Number(cdate.substr(8,2));

    // intialize date object to this date
    var date = new Date(year,mon,day);

    var tyear = date.getFullYear();
    var tmon = date.getMonth();
    var mday = date.getDate();
    var wday = date.getDay()+1;
    var mdays = daysinMonth(tmon,year);
    var dDiff = 7 - wday;
    
    if(mday + dDiff > mdays)
    {
       tmon++;
       if(tmon > 11)
       {
          tmon = 0;
          tyear++; 
       }
       mday = (mday + dDiff) - mdays;
    }
    else
      mday += dDiff;
  
    
    if (mday < 10)
    mday = "0"+mday;

    tmon += 1; 
    if (tmon < 10)
    tmon = "0"+tmon;

    return tyear+"/"+tmon+"/"+mday;
    
   } 

 function MontheEndDate(cdate)
 {
    
    cdate = ""+cdate;
    var year = new Number(cdate.substr(0,4));
    var mon = new Number(cdate.substr(5,2));
    // date objects use 0-11 as months
    mon = mon -1;
    var day = new Number(cdate.substr(8,2));

    // intialize date object to this date
    var date = new Date(year,mon,day);

    var tyear = date.getFullYear();
    var tmon = date.getMonth();
    var mdays = daysinMonth(tmon,tyear);
 
    tmon += 1; 
    if (tmon < 10)
    tmon = "0"+tmon;

    return tyear+"/"+tmon+"/"+mdays;
    
   } 


  function getTDYear(date)
  {
     return date.substr(0,4);
  } 



  function getTDMonth(date)
  {
     return date.substr(5,2);
  } 


  function getTDDay(date)
  {
     return date.substr(8,2);
  } 




  function getwYear(date)
  {
     date = WeekeEndDate(date);
     return date.substr(0,4);
  } 



  function getwMonth(date)
  {
     date = WeekeEndDate(date);
     return date.substr(5,2);
  } 


  function getwDay(date)
  {
     date = WeekeEndDate(date);
     return date.substr(8,2);
  } 


  function getmYear(date)
  {
     date = MontheEndDate(date);
     return date.substr(0,4);
  } 



  function getmMonth(date)
  {
     date = MontheEndDate(date);
     return date.substr(5,2);
  } 


  function getmDay(date)
  {
     date = MontheEndDate(date);
     return date.substr(8,2);
  } 


  function daysinMonth(month, year)
  {
    var nDays;

  if(month == 0 || month == 2 ||month == 4 ||month == 6 || month == 7 || month == 9 || month == 11 ) //Months Having 31 days 0-11
  {
     nDays=31;
   }
  else if(month == 3 ||month == 5 || month == 8 ||month == 10)
  {
     nDays=30;
   } //Months having 30 Days
  else if((month == 1) && (year%4 == 0)) //Feb at a leap year
  {
     nDays=29;
   }
  else
  {
      nDays=28;
   } //Otherwise Feb with 28days

    return ""+nDays;
  }


   function getYear()
   {
      return ""+year;
   }
 
   function getMonth()
   {
      return ""+month;
   }

   function getDay()
   {
      return ""+day;
   }


   function getHour()
   {
      return ""+hour;
   }
 
   function getMinute()
   {
      return ""+minute;
   }

   function getSecond()
   {
      return ""+second;
   }

   function getMilliSecond()
   {
      return ""+millisecond;
   }


   function getTimeZone()
   {
      return timezone;
    }  
    

    ]]>
  </msxsl:script>

</xsl:stylesheet>