<?xml version="1.0" encoding="iso-8859-1"?>
<!-- 	stylesheet for Sorting Raw 3700 line items : version $Revision:   1.1 
	Author: GTT		
	Date	06/16/04
	Changes:
		06/16/04	Sort all line items by date, store, unit, period and menuitem number.

 $-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<xsl:template match="/">
<CTalkRawPOSTransactions>
      <xsl:apply-templates select="//Transaction">
         <xsl:sort select="BUSINESSDATE"/>
         <xsl:sort select="STOREID" data-type="number" order="ascending"/>
         <xsl:sort select="UNITID" data-type="number" order="ascending"/>
         <xsl:sort select="FixedPeriod" data-type="number" order="ascending"/>
         <xsl:sort select="MENUITEMNUM" data-type="number" order="ascending"/>
      </xsl:apply-templates>
</CTalkRawPOSTransactions>
</xsl:template>

<xsl:template match="Transaction">
  <xsl:copy-of select="."/>
</xsl:template>

</xsl:stylesheet>
