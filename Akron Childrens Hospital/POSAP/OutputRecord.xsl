﻿<?xml version="1.0" encoding="UTF-8"?>
<!-- 	stylesheet for creating detail ouput records for the CSV APGL Interface: version $Revision:   1.5  $
     Changes:
     10/06/03	GTT	Ouput the detail records.

     -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  version="1.0">
  
  <xsl:output method="text" encoding="UTF-8" /> 

  
  <xsl:template match="/">
       <xsl:apply-templates select='//Record'/>
  </xsl:template>
  
  <xsl:template match='Record'>
	     <xsl:value-of select="."/>
  </xsl:template>

</xsl:stylesheet>

