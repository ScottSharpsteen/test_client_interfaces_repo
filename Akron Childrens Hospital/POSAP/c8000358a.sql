-----------------------------------------------------------------------------------------
-- Patch ID: c8000358a.sql
-- Description: Set FSI parms
-- CLARIFY reference: per GTT
-- DEV Review: cjv2
-- Rerun Flag: N
-- Target DB: 800
-- Syntax: ASA T-SQL
-----------------------------------------------------------------------------------------
SET NOCOUNT ON
IF EXISTS (SELECT 1 FROM cbord.cbo0023g_dbpatchresult WHERE connectionid = @@spid)
BEGIN
	DELETE FROM cbord.cbo0023g_dbpatchresult WHERE connectionid = @@spid
	COMMIT
END
BEGIN TRANSACTION
IF @@error <> 0
	INSERT INTO cbord.cbo0023g_dbpatchresult(resultcode, resulttext, connectionid)
	VALUES(-1, 'Unable to open transaction for patch' , @@spid)

-- End of Patch Setup
--
-- PATCH CODE BEGINS AFTER THIS LINE
--
--UPDATE cbord.cbo0001p_parms SET Parmvalue = 'c:\cbordwin\Interfaces\' WHERE application = 'fsi' AND Parmname = 'AcctDirectory'
--UPDATE cbord.cbo0001p_parms SET Parmvalue = 'c:\cbordwin\work\intfLogs\' WHERE application = 'fsi' AND Parmname = 'AcctLogDirectory'
UPDATE cbord.cbo0001p_parms SET Parmvalue = 'D:\cbordwin\work\Intf\Inbound\' WHERE application = 'fsi' AND Parmname = 'FilePathForCIF'
UPDATE cbord.cbo0001p_parms SET Parmvalue = 'N' WHERE application = 'fsi' AND Parmname = 'UseMessageQueue'
-- ADD THE FOLLOWING CODE AFTER EACH SQL STATEMENT TO DETECT ERRORS
GO
INSERT INTO cbord.cbo0023g_dbpatchresult(resultcode, resulttext, connectionid)
SELECT 100, 'SQLSTATE ' + SQLSTATE + ', ' + ERRORMSG(SQLCODE), @@spid
WHERE @@error <> 0

GO
-- END CODE TO BE ADDED AFTER EACH SQL STATEMENT TO DETECT ERRORS



--
-- PATCH CODE ENDS ABOVE THIS LINE
--
GO
IF @@error<>0
	INSERT INTO cbord.cbo0023g_dbpatchresult(resultcode, resulttext, connectionid)
	VALUES(-1, 'Problem with patch wrapper', @@spid)

-- Start of Declarations
DECLARE @s_patchid VARCHAR(8)         -- unique id of this patch
DECLARE @s_targetdb VARCHAR(8)        -- db version this patch normally targets
DECLARE @s_prereqdb VARCHAR(8)        -- lowest db ver this patch can target
DECLARE @s_prereqpatch VARCHAR(8)     -- comma-delim list of prereq patch IDs
DECLARE @s_appid VARCHAR(3)           -- app id this patch affects (doc only)
DECLARE @s_prereqappver VARCHAR(8)    -- app must be at this version to use this patch
DECLARE @s_patchtype VARCHAR(1)       -- unused
DECLARE @s_canrerun VARCHAR (1)         -- Y/N, can patch be rerun IF nessary?
DECLARE @s_description VARCHAR(254)   -- description of patch
DECLARE @dt_now DATETIME
DECLARE @i_result SMALLINT            -- result code of patch
DECLARE @s_resulttext VARCHAR(254)    -- result text
DECLARE @s_dbver VARCHAR(8)           -- current db version
DECLARE @l_patchhist_intid  INTEGER   -- intid of history row of this patchid
DECLARE @i_patchhist_result SMALLINT  -- result of prev. application of this patch
DECLARE @s_step VARCHAR(40)           -- process step (for debugging)
DECLARE @error_number INTEGER
DECLARE @s_error_sqlstate VARCHAR(250)
--
--
SET @error_number = @@error
SET @s_step = '1'
SET @s_description = 'Set FSI parms'
SET @s_patchid = 'c8000358'
SET @s_targetdb ='9999'
SET @s_prereqdb = ''             -- default: no prereq db
SET @s_prereqpatch = ''
SET @s_appid = 'CBO'
SET @s_prereqappver = ''        -- default: no prereq app ver
SET @s_patchtype = 'r'        -- add a 'r' to allow cpatches to rerun
SET @s_canrerun = 'Y'     -- default: can rerun
-- End of Declarations
IF EXISTS (SELECT 1 FROM cbord.cbo0023g_dbpatchresult WHERE resultcode = 100 AND connectionid = @@spid)
	SELECT FIRST @error_number = resultcode,
		@s_error_sqlstate = resulttext
	FROM cbord.cbo0023g_dbpatchresult


-- Start of Patch Setup
EXEC cbord.cbo_dbpatchsetup
  @s_patchid,
  @s_targetdb,
  @s_prereqdb,
  @s_prereqpatch,
  @s_appid,
  @s_prereqappver,
  @s_patchtype,
  @s_canrerun,
  @s_description,
  '',
  @i_result,
  @s_resulttext,
  @s_step

IF (@i_result = 0 OR @i_result = 10) AND @error_number = 0
    BEGIN
        IF @s_resulttext = ''
        SET @s_resulttext = 'Patch applied.'
        SET @dt_now = getdate()
        SET @s_step = '900'
        EXEC cbord.cbo_putpatchresult 'Y', 'Y', @s_patchid, @dt_now, @i_result, @s_resulttext
        COMMIT -- commit the patch results before the next batch begins
    END
ELSE IF (@i_result <> 0 OR @i_result <> 100 OR @i_result <> 10) AND @error_number = 0
    BEGIN
        ROLLBACK TRANSACTION -- rollback the patch results if it has been previously applied
        SET @dt_now = getdate()
        SET @s_step = '900'
        EXEC cbord.cbo_putpatchresult 'Y', 'Y', @s_patchid, @dt_now, @i_result, @s_resulttext
    END
ELSE IF (@error_number <> 0 OR @error_number = 50001) AND @@trancount >= 1
    BEGIN
        ROLLBACK TRANSACTION -- rollback the patch results if there was an error
        SET @dt_now = getdate()
        SET @i_result = 100
        SET @s_resulttext = 'Exception ' + @s_error_sqlstate + '. Patch Failed'
        EXEC cbord.cbo_putpatchresult 'Y', 'Y', @s_patchid, @dt_now, @i_result, @s_resulttext
    END
ELSE IF (@error_number <> 0 OR @error_number = 50001) AND @@trancount = 0
    BEGIN
		-- No transaction to rollback
        SET @dt_now = getdate()
        SET @i_result = 100
        SET @s_resulttext = 'Exception ' + @s_error_sqlstate + '. Patch Failed'
        EXEC cbord.cbo_putpatchresult 'Y', 'Y', @s_patchid, @dt_now, @i_result, @s_resulttext
    END

COMMIT

SELECT * FROM cbo0022p_dbpatchhist ORDER BY patchdtm DESC
