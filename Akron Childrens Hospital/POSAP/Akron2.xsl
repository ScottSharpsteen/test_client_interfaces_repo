<xsl:stylesheet   xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />
  <xsl:key name="group-items" match="Transaction" use="*/HeaderKey"/>

  
 <xsl:template match="/">
 <CCIFAPGL> 
      <xsl:apply-templates select="//Operation"/>   
      <xsl:apply-templates select="//TransmissionId"/>   
      <xsl:apply-templates select="//Transaction[generate-id(.)=generate-id(key('group-items',*/HeaderKey))]">   
	  <xsl:sort select="*/VendorId"/>
	  <xsl:sort select="*/TransactionDate"/>
	  <xsl:sort select="*/DeliveryDate"/>
	  <xsl:sort select="*/PRNumber"/>
      </xsl:apply-templates>       
  </CCIFAPGL>   
 </xsl:template>

<xsl:template match='Transaction'>
   <Transaction>
       <xsl:apply-templates select="Header"/>
       <ItemList>
        <xsl:apply-templates select="key('group-items', */HeaderKey)/*/Item"/> 
   </ItemList>   
   </Transaction>   
 </xsl:template>

<xsl:template match='Item'>
   <Item>
      <xsl:apply-templates select="*"/>
   </Item>   
 </xsl:template>

<xsl:template match='Total'>
   <Total>
      <xsl:value-of select="."/>
   </Total>   
 </xsl:template>

<xsl:template match='TaxTotal[1]'>
   <TaxTotal>
      <xsl:value-of select="."/>
   </TaxTotal>   
 </xsl:template>

<xsl:template match='ItemKey'><ItemKey><xsl:value-of select="."/></ItemKey></xsl:template>
<xsl:template match='Header'><Header><xsl:apply-templates select="*"/></Header></xsl:template>
<xsl:template match='AccountNumber'><AccountNumber><xsl:value-of select="."/></AccountNumber></xsl:template>
<xsl:template match='VendorId'><VendorId><xsl:value-of select="."/></VendorId></xsl:template>
<xsl:template match='InviceNumber'><InviceNumber><xsl:value-of select="."/></InviceNumber></xsl:template>
<xsl:template match='Today'><Today><xsl:value-of select="."/></Today></xsl:template>
<xsl:template match='DeliveryDate'><DeliveryDate><xsl:value-of select="."/></DeliveryDate></xsl:template>
<xsl:template match='TransactionDate'><TransactionDate><xsl:value-of select="."/></TransactionDate></xsl:template>
<xsl:template match='PRNumber'><PRNumber><xsl:value-of select="."/></PRNumber></xsl:template>
<xsl:template match='Operation'><Operation><xsl:value-of select="."/></Operation></xsl:template>
<xsl:template match='TransmissionId'><TransmissionId><xsl:value-of select="."/></TransmissionId></xsl:template>
<xsl:template match='Description'><Description><xsl:value-of select="."/></Description></xsl:template>
<xsl:template match='Operation'><Operation><xsl:value-of select="."/></Operation></xsl:template>
<xsl:template match='TransmissionId'><TransmissionId><xsl:value-of select="."/></TransmissionId></xsl:template>
<xsl:template match='UnitId'><UnitId><xsl:value-of select="."/></UnitId></xsl:template>
<xsl:template match='HeaderKey'/>



</xsl:stylesheet>
