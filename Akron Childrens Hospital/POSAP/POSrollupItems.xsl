<?xml version="1.0" encoding="iso-8859-1"?>
<!-- 	
	stylesheet for aggregating Item values in POS messages: version $Revision:   1.1  $
	Author: PDA
	Changes:
	02/13/2002	GTT Sort the Service Menus by PeriodId.
	02/13/2002	GTT Determine item price.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                version="1.0">
  
<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<xsl:key name="group-items" match="ITEM" use="concat(PLU,generate-id(../..))" />



<xsl:template match="/">
  <CTalkServiceMenuImport>
    <xsl:for-each select="*/ServiceMenu">
       <xsl:sort select="./Header/PeriodId" data-type="number" order="ascending"/>
       <xsl:apply-templates select="."/>
    </xsl:for-each>    
  </CTalkServiceMenuImport>
</xsl:template>

<xsl:template match="ServiceMenu">
  <ServiceMenu>
    <xsl:apply-templates select="Header"/>
    <xsl:variable name="serviceMenuId" select="generate-id(.)"/>

  <ItemList>
  <xsl:for-each select="ItemList/ITEM[count(. | key('group-items', concat(PLU,$serviceMenuId))[1])=1]">
      <xsl:sort select="./PLU" data-type="number" order="ascending"/>
      <xsl:variable name="SumQnt"  select="sum(key('group-items', concat(./PLU,$serviceMenuId))/QUANTITY)"/>

       <xsl:variable name="Price">
            <xsl:choose>
		<xsl:when test="./PLU='1' or string(../../Header/BIZRULES/PosType)='M8700'">
                          <xsl:value-of select="sum(key('group-items', concat(./PLU,$serviceMenuId))/PRICE) div count(key('group-items', concat(./PLU,$serviceMenuId)))"/>
                </xsl:when>
                <xsl:otherwise>
			   <xsl:value-of select="./PRICE"/>
		</xsl:otherwise>
             </xsl:choose>       
      </xsl:variable>


     <xsl:if test="cbord:roundOff(string($SumQnt),'1','0') &gt;= 1 ">
      <Item>
        <PLU><xsl:value-of select="./PLU"/></PLU>
 	    <ActualInfo>
	          <PortionCount>
	  	    <xsl:value-of select="cbord:roundOff(string($SumQnt),'1','0')"/>
	  	  </PortionCount>
	  	  <Price><xsl:value-of select="cbord:roundOff(string($Price), '1', '2')"/></Price>
	    </ActualInfo>
        </Item>
    </xsl:if>
    </xsl:for-each>	
    </ItemList>
  </ServiceMenu> 
</xsl:template>

<xsl:template match="Header">
   <Header type="Update">
	    <ServiceDate><xsl:value-of select="ServiceDate"/></ServiceDate>
	    <MealPeriod><xsl:value-of select="MealPeriod"/></MealPeriod>
	    <ServiceUnitExtId><xsl:value-of select="ServiceUnitExtId"/></ServiceUnitExtId>
	    <CustomerCount><xsl:value-of select="CustomerCount"/></CustomerCount>
    </Header>
</xsl:template>


<!--xsl:template name="sumItems">
   <xsl:param name="items"/>
   <xsl:param name="count"/>
 
   <xsl:if test="$items">
     <xsl:call-template name="sumItems">
       <xsl:with-param name="items" select="$items[position != 1]"/>
       <xsl:with-param name="count" select="$count + $items[1]/QUANTITY"/>
     </xsl:call-template> 
   </xsl:if>   
</xsl:template-->



<msxsl:script implements-prefix="cbord">
  <![CDATA[

  // round of to the nearest precision

var curr = 0;
var prev = 99999999;
var sum = 0;

function roundOff(value1, value2, precision)
{
        value1 =  ""+value1;
        value2 =  ""+value2;

        precision = parseInt(precision);
 
        var whole = "" + Math.round(value2 * (value1 * Math.pow(10, precision)));

        var decPoint = whole.length - precision;
       if(precision == 0)
                result = whole;      
       else if(decPoint < 0)
        {
            result = "0.";
            while(decPoint < 0)
            {
               result = result + "0";
               decPoint = decPoint + 1;
            } 
            result = result + whole;
        }
       else if(decPoint > 0)  
            { 
                result = whole.substring(0, decPoint);
                result += ".";
                result += whole.substring(decPoint, whole.length);
        }
        else
        {
                result = "0."+whole;
        }
        return result;
}

function sumPrices(value, key)
{
        curr = parseInt(key); 
        if (curr != prev)
        {
          p = new Number(value);
          sum = sum + p;
          prev = curr;
        }  
        return "";
}



function setSum()
{
   sum = 0;
   curr = 0;
   prev = 99999999
   return "";
   
}


function getPrices()
{
   return ""+sum;
}


  ]]>
</msxsl:script>
</xsl:stylesheet>
