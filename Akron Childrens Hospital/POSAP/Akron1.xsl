<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:cbord='http://cbord.com/xsltns'
  version="1.0">

<!-- 3/16/06  VJT removed the Production Unit Group field and replaced with Cost CenterID --> 
    <xsl:import href='cbordscript.xsl'/>
    <xsl:import href='DateConversion.xsl'/>

    <xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />
  

    <xsl:template match="/">
    <xsl:copy>
         <xsl:apply-templates select="/*"/>
    </xsl:copy>
  </xsl:template>

<xsl:template match='Transaction'>
   <xsl:copy>
        <xsl:apply-templates select='*'/>
  </xsl:copy>
</xsl:template>

<xsl:template match='Header'>
      <xsl:copy>
            <HeaderKey>
		<xsl:value-of select='cbord:StrToUpper(substring(*/UnitId[10],1,1))'/>
		<xsl:value-of select="TransactionDate"/>
		<xsl:value-of select="DeliveryDate"/>
		<xsl:value-of select="Description"/>
		<xsl:value-of select="*/TransactionNumber[1]"/>
		<xsl:value-of select="*/TransactionNumber[3]"/>
		<xsl:value-of select="*/OutgoingUnitId[5]"/>
		<xsl:value-of select="*/UnitId[1]"/>
            </HeaderKey>
            <UnitId><xsl:value-of select='*/UnitId[1]'/></UnitId>
            <VendorId>
	       <xsl:value-of select="cbord:padRight(substring(*/OutgoingUnitId[5],1,10), ' ', 10)"/>
            </VendorId>
            <InviceNumber>                 
	          <xsl:value-of select="cbord:padRight(substring(*/TransactionNumber[3],1,20), ' ', 20)"/>
            </InviceNumber>
            <Today><xsl:value-of select="cbord:today('')"/></Today>
            <DeliveryDate>
                  <xsl:call-template name="yyyy-mm-dd2mmddyyyy">
  			<xsl:with-param name="date" select="DeliveryDate"/>
                  </xsl:call-template>
	    </DeliveryDate>
            <TransactionDate>
                  <xsl:call-template name="yyyy-mm-dd2yyyymmdd">
  			<xsl:with-param name="date" select="TransactionDate"/>
                  </xsl:call-template>
            </TransactionDate>
            <!--xsl:apply-templates select='TransactionDate'/-->
            <PRNumber><xsl:value-of select="cbord:padRight(substring(*/TransactionNumber[2],1,20), ' ', 20)"/></PRNumber>
            <xsl:apply-templates select='Description'/>
  </xsl:copy>
</xsl:template>

<xsl:template match='ItemList'>
   <xsl:copy>
        <xsl:apply-templates select='*'/>
  </xsl:copy>
</xsl:template>


<xsl:template match='Item'>
  <xsl:if test="Total != 0">
    <xsl:copy>
      <ItemKey>
		<xsl:value-of select='cbord:StrToUpper(substring(../../*/*/UnitId[10],1,1))'/>
		<xsl:value-of select="../../*/TransactionDate"/>
		<xsl:value-of select="../../*/DeliveryDate"/>
		<xsl:value-of select="../../*/Description"/>
		<xsl:value-of select="../../*/*/TransactionNumber[1]"/>
		<xsl:value-of select="../../*/*/TransactionNumber[3]"/>
		<xsl:value-of select="../../*/*/OutgoingUnitId[5]"/>
		<xsl:value-of select="../../*/*/UnitId[1]"/>
		<xsl:value-of select="*/AccountNumber[1]"/>
      </ItemKey> 
      <xsl:apply-templates select='Total'/>
      <xsl:apply-templates select='TaxTotal[1]'/>
      <AccountNumber>
          <!--xsl:value-of select="*/AccountNumber[1]"/-->
	  <xsl:value-of select="concat( ../../*/*/FacilityId[2],'-' ../../*/*/UnitId[3],'-',../../*/*/UnitId[1],'-',*/AccountNumber[1])"/>
      </AccountNumber>
    </xsl:copy>
  </xsl:if>
</xsl:template>

  <xsl:template match="Total">
    <xsl:copy>
      <xsl:value-of select="cbord:multAndAddToSum(format-number(.,'0.00'),'1.0',2)"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="TaxTotal">
    <xsl:copy>
      <xsl:value-of select="cbord:multAndAddToSum(format-number(.,'0.00'),'1.0',2)"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="UnitOfMeasure"/>
  <xsl:template match="ItemTrait"/>
  <xsl:template match="TaxTotal[position() != 1]"/>
  <xsl:template match="FacilityIdList"/>
  <xsl:template match="PriceQuantityList"/>
  <xsl:template match="AccountNumberList"/>
  <xsl:template match="Date"/>
  <xsl:template match="InternalID"/>
  <xsl:template match="ProductGroupID"/>
  <xsl:template match="ItemName"/>

  <xsl:template match="*|@*|text()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
