<?xml version="1.0" encoding="iso-8859-1"?>
<!-- 	stylesheet for filtering and translating raw generic POS messages: version $Revision:   1.1
	Author: PDA
 	Changes:
	Date  : 03/28/2002	GTT Add routines to process different Micors data.
		03/28/2002	GTT xlate meal periods
		03/28/2002	GTT xlate Business date
		03/28/2002	GTT xlate ExternalServiceUnitsIds
		03/28/2002	GTT xlate PLU
		03/28/2002	GTT filter plu, mitotals and configData tables.
		04/10/2002	GTT determine price for the items
		04/15/2002	GTT filter transactions with no items
		04/15/2002	GTT filter items with 0 quantity
		09/17/2003	GTT Rearrange to make it easier to add new PosType processing.
				    This will allow an IA to write the translation for a new PosType
				    without affecting the existing PosTypes. 
		

	
$-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                version="1.0">

<xsl:include href="DateConversionPOS.xsl"/>

<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<xsl:key name="pluxlate" match="PLUTable" use="PLU"/>
<xsl:key name="unitXlate" match="PosUnit" use="concat(PosType, PosUnitId)"/>

<xsl:key name="Prices" match="M8700MITOTALS" use="concat(PLU, UNITID)"/>
<xsl:key name="myItems" match="ITEM" use="concat(UNITID, PLU)"/>


<xsl:template match="/">
<ServiceMenuImport>
  <!-- Filter all Transactions that have no items -->
  <xsl:apply-templates select="//TRANSACTION[count(ITEMLIST/ITEM[QUANTITY != 0]) != 0]"/> 
</ServiceMenuImport>
</xsl:template>


<xsl:template match="TRANSACTION">
<xsl:copy>
    <xsl:apply-templates select="HEADER"/>
    <xsl:apply-templates select="ITEMLIST"/>
</xsl:copy>
</xsl:template>


<!--
    M2700 xlate
-->
<xsl:template match="HEADER[BIZRULES/PosType='M2700']">
  <xsl:copy>
      <xsl:variable name="normalTime">
        <xsl:call-template name="normalizeTime">
          <xsl:with-param name="time" select="TRANSTIME"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="hour" select="substring($normalTime,1,2)"/>
      <xsl:variable name="min" select="substring($normalTime,4,2)"/>

	<xsl:copy-of select="TRANSNUM"/>
        <ServiceUnitExtId>
          <xsl:value-of select="key('unitXlate', concat(BIZRULES/PosType, PosUnitId))/PosLink"/>
       </ServiceUnitExtId>
       <xsl:copy-of select="COVERCNT"/>
       <BUSINESSDATE>
         <xsl:variable name="normalDate">
            <xsl:call-template name="normalizeDate">
                <xsl:with-param name="date" select="TRANSDATE"/>
                <xsl:with-param name="type" select="'8700'"/>
             </xsl:call-template>
          </xsl:variable>
         <xsl:variable name="closeTime" select="key('unitXlate', concat(BIZRULES/PosType, PosUnitId))[1]/CloseTime"/>
         <xsl:variable name="slopHour" select="substring($closeTime,1,2)"/>
         <xsl:variable name="slopMin" select="substring($closeTime,4,2)"/>
          <xsl:choose>
             <!-- HH:MM is in [00:00, slopHour:slopMin] -->
             <xsl:when test="($hour &lt; $slopHour) or ($hour = $slopHour and $min &lt;= $slopMin)">
               <xsl:value-of select="cbord:yesterday(string($normalDate))"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="$normalDate"/>
            </xsl:otherwise>
           </xsl:choose>
      </BUSINESSDATE>

      <MEALPERIOD>  
        <xsl:variable name="fPeriod" select="MEALPERIODRAWDATA/FixedPeriod"/>
        <xsl:value-of select="key('unitXlate', concat(*/PosType, PosUnitId))/*/MealPeriod[FixedPeriod=$fPeriod]/PeriodName"/>
      </MEALPERIOD>
      <BIZRULES>
     	<Time><xsl:value-of select="MEALPERIODRAWDATA/TRANSTIME"/></Time>
     	<Unit><xsl:value-of select="PosUnitId"/></Unit>
     	<MealPeriod><xsl:value-of select="./BIZRULES/MealPeriod"/></MealPeriod> 
     	<PriceRule><xsl:value-of select="./BIZRULES/PriceRule"/></PriceRule> 
     	<UpdatePrepared><xsl:value-of select="./BIZRULES/UpdatePrepared"/></UpdatePrepared> 
     	<PosType><xsl:value-of select="./BIZRULES/PosType"/></PosType> 
     </BIZRULES>
  </xsl:copy>
</xsl:template>

<!--
    M3700  xlate
-->
<xsl:template match="HEADER[BIZRULES/PosType='M3700']">
  <xsl:copy>
	<xsl:copy-of select="TRANSNUM"/>
        <ServiceUnitExtId>
          <xsl:value-of select="key('unitXlate', concat(BIZRULES/PosType, PosUnitId))/PosLink"/>
       </ServiceUnitExtId>
	<xsl:copy-of select="COVERCNT"/>
       <BUSINESSDATE>
         <xsl:call-template name="normalizeDate">
            <xsl:with-param name="date" select="MEALPERIODRAWDATA/TRANSDATE"/>
            <xsl:with-param name="type" select="BIZRULES/PosType"/>
          </xsl:call-template>
       </BUSINESSDATE>
      <MEALPERIOD>  
        <xsl:variable name="fPeriod" select="MEALPERIODRAWDATA/FixedPeriod"/>
        <xsl:value-of select="key('unitXlate', concat(*/PosType, PosUnitId))/*/MealPeriod[FixedPeriod=$fPeriod]/PeriodName"/>
      </MEALPERIOD>
       <BIZRULES>
     	<Time><xsl:value-of select="MEALPERIODRAWDATA/TRANSTIME"/></Time>
     	<Unit><xsl:value-of select="PosUnitId"/></Unit>
     	<MealPeriod><xsl:value-of select="./BIZRULES/MealPeriod"/></MealPeriod> 
     	<PriceRule><xsl:value-of select="./BIZRULES/PriceRule"/></PriceRule> 
     	<UpdatePrepared><xsl:value-of select="./BIZRULES/UpdatePrepared"/></UpdatePrepared> 
     	<PosType><xsl:value-of select="./BIZRULES/PosType"/></PosType> 
     </BIZRULES>
  </xsl:copy>
</xsl:template>

<!--
    M8700  xlate
-->
<xsl:template match="HEADER[BIZRULES/PosType='M8700']">

  <xsl:copy>
      <xsl:variable name="normalTime">
        <xsl:call-template name="normalizeTime">
          <xsl:with-param name="time" select="MEALPERIODRAWDATA/TRANSTIME"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="hour" select="substring($normalTime,1,2)"/>
      <xsl:variable name="min" select="substring($normalTime,4,2)"/>

	<xsl:copy-of select="TRANSNUM"/>
        <ServiceUnitExtId>
             <xsl:value-of select="key('unitXlate', concat(BIZRULES/PosType, PosUnitId))/PosLink"/>
        </ServiceUnitExtId>
	<xsl:copy-of select="COVERCNT"/>
       <BUSINESSDATE>
         <xsl:variable name="normalDate">
            <xsl:call-template name="normalizeDate">
                <xsl:with-param name="date" select="MEALPERIODRAWDATA/TRANSDATE"/>
                <xsl:with-param name="type" select="BIZRULES/PosType"/>
             </xsl:call-template>
          </xsl:variable>


         <xsl:variable name="closeTime" select="key('unitXlate', concat(BIZRULES/PosType, PosUnitId))[1]/CloseTime"/>
         <xsl:variable name="slopHour" select="substring($closeTime,1,2)"/>
         <xsl:variable name="slopMin" select="substring($closeTime,4,2)"/>

          <xsl:choose>
             <!-- HH:MM is in [00:00, slopHour:slopMin] -->
             <xsl:when test="($hour &lt; $slopHour) or ($hour = $slopHour and $min &lt;= $slopMin)">
               <xsl:value-of select="cbord:yesterday(string($normalDate))"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="$normalDate"/>
            </xsl:otherwise>
           </xsl:choose>
      </BUSINESSDATE>
      <MEALPERIOD>  
        <xsl:variable name="fPeriod" select="MEALPERIODRAWDATA/FixedPeriod"/>
        <xsl:value-of select="key('unitXlate', concat(*/PosType, PosUnitId))/*/MealPeriod[FixedPeriod=$fPeriod]/PeriodName"/>
      </MEALPERIOD>

     <BIZRULES>
     	<Time><xsl:value-of select="MEALPERIODRAWDATA/TRANSTIME"/></Time>
     	<Unit><xsl:value-of select="PosUnitId"/></Unit>
     	<MealPeriod><xsl:value-of select="./BIZRULES/MealPeriod"/></MealPeriod> 
     	<PriceRule><xsl:value-of select="./BIZRULES/PriceRule"/></PriceRule> 
     	<UpdatePrepared><xsl:value-of select="./BIZRULES/UpdatePrepared"/></UpdatePrepared> 
     	<PosType><xsl:value-of select="./BIZRULES/PosType"/></PosType> 
     </BIZRULES>
  </xsl:copy>
</xsl:template>

<!--
    InfoGenesis  xlate
-->
<xsl:template match="HEADER[BIZRULES/PosType='InfoGenesis']">
  <xsl:copy>
	<xsl:copy-of select="TRANSNUM"/>
        <ServiceUnitExtId>
            <xsl:value-of select="key('unitXlate', concat(BIZRULES/PosType, PosUnitId))/PosLink"/>
       </ServiceUnitExtId>
	<xsl:copy-of select="COVERCNT"/>
       <BUSINESSDATE>
           <xsl:value-of select="TRANSDATE"/>
      </BUSINESSDATE>
       <MEALPERIOD>  
            <xsl:value-of select="MEALPERIODRAWDATA/MEALPERIOD"/>
	</MEALPERIOD>
     <BIZRULES>
     	<Time><xsl:value-of select="MEALPERIODRAWDATA/TRANSTIME"/></Time>
     	<Unit><xsl:value-of select="PosUnitId"/></Unit>
     	<MealPeriod><xsl:value-of select="./BIZRULES/MealPeriod"/></MealPeriod> 
     	<PriceRule><xsl:value-of select="./BIZRULES/PriceRule"/></PriceRule> 
     	<UpdatePrepared><xsl:value-of select="./BIZRULES/UpdatePrepared"/></UpdatePrepared> 
     	<PosType><xsl:value-of select="./BIZRULES/PosType"/></PosType> 
     </BIZRULES>
  </xsl:copy>
</xsl:template>

<!--
    Revelation  xlate
-->
<xsl:template match="HEADER[BIZRULES/PosType='Revelation']">
  <xsl:copy>
	<xsl:copy-of select="TRANSNUM"/>
        <ServiceUnitExtId>
          <xsl:value-of select="key('unitXlate', concat(BIZRULES/PosType, PosUnitId))/PosLink"/>
       </ServiceUnitExtId>
	<xsl:copy-of select="COVERCNT"/>
       <BUSINESSDATE>
           <xsl:value-of select="TRANSDATE"/>       
      </BUSINESSDATE>
       <MEALPERIOD>  
            <xsl:value-of select="MEALPERIODRAWDATA/MEALPERIOD"/>
	</MEALPERIOD>
     <BIZRULES>
     	<Time><xsl:value-of select="MEALPERIODRAWDATA/TRANSTIME"/></Time>
     	<Unit><xsl:value-of select="PosUnitId"/></Unit>
     	<MealPeriod><xsl:value-of select="./BIZRULES/MealPeriod"/></MealPeriod> 
     	<PriceRule><xsl:value-of select="./BIZRULES/PriceRule"/></PriceRule> 
     	<UpdatePrepared><xsl:value-of select="./BIZRULES/UpdatePrepared"/></UpdatePrepared> 
     	<PosType><xsl:value-of select="./BIZRULES/PosType"/></PosType> 
     </BIZRULES>
  </xsl:copy>
</xsl:template>

<!-- This is generic, will process all items regardless of the PosType
     Will filter all items with a quantity of 0
 -->
<xsl:template match="ITEMLIST">
  <xsl:copy>
	<xsl:apply-templates select="ITEM[QUANTITY != 0]"/>
  </xsl:copy>
</xsl:template>


<xsl:template match="ITEM">
 <ITEM>
  <xsl:choose>
   <xsl:when test="count(key('pluxlate',./PLU)/PLU) &gt; 0">
    <PLU>
         <xsl:value-of select="key('pluxlate',./PLU)[1]/NEWPLU"/>
    </PLU>
    <PRICE>
         <xsl:value-of select="./PRICE"/>
    </PRICE>
   </xsl:when>
   <xsl:otherwise>
    <PLU> 
         <xsl:value-of select="./PLU"/>
    </PLU>
    <PRICE>
         <xsl:value-of select="./PRICE"/>
    </PRICE>
   </xsl:otherwise> 
  </xsl:choose>
  <ITEMNAME><xsl:value-of select="./ITEMNAME"/></ITEMNAME>
  <QUANTITY><xsl:value-of select="./QUANTITY"/></QUANTITY>
 </ITEM>
</xsl:template>


<!-- Get rid of the Plu Translation Table -->
<xsl:template match="PLUXlateTable"/>
<!-- Get rid of MITOTALS -->
<xsl:template match="M8700MITOTALS"/>
<!-- Get rid of the UnitId Translation Table -->
<xsl:template match="PosUnitList"/>

<!-- Search for the meal period -->
<xsl:template name="SearchFixedPeriods">
  <xsl:param name="meal"/>
  <xsl:param name="list"/>
  <!--xsl:param name="periods"/-->
  <xsl:if test="$list">
    <xsl:choose>
      <xsl:when test="$list">
          <xsl:choose>
             <xsl:when test="$list[1]/FixedPeriod=$meal">
                  <xsl:value-of select="$list[1]/PeriodName"/>
             </xsl:when>
             <xsl:otherwise>
                <xsl:call-template name="SearchFixedPeriods">
          	  <xsl:with-param name="meal" select="$meal"/>
	  	  <xsl:with-param name="list" select="$list[position() != 1]"/>
          	  <!--xsl:with-param name="periods" select="$periods[position() != 1]"/--> 
                </xsl:call-template>
             </xsl:otherwise> 
          </xsl:choose>
     </xsl:when>
     <!--xsl:otherwise>
          <xsl:call-template name="SearchFixedPeriods">
          	<xsl:with-param name="meal" select="$meal"/>
	  	<xsl:with-param name="list" select="$list[position() != 1]"/>
          	<xsl:with-param name="periods" select="$list[2]/FixedPeriod"/> 
         </xsl:call-template>
     </xsl:otherwise-->
    </xsl:choose>
  </xsl:if>
</xsl:template> 

<xsl:template name="SearchTimeIntervals">
  <xsl:param name="cHour"/>
  <xsl:param name="cMin"/>
  <xsl:param name="list"/>
  <xsl:param name="tList"/>

  <xsl:choose>
  <xsl:when test="$list">
  <xsl:variable name="sHour" select="number(substring($list[position()]/Start,1,2))"/>
  <xsl:variable name="sMin" select="number(substring($list[position()]/Start,4,2))"/>
  <xsl:variable name="eHour" select="number(substring($list[position()]/End,1,2))"/>
  <xsl:variable name="eMin" select="number(substring($list[position()]/End,4,2))"/>

  <xsl:choose>
        <!-- Several meal periods for the day -->
       <xsl:when test="($cHour = $sHour and $cMin &gt;= $sMin) or ($cHour &gt; $sHour and $cHour &lt; $eHour) or ($cHour = $eHour and $cMin &lt;= $eMin) or ($cHour &gt; $sHour and $eHour &lt;= $sHour) or ($cHour &lt; $sHour and $cHour &lt;= $eHour and $eHour &lt; $sHour)">
         <xsl:value-of select="$list[position()]/PeriodName"/>
       </xsl:when>
       <xsl:otherwise>
         <xsl:call-template name="SearchTimeIntervals">
            <xsl:with-param name="cHour" select="$cHour"/>
            <xsl:with-param name="cMin" select="$cMin"/>
            <xsl:with-param name="list" select="$list[position() != 1]"/>
            <xsl:with-param name="tList" select="$tList"/>
         </xsl:call-template>
       </xsl:otherwise>  
    </xsl:choose>
  </xsl:when> 
  <!-- this will occur when meal period spans the midninght -->
  <xsl:otherwise>
    <!--xsl:value-of select="$tList[last()]/PeriodName"/-->
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<!-- normalize any POS time format -->
<xsl:template name="normalizeTime">
  <xsl:param name="time"/>

  <xsl:choose>
    <!-- change YY:XXPM to YY+12:XX (except for 12:XXPM)-->
    <xsl:when test="substring($time,6,2)='PM' and substring($time,1,2)!='12'">
      <xsl:value-of select=" concat( string( 12+number(substring($time,1,2))), substring($time,3,3))"/>
    </xsl:when>
    <!-- change 12:XXAM to 00:XX -->
    <xsl:when test="substring($time,6,2)='AM' and substring($time,1,2)='12'">
      <xsl:value-of select=" concat( '00', substring($time,3,3))"/>
    </xsl:when>
    <!-- leave all others the same -->
    <xsl:otherwise>
      <xsl:value-of select="substring($time,1,5)"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- normalize any POS date format into CCYY/MM/DD -->
<xsl:template name="normalizeDate">
  <xsl:param name="date"/>
  <xsl:param name="type"/>
  <xsl:choose>
    <xsl:when test="$type='M2700'">
       <xsl:variable name="month">
         <xsl:call-template name="DCName-to-Month">
          <xsl:with-param name="name" select="substring($date,1,3)"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="day" select="substring($date,4,2)"/>
      <xsl:variable name="year" select="concat( '20', substring($date,7,2) )"/>
      <xsl:value-of select="concat($year,'/',$month,'/',$day)"/>
    </xsl:when>
    <xsl:when test="$type='M3700'">
      <xsl:value-of select="concat(substring($date,1,4), '/', substring($date,6,2), '/', substring($date,9,2))"/>
    </xsl:when>
    <xsl:when test="$type='M8700'">
        <xsl:variable name="year"  select="substring($date,7,2)"/>
        <xsl:variable name="month" select="substring($date,1,2)"/>
        <xsl:variable name="day"   select="substring($date,4,2)"/>
        <xsl:value-of select="concat('20',$year,'/',$month,'/',$day)"/>
     </xsl:when>
     <xsl:when test="$type='InfoGenesis' or $type='Revelation'">
      <xsl:value-of select="concat('20',substring($date,1,2), '/', substring($date,3,2), '/', substring($date,5,2))"/>       
    </xsl:when>
  </xsl:choose>  
</xsl:template>

<!-- Handle any node not yet matched, strip out any comments or processing instructions -->
<xsl:template match="*|@*|text()">
  <xsl:copy><xsl:apply-templates select="*|@*|text()"/></xsl:copy>
</xsl:template>

<msxsl:script implements-prefix="cbord">
  <![CDATA[

  // round of to the nearest precision

// subtract one from a date: input is in format CCYY/MM/DD
  function yesterday(date)
  {
    var MinMilli = 1000 * 60;
    var HrMilli = MinMilli * 60;
    var DyMilli = HrMilli * 24;

    var thatdate = new Date(20010101); //for No daylight savings
    var thisdate=new Date();

     if (thisdate.getTimezoneOffset()==thatdate.getTimezoneOffset());  
     else
        DyMilli = HrMilli * 23;


    var year = new Number(date.substr(0,4));
    var mon = new Number(date.substr(5,2));
    // date objects use 0-11 as months
    mon = mon -1;
    var day = new Number(date.substr(8,2));

    // intialize date object to this date
    var date = new Date(year,mon,day);

    // translate to milliseconds since 1/1/1970
    var dateInMilliSeconds = date.getTime();
    // go back one day
    dateInMilliSeconds = dateInMilliSeconds-DyMilli;
    date.setTime(dateInMilliSeconds);

    // translate back to a string
    year = date.getFullYear();
    mon = date.getMonth() + 1;
    day = date.getDate();
    var monStr;
    var dayStr;
    if (mon < 10)
    {
      monStr = "0" + mon;
    }
    else
    {
      monStr = "" + mon;
    }
    if (day < 10)
    {
      dayStr = "0" + day;
    }
    else
    {
      dayStr = "" + day;
    }

    return year + "/" + monStr + "/" + dayStr;
  }




  ]]>
</msxsl:script>

</xsl:stylesheet>

