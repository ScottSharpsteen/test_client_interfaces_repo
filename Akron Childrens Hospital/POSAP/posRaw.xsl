<?xml version="1.0" encoding="iso-8859-1"?>
<!-- 	stylesheet for transforming Raw 3700 line items into header/details : version $Revision:   1.1 
	Author: GTT		
	Date	08/23/01
	Changes:
		06/15/04	Cover count = TRANSCNT for all same items in a period.
		06/15/04	Need to handle items in a multi key states.

 $-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<xsl:key name="group-trans" match="Transaction" use="concat(FixedPeriod,BUSINESSDATE,UNITID,STOREID)" />

<xsl:template match="/">
<CTalkRawPOSTransactions>
  <xsl:for-each select="//Transaction[count(. | key('group-trans', concat(FixedPeriod,BUSINESSDATE,UNITID,STOREID))[1])=1]">
    <!--xsl:if test="sum(key('group-trans', concat(FixedPeriod,BUSINESSDATE,UNITID,STOREID))/QUANTITY) &gt; 0"-->
    <TRANSACTION>
        <xsl:apply-templates select="." mode="header"/>
        <xsl:apply-templates select="." mode="itemlist"/>
    </TRANSACTION>
   <!--/xsl:if-->
  </xsl:for-each>
</CTalkRawPOSTransactions>
</xsl:template>

<xsl:template match="Transaction" mode="header">
<HEADER>
  <!-- generate unique id for this transaction so combination logic works later on -->
  <TRANSNUM>
    <xsl:value-of select="concat(FixedPeriod,BUSINESSDATE,UNITID,STOREID)"/>
  </TRANSNUM>
  <UNITID>
     <xsl:value-of select="UNITID"/>
  </UNITID>
  <PosUnitId>
     <xsl:value-of select="UNITID"/>
  </PosUnitId>
  <COVERCNT>
      <!--xsl:variable name="plu" select="MENUITEMNUM"/-->
      <!--xsl:value-of select="sum(key('group-trans', concat(FixedPeriod,BUSINESSDATE,UNITID,STOREID))[MENUITEMNUM=$plu]/TRANSCNT)"/-->
      <xsl:value-of select="COVERCNT"/>
  </COVERCNT>
  <MEALPERIODRAWDATA>
    <FixedPeriod><xsl:value-of select="FixedPeriod"/></FixedPeriod>
    <TRANSDATE><xsl:value-of select="BUSINESSDATE"/></TRANSDATE>
  </MEALPERIODRAWDATA>
  <BIZRULES>
	<MealPeriod>FixedPeriod</MealPeriod>
	<PriceRule>ItemPrice</PriceRule>
	<UpdatePrepared>No</UpdatePrepared>
        <PosType>M3700</PosType>
  </BIZRULES>
</HEADER>
</xsl:template>

<xsl:template match="Transaction" mode="itemlist">
  <ITEMLIST>
    <xsl:apply-templates select="key('group-trans', concat(FixedPeriod,BUSINESSDATE,UNITID,STOREID))" mode="Item"/>
  </ITEMLIST>
</xsl:template>


<xsl:template match="Transaction" mode="Item">
  <ITEM>

  <PLU><xsl:value-of select="MENUITEMNUM"/></PLU> 
  <QUANTITY><xsl:value-of select="QUANTITY"/></QUANTITY>
  <xsl:variable name="posprc">
	 <xsl:choose>
		<xsl:when test="QUANTITY != 0">
                     <xsl:value-of select="PRICE div QUANTITY"/>
		</xsl:when>
		<xsl:otherwise>
                     	    <xsl:value-of select="PRICE div 1"/>
		</xsl:otherwise>
	  </xsl:choose>
   </xsl:variable>
   <PRICE><xsl:value-of select="$posprc"/></PRICE>
  </ITEM>
</xsl:template>





</xsl:stylesheet>
