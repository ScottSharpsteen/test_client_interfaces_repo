<?xml version="1.0" encoding="iso-8859-1"?>

<!-- Perform the union of all elements from a merge: $Revision:   1.0  $ -->
<!-- Note: the root tag in the output is copied from the first output/contents tag -->
<!-- no check is made to see if all output/contents/* tags are the same -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<xsl:template match="/">
  <!-- grab root tag name from root of first contents node -->
  <xsl:element name="{name(/merge/output[1]/contents/*)}">
    <xsl:for-each select="*/output/contents/*/*">
      <xsl:copy-of select="."/>
    </xsl:for-each>
  </xsl:element>

</xsl:template>


</xsl:stylesheet>
