<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" /> 


<xsl:template match="/">
  <PosUnitDefinition>
     <xsl:apply-templates select="//Operation"/>
     <xsl:apply-templates select="//TransmissionId"/>
     <xsl:apply-templates select="//Header"/>
     <xsl:apply-templates select="//PosUnitList"/>
  </PosUnitDefinition>
</xsl:template>

<xsl:template match='Operation'> 
   <xsl:copy>
	<xsl:value-of select ='.' />    
    </xsl:copy>
</xsl:template>

<xsl:template match='TransmissionId'>
   <xsl:copy>
	<xsl:value-of select ='.' />    
    </xsl:copy>
</xsl:template>
 

<xsl:template match='Header'> 
   <xsl:copy>
        <xsl:apply-templates select="Facility"/>
	<xsl:apply-templates select="PosPath"/>
    </xsl:copy>
</xsl:template>

<xsl:template match='Facility'>
   <xsl:copy>
	<xsl:value-of select ='.' />    
    </xsl:copy>
</xsl:template>

<xsl:template match='PosPath'>
   <xsl:copy>
	<xsl:value-of select ='.' />    
    </xsl:copy>
</xsl:template>

<xsl:template match='PosUnitList'>
   <xsl:copy>
        <xsl:apply-templates select="*"/>
    </xsl:copy>
</xsl:template>


<xsl:template match='PosUnitId'> 
   <xsl:copy>
      <xsl:choose>
         <xsl:when test="../PosType = 'M2700'">
	    <xsl:value-of select = 'format-number(., "00")' /> 
         </xsl:when>
         <xsl:when test="../PosType = 'M8700'">
             <xsl:choose>
                 <xsl:when test="contains(., '-')">
	            <xsl:value-of select ="format-number(substring-after(.,'-'), '0')"/> 
                  </xsl:when>
                 <xsl:when test="contains(., '_')">
	            <xsl:value-of select ="format-number(substring-after(.,'_'), '0')"/> 
                  </xsl:when>
                 <xsl:when test="contains(., '.')">
	            <xsl:value-of select ="format-number(substring-after(.,'.'), '0')"/> 
                  </xsl:when>
		  <xsl:otherwise>
	            <xsl:value-of select ="."/> 			
	   	  </xsl:otherwise>
             </xsl:choose>
         </xsl:when>
         <xsl:otherwise>
	    <xsl:value-of select = '.' /> 
         </xsl:otherwise>
      </xsl:choose>
    </xsl:copy>
  </xsl:template>

<xsl:template match="MealPeriod[../../PosType='M2700' or ../../PosType='M8700']">
    <xsl:copy>
          <xsl:apply-templates select="Start"/>
	  <xsl:apply-templates select="End"/>
          <xsl:apply-templates select="PeriodName"/>

          <xsl:variable name="sMin">
                <xsl:variable name="smin" select="number(substring(Start,4,2))"/>
		<xsl:choose>
                    <xsl:when test="$smin = 0">
		         <xsl:value-of select="0"/>
		    </xsl:when>
                    <xsl:when test="$smin &gt;= 0 and $smin &lt;= 15">
		         <xsl:value-of select="15"/>
		    </xsl:when>
                    <xsl:when test="$smin &gt; 15 and $smin &lt;= 30">
		         <xsl:value-of select="30"/>
		    </xsl:when>
                    <xsl:when test="$smin &gt; 30 and $smin &lt;= 45">
		         <xsl:value-of select="45"/>
		    </xsl:when>
                    <xsl:when test="$smin &gt; 45 and $smin &lt;= 60">
		         <xsl:value-of select="59"/>
		    </xsl:when>
	        </xsl:choose> 
	  </xsl:variable>
          <xsl:variable name="eMin">
                <xsl:variable name="emin" select="number(substring(End,4,2))"/>
		<xsl:choose>
                    <xsl:when test="$emin = 0">
		         <xsl:value-of select="0"/>
		    </xsl:when>
                    <xsl:when test="$emin &gt;= 1 and $emin &lt;= 15">
		         <xsl:value-of select="15"/>
		    </xsl:when>
                    <xsl:when test="$emin &gt; 15 and $emin &lt;= 30">
		         <xsl:value-of select="30"/>
		    </xsl:when>
                    <xsl:when test="$emin &gt; 30 and $emin &lt;= 45">
		         <xsl:value-of select="45"/>
		    </xsl:when>
                    <xsl:when test="$emin &gt; 45 and $emin &lt;= 60">
		         <xsl:value-of select="59"/>
		    </xsl:when>
	        </xsl:choose> 
	  </xsl:variable>          

          <xsl:variable name="sHour">
		<xsl:choose>
		   <xsl:when test="number(substring(Start,1,2))=0 and number(substring(Start,4,2))=0">
			 <xsl:value-of select="24"/>
                   </xsl:when>
                   <xsl:otherwise>
			<xsl:value-of select="number(substring(Start,1,2))"/>
		   </xsl:otherwise>
		</xsl:choose>
	  </xsl:variable>      


          <xsl:variable name="eHour">
		<xsl:choose>
		   <xsl:when test="number(substring(End,1,2))=0 and number(substring(End,4,2))=0">
			 <xsl:value-of select="24"/>
                   </xsl:when>
                   <xsl:otherwise>
			<xsl:value-of select="number(substring(End,1,2))"/>
		   </xsl:otherwise>
		</xsl:choose>
	  </xsl:variable>          

          <xsl:choose>
              <xsl:when test="$eHour &lt;= $sHour">
                    <xsl:call-template name="eNumTimePeriod">
	                <xsl:with-param name="start" select="$sHour * 60 + $sMin"/>
	                <xsl:with-param name="end" select="24 * 60"/>
	            </xsl:call-template> 
                    <xsl:call-template name="eNumTimePeriod">
	                <xsl:with-param name="start" select="15"/>
	                <xsl:with-param name="end" select="$eHour * 60 + $eMin"/>
	            </xsl:call-template> 
              </xsl:when>
	      <xsl:otherwise>
                    <xsl:call-template name="eNumTimePeriod">
	                <xsl:with-param name="start" select="$sHour * 60 + $sMin"/>
	                <xsl:with-param name="end" select="$eHour * 60 + $eMin"/>
	            </xsl:call-template> 	
	      </xsl:otherwise>
	  </xsl:choose>
    </xsl:copy>
</xsl:template>


<xsl:template match="MealPeriod[../../PosType='M3700']">
    <xsl:copy>
       <xsl:apply-templates select="*"/>
    </xsl:copy>
</xsl:template>

<xsl:template name="eNumTimePeriod">
    <xsl:param name="start"/>
    <xsl:param name="end"/>

    <xsl:if test="$start &lt;= $end">
	<FixedPeriod><xsl:value-of select="round($start div 15) "/></FixedPeriod>

        <xsl:call-template name="eNumTimePeriod">
	     <xsl:with-param name="start" select="$start + 15"/>
	     <xsl:with-param name="end" select="$end"/>
	</xsl:call-template> 		
    </xsl:if> 
</xsl:template> 


  <xsl:template match="*|@*|text()">
    <xsl:copy>
      <xsl:apply-templates select='*|@*|text()'/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>