<?xml version="1.0" encoding="UTF-8"?>
<!-- 	stylesheet for creating Client AP Interface messages: version $Revision:   1.5  $
	Changes:
	10/25/04   Modified for Target file layout

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
		        version="1.0">

<xsl:output method="text" encoding="UTF-8" /> 
<xsl:include href="cbordscript.xsl"/> 
<xsl:variable name="EndOfRec"><xsl:text>&#x0d;&#x0a;</xsl:text></xsl:variable>

<xsl:template match="/">
   <xsl:apply-templates select="//Transaction[1]" mode="base"/>
</xsl:template>

<xsl:template match="Transaction" mode="base">
   <xsl:variable name="operation" select="../Operation"/> 
   <xsl:if test="generate-id((//Transaction[../Operation=$operation])[1]) = generate-id(.)">
       <xsl:apply-templates select="//Transaction[../Operation=$operation][position() = 1]" mode="header"/> 
       <xsl:apply-templates select="//Transaction[../Operation=$operation]" mode="detail"/>
   </xsl:if>
</xsl:template>

<xsl:template match="Transaction[../Operation='RCV']" mode="header">       
       <xsl:value-of select="cbord:padRight('', ' ',22)"/>
       <xsl:value-of select="'UNIVERSITY OF AKRON AUDIT REPORT'"/>
       <xsl:value-of select="cbord:padRight('', ' ',11)"/>
       <xsl:value-of select="'RUN DATE: '"/>
       <xsl:value-of select="cbord:today('/')" />
       <xsl:value-of select="$EndOfRec"/>
       <xsl:value-of select="cbord:padRight('', ' ',25)"/>
       <xsl:value-of select="'INVOICES FROM CBORD AP'"/>
       <xsl:value-of select="cbord:padRight('', ' ',3)"/>
       <xsl:value-of select="cbord:padRight('', ' ',2)"/>
       <xsl:value-of select="$EndOfRec"/>
       <xsl:value-of select="$EndOfRec"/>
       <xsl:value-of select="$EndOfRec"/>
       <xsl:value-of select="'AP INTERFACE TOTAL HEADER COUNT:    '"/>
       <xsl:value-of select="cbord:padRight('', ' ',8)"/>
       <xsl:value-of select="cbord:padLeft(string(count(//Transaction)), '0', 7)"/>
       <xsl:value-of select="cbord:padRight('', ' ',25)"/>
       <xsl:value-of select="$EndOfRec"/>
       <xsl:value-of select="'AP INTERFACE TOTAL HEADER AMOUNT:    '"/>
       <xsl:value-of select="cbord:padLeft(string(sum(//Total)), '0', 14)"/>
       <xsl:value-of select="cbord:padRight('', ' ',25)"/>
       <xsl:value-of select="$EndOfRec"/>
       <xsl:value-of select="$EndOfRec"/>
       <xsl:value-of select="$EndOfRec"/>
       <xsl:value-of select="'AP INTERFACE DETAILS:    '"/>
       <xsl:value-of select="$EndOfRec"/>
</xsl:template>

<xsl:template match="Transaction[../Operation='RCV']" mode="detail">       
       <xsl:value-of select="concat('PO: ', */PRNumber)"/>
       <xsl:value-of select="cbord:padRight('', ' ',8)"/>
       <xsl:value-of select="'DETAIL COUNT: '"/>
       <xsl:value-of select="cbord:padLeft(string(count(*/Item)), '0', 7)"/>
       <xsl:value-of select="cbord:padRight('', ' ',25)"/>
       <xsl:value-of select="'DETAIL AMOUNT: '"/>
       <xsl:value-of select="cbord:padLeft(string(sum(*/*/Total)), '0', 14)"/>
       <xsl:value-of select="cbord:padRight('', ' ',25)"/>
       <xsl:value-of select="$EndOfRec"/>
</xsl:template>

</xsl:stylesheet>

