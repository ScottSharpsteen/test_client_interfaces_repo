<?xml version="1.0"?>
<!-- ADT XSLT for transforming HL7 messages. PVCS version: $Revision:   1.0  $ -->
<!-- this is for inclusion in the main xlate script for ADT messages -->
<!-- PVCS version: $Revision:   1.0  $ -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:translator="urn:translatorObject"
                version="1.0">
                
<!-- serviceOrder template called by extractOrderInfo. -->
<xsl:template match="ORC" mode="serviceOrder">
	<xsl:variable name="ORC-ID" select="generate-id()"/>
 	<xsl:variable name="pORC" select="."/>
				 	
	<!-- Variable for Cancel Orders, contains 'Y' if current ORC was sent with no ODT segments -->
	<xsl:variable name="CurrentORCwNoODT">
		<xsl:if test="not(key('ODTList', $ORC-ID)) and $msgAction='OrderCancel'">
			<xsl:value-of select="'Y'"/>
		</xsl:if>
	</xsl:variable>

	<xsl:if test="key('ODTList', $ORC-ID)">	
	<!--xsl:if test="key('ODTList', $ORC-ID) or (contains(string($CurrentORCwNoODT),'Y') and $msgAction='OrderCancel')"-->	
		<!-- Used to determine which ORC/ODT grouping is being processed-->
		<xsl:variable name="pos" select="position()"/>
		<xsl:for-each select="key('ODTList', $ORC-ID)/ODT">
			<ServiceOrderInfo>

				<OrderID><xsl:value-of select="$pORC/ORC.2/EI.1"/></OrderID>

				<!--LSSMOD:  revamped return calls for serv i/f-->
				<serviceStartDTM>
				<!-- HL7DateTime returns a tag 'returnTime' that we'll assign 
				to a variable so only the date is passed to the update layer -->
					<xsl:call-template name="HL7DateTime">
						<xsl:with-param name="Time1" select="$pORC/ORC.7/XCM.4"/>
						<xsl:with-param name="Time2" select="$pORC/ORC.15"/>
						<xsl:with-param name="hl7Where1" select="'Service StartDTM (ORC.7/XCM.4)'"/>
						<xsl:with-param name="hl7Where2" select="'Service StartDTM (ORC.15)'"/>
						<xsl:with-param name="noSysDTM" select="'N'"/>
					</xsl:call-template>
				</serviceStartDTM>

				<serviceEndDTM>
				<!-- HL7DateTime returns a tag 'returnTime' that we'll assign 
				to a variable so only the date is passed to the update layer -->
					<xsl:call-template name="HL7DateTime">
						<xsl:with-param name="Time1" select="$pORC/ORC.7/XCM.5"/>
						<xsl:with-param name="hl7Where1" select="'Service EndDTM (ORC.7/XCM.5)'"/>
						<xsl:with-param name="noSysDTM" select="'Y'"/>
						<xsl:with-param name="displayWarningsOnAllBlank" select="'N'"/>
					</xsl:call-template>
				</serviceEndDTM>
				
				<!-- Day of Week - Not currently interfaced.  No source location in HL7  -->
				<codedDayRule><dayOfWeek/></codedDayRule>
				
				<!-- If present, grab the start and end meals.  If no meal is sent in ODT.2.2 use ODT.2.1 -->
				<codedStartMeal>
					<xsl:value-of select="ODT.2.LST/ODT.2/CE.1"/>
				</codedStartMeal>
				<codedEndMeal>
					<xsl:variable name="codedEM">
						<xsl:value-of select="ODT.2.LST/ODT.2[2]/CE.1"/>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="$codedEM = ''">
							<xsl:value-of select="ODT.2.LST/ODT.2/CE.1"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$codedEM"/>
						</xsl:otherwise>
					</xsl:choose>
				</codedEndMeal>
				
				<!-- Service/Location:  takes ODT.1 and the service's nursing station from clientRules 
				except in the case of a Tray Ticket Message or Personal Menu Note.  -->
				<serviceLocationInfo>
					<xsl:choose>
						<!-- By default the Location will not be interfaced if the message is for one of the notes -->
						<xsl:when test="(string(ODT.1) = $serviceTrayTicketMessage) 
								or (string(ODT.1) = $servicePersonalMenuNote)
								and ($preventNoteServiceLocation = 'Y')">
							<serviceLocation></serviceLocation>
							<serviceNS></serviceNS>
						</xsl:when>
						<xsl:otherwise>
							<serviceLocation><xsl:value-of select="ODT.1"/></serviceLocation>
							<serviceNS>
								<xsl:choose>
									<xsl:when test="$serviceNursingStation=''">
										<xsl:value-of select="ODT.1"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$serviceNursingStation"/>
									</xsl:otherwise>
								</xsl:choose>
							</serviceNS>
						</xsl:otherwise>
					</xsl:choose>
				</serviceLocationInfo>
				
				<!-- Value the note fields if their triggers are passed on ODT.1 -->
				<trayNote>
					<xsl:if test="string(ODT.1) = $serviceTrayTicketMessage">
						<xsl:value-of select="ODT.3"/>
					</xsl:if>
				</trayNote>
				<menuNote>
					<xsl:if test="string(ODT.1) = $servicePersonalMenuNote">
						<xsl:value-of select="ODT.3"/>
					</xsl:if>
				</menuNote>
				
			</ServiceOrderInfo>
		</xsl:for-each>
	</xsl:if>

	<!-- for service cancelation -->
	<xsl:if test="(contains(string($CurrentORCwNoODT),'Y') and $msgAction='OrderCancel')">	
		<ServiceOrderInfo>
			<OrderID><xsl:value-of select="$pORC/ORC.2/EI.1"/></OrderID>
		</ServiceOrderInfo>
	</xsl:if>
</xsl:template>

<!-- serviceOrder template called by extractOrderInfo. -->
<!-- Values are taken from OBX by default, but can come from anywhere. -->
<xsl:template name="serviceOrderADT">
	<orderInfo>
		<ServiceOrderList>
			<ServiceOrderInfo>
			
				<!-- Hard-wired Order ID -->
				<OrderID>ServOrd01</OrderID>
				
				<serviceStartDTM>
					<xsl:variable name="orderStartReturnTime">
						<xsl:call-template name="HL7DateTime">
							<xsl:with-param name="Time1" select="/*/ADT_A08.LST.OBX/OBX/OBX.15/CE.1"/>
							<xsl:with-param name="hl7Where1" select="'Service StartDTM (/*/ADT_A08.LST.OBX/OBX/OBX.15/CE.1)'"/>
							<xsl:with-param name="noSysDTM" select="'N'"/>
							<xsl:with-param name="displayWarningsOnAllBlank" select="'Y'"/>
						</xsl:call-template>
					</xsl:variable>
					<returnTime>
						<xsl:value-of select="$orderStartReturnTime"/>
					</returnTime>
				</serviceStartDTM>
				
				<serviceEndDTM>
					<xsl:variable name="orderEndReturnTime">
						<xsl:call-template name="HL7DateTime">
							<xsl:with-param name="Time1" select="/*/ADT_A08.LST.OBX/OBX/OBX.15/CE.2"/>
							<xsl:with-param name="hl7Where1" select="'Service EndDTM (/*/ADT_A08.LST.OBX/OBX/OBX.15/CE.2)'"/>
							<xsl:with-param name="noSysDTM" select="'Y'"/>
							<xsl:with-param name="displayWarningsOnAllBlank" select="'N'"/>
						</xsl:call-template>
					</xsl:variable>
					<returnTime>
						<xsl:value-of select="$orderEndReturnTime"/>
					</returnTime>
				</serviceEndDTM>
				
				<!-- Day of Week - Not currently interfaced.  No source location in HL7  -->
				<codedDayRule><dayOfWeek/></codedDayRule>
				
				<!-- Start and end meals -->
				<codedStartMeal>
					<xsl:value-of select="/*/ADT_A08.LST.OBX/OBX/OBX.6/CE.1"/>
				</codedStartMeal>
				<codedEndMeal>
					<xsl:value-of select="/*/ADT_A08.LST.OBX/OBX/OBX.6/CE.2"/>
				</codedEndMeal>
				
				<!-- Service/Location:  takes ODT.1 and the service's nursing station from clientRules -->
				<serviceLocationInfo>
					<serviceLocation>
						<xsl:value-of select="/*/ADT_A08.LST.OBX/OBX/OBX.5.LST/OBX.5/CE.1"/>
					</serviceLocation>
					<serviceNS>
						<xsl:value-of select="/*/ADT_A08.LST.OBX/OBX/OBX.5.LST/OBX.5/CE.2"/>
					</serviceNS>
				</serviceLocationInfo>
				
				<!-- Value the note fields if their triggers are passed on ODT.1 -->
				<trayNote>
					<xsl:value-of select="/*/ADT_A08.LST.OBX/OBX/OBX.5.LST/OBX.5/CE.3"/>
				</trayNote>
				<menuNote>
					<xsl:value-of select="/*/ADT_A08.LST.OBX/OBX/OBX.5.LST/OBX.5/CE.4"/>
				</menuNote>
				
			</ServiceOrderInfo>
		</ServiceOrderList>
	</orderInfo>
</xsl:template>

</xsl:stylesheet>