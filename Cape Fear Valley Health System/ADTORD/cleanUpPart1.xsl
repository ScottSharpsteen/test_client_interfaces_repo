﻿<?xml version="1.0"?>
<!-- Generic stylesheet for transforming ADT messages. PVCS version: $Revision:   1.35  $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:cbord="http://cbord.com/xsltns"
                xmlns:cbordRM="http://cbord.com/xsltnsRM"
                xmlns:translator="urn:translatorObject"
                version="1.0">

<!-- include client rules for ADT interface: these will over ride any default rules -->
<xsl:import href="clientRules.xsl"/> 

<xsl:output method="xml" encoding="UTF-8" indent="no" omit-xml-declaration="yes"/>

<!-- xsl:include href="debug.xsl"/ -->

<msxsl:script implements-prefix="cbord">
    <![CDATA[

    // return the current time properly formatted
    function getNow()
    {
         var now;
         now = new Date();
         var text = "";
         text = text + now.getFullYear();
         text = text + "-";
         var month = now.getMonth();
         month += 1;
         if (month < 10)
         {
            text = text + "0";
         }
         text = text + month;
         text = text + "-";
         if (now.getDate() < 10)
         {
            text = text + "0";
         }
         text = text + now.getDate();

        // add time
         var hour = now.getHours();
         var min = now.getMinutes();
         var sec = now.getSeconds();

         var c = ":";
         text += " ";
         if (hour < 10)
            text += "0";
         text += hour + c;
         if (min < 10)
            text += "0";
         text += min + c;
         if (sec < 10)
            text += "0";
         text += sec;
  
         return text;
    }

    // translate a string to lower case
    function stringToLower(str)
    {
        if (str && typeof(str) == "string")
        {
            return str.toLowerCase();
        }
        return "";
    }

    ]]>
</msxsl:script>

<xsl:template match="/">
	<CCIFADTList>
		<xsl:choose>
			<!-- Check for msg filter -->
			<xsl:when test="//FILTER">
				<xsl:call-template name="filterMsg"/>
			</xsl:when>
			<!-- process msg -->
			<xsl:otherwise>
				<xsl:apply-templates select="/*/ADTORD"/>
			</xsl:otherwise>
		</xsl:choose>
	</CCIFADTList>
</xsl:template>

<xsl:template match="ADTORD">
	<CCIFADT>
		<OriginalMsgInfo>
			<xsl:apply-templates select="./OriginalMsgInfo/*"/>
		</OriginalMsgInfo>

		<operation>
			<xsl:call-template name="attribute">
				<xsl:with-param name="value" select="./operation/action"/>
				<xsl:with-param name="attributeName" select="'name'"/>
			</xsl:call-template>
			<xsl:apply-templates select="./operation/*"/>
		</operation>

		<personInfo>
			<!-- Attributes of personInfo -->
			<xsl:if test="./personInfo/sex">
				<xsl:call-template name="attribute">
					<xsl:with-param name="value">
						<!-- Test for Male gender attribute -->
						<xsl:if test="cbord:stringToLower(string(./personInfo/sex)) = 'm'">
							<xsl:value-of select="'Male'"/>
						</xsl:if>
						<!-- Test for Female gender attribute -->
						<xsl:if test="cbord:stringToLower(string(./personInfo/sex)) = 'f' or cbord:stringToLower(string(./personInfo/sex)) = 'p' or cbord:stringToLower(string(./personInfo/sex)) = 'l' or ./personInfo/lactating = 'Yes' or cbord:stringToLower(string(./personInfo/pregnent))='b6'">
							<xsl:value-of select="'Female'"/>
						</xsl:if>
					</xsl:with-param>
					<xsl:with-param name="attributeName" select="'sex'"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="cbord:stringToLower(string(./personInfo/pregnant))='b6' or cbord:stringToLower(string(./personInfo/sex)) = 'p'">
				<xsl:call-template name="attribute">
					<xsl:with-param name="value" select="'Yes'"/>
					<xsl:with-param name="attributeName" select="'pregnant'"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="./personInfo/lactating or cbord:stringToLower(string(./personInfo/sex)) = 'l'">
				<xsl:call-template name="attribute">
					<xsl:with-param name="value" select="'Yes'"/>
					<xsl:with-param name="attributeName" select="'lactating'"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="cbord:stringToLower(string(./personInfo/vip)) = 'y'">
				<xsl:call-template name="attribute">
					<xsl:with-param name="value" select="'Yes'"/>
					<xsl:with-param name="attributeName" select="'VIP'"/>
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="./personInfo/insulin">
				<xsl:if test="cbord:stringToLower(string(./personInfo/insulin)) = 'y'">
					<xsl:call-template name="attribute">
					<xsl:with-param name="value" select="'Yes'"/>
					<xsl:with-param name="attributeName" select="'insulin'"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:if test="cbord:stringToLower(string(./personInfo/insulin)) = 'n'">
					<xsl:call-template name="attribute">
					<xsl:with-param name="value" select="'No'"/>
					<xsl:with-param name="attributeName" select="'insulin'"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>

			<xsl:apply-templates select="./personInfo/*"/>
		</personInfo>

		<visitInfo>
			<xsl:apply-templates select="./visitInfo/*"/>
		</visitInfo>
		
		<xsl:if test="./mergeInfo">
	            <mergeInfo>
      	            <xsl:apply-templates select="./mergeInfo/*"/>
                  </mergeInfo>
		</xsl:if>

		<xsl:if test="./orderInfo">
			<xsl:choose>
				<xsl:when test="$useAgeBasedDiets='YES' and $noDOB='NO' and ./personInfo/ageIntervals/ageInterval='noDateOfBirth'"/>
				<xsl:otherwise>
			            <orderInfo>
      			            <xsl:apply-templates select="./orderInfo/*"/>
		                  </orderInfo>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="./WARNING">
      			<xsl:apply-templates select="./WARNING"/>
		</xsl:if>
	</CCIFADT>
</xsl:template>

<!-- OriginalMsgInfo -->
<xsl:template match="DTM">
	<DTM><xsl:value-of select="."/></DTM>
</xsl:template>
<xsl:template match="msgType">
</xsl:template>
<xsl:template match="Type">
	<Type><xsl:value-of select="."/></Type>
</xsl:template>
<xsl:template match="SequenceNumber">
	<xsl:if test="string(.)!=''">
		<SequenceNumber><xsl:value-of select="."/></SequenceNumber>
	</xsl:if>
</xsl:template>

<!-- operation -->
<xsl:template match="action"/>
<xsl:template match="facility">
	<facility><xsl:value-of select="."/></facility>
</xsl:template>
<xsl:template match="transactionDTM">
	<xsl:choose>
		<xsl:when test="./WARNING">
			<transactionDTM>
				<xsl:value-of select="cbord:getNow()"/>
				<WARNING><xsl:value-of select="./WARNING"/></WARNING>
			</transactionDTM>
		</xsl:when>
		<xsl:otherwise>
			<transactionDTM><xsl:value-of select="."/></transactionDTM>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- personInfo -->
<xsl:template match="sex"/>
<xsl:template match="pregnant"/>
<xsl:template match="lactating"/>
<xsl:template match="vip"/>
<xsl:template match="insulin"/>
<xsl:template match="MRN">
	<xsl:if test="string(.)!=''">
		<MRN><xsl:value-of select="."/></MRN>
	</xsl:if>
</xsl:template>
<xsl:template match="name">
	<xsl:if test="string(./lastName)!='' or string(./firstName)!='' or string(./middleName)!='' or string(./suffix)!='' or string(./nickName)!=''">
		<name>
			<xsl:apply-templates select="./*"/>
		</name>
	</xsl:if>
</xsl:template>
<!-- name -->
<xsl:template match="lastName">
	<xsl:if test="string(.)!=''">
		<lastName><xsl:value-of select="."/></lastName>
	</xsl:if>
</xsl:template>
<xsl:template match="firstName">
	<xsl:if test="string(.)!=''">
		<firstName><xsl:value-of select="."/></firstName>
	</xsl:if>
</xsl:template>
<xsl:template match="middleName">
	<xsl:if test="string(.)!=''">
		<middleName><xsl:value-of select="."/></middleName>
	</xsl:if>
</xsl:template>
<xsl:template match="suffix">
	<xsl:if test="string(.)!=''">
		<suffix><xsl:value-of select="."/></suffix>
	</xsl:if>
</xsl:template>
<xsl:template match="nickName">
	<xsl:if test="string(.)!=''">
		<nickName><xsl:value-of select="."/></nickName>
	</xsl:if>
</xsl:template>
<!-- personInfo cont. -->
<xsl:template match="DOB">
	<xsl:if test="string(.)!='' or ./WARNING">
		<xsl:choose>
			<xsl:when test="./WARNING">
				<WARNING><xsl:value-of select="./WARNING"/></WARNING>
			</xsl:when>
			<xsl:otherwise>
				<DOB><xsl:value-of select="."/></DOB>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
</xsl:template>
<xsl:template match="ageIntervals">
	<xsl:choose>
		<xsl:when test="./WARNING">
			<xsl:for-each select="./WARNING">
				<WARNING><xsl:value-of select="."/></WARNING>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="SSN">
	<xsl:if test="string(.)!=''">
		<SSN><xsl:value-of select="."/></SSN>
	</xsl:if>
</xsl:template>

<!-- visitInfo -->
<xsl:template match="patientLocation">
	<xsl:if test="string(./nursingStation)!='' or string(./room)!='' or string(./bed)!=''">
		<!-- Variable for extracted and translated location -->
		<xsl:variable name="room">
			<!-- Call to extract and translate location -->
			<xsl:call-template name="extractxRoom">
				<xsl:with-param name="ns"  select="./nursingStation"/>
				<xsl:with-param name="room"  select="./room"/>
				<xsl:with-param name="bed"  select="./bed"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- When variable has valid extracted and translated location: use it -->
		<!-- Otherwise, Produce WARNING tag -->
		<xsl:choose>
			<xsl:when test="contains($room, 'Invalid')">
				<WARNING><xsl:value-of select="$room"/></WARNING>
			</xsl:when>
			<xsl:otherwise>
				<Room><xsl:value-of select="$room"/></Room>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
</xsl:template>
<!-- extract and translate location -->
<xsl:template name="extractxRoom">
	<xsl:param name="ns"/>
	<xsl:param name="room"/>
	<xsl:param name="bed"/>

	<!-- call nursing station conversion function (user defined) -->
	<xsl:variable name="NSSns">
		<xsl:value-of select="cbordRM:getNursingStation(string($ns))"/>
	</xsl:variable>

	<!-- create room+bed key based on translation table rules -->
	<xsl:variable name="roomBed">
		<xsl:value-of select="cbordRM:getRoomBed(string($ns), string($room), string($bed))"/>
	</xsl:variable>

	<!-- for warning messages -->
	<xsl:variable name="keyStr">
		<xsl:value-of select="concat('Message Data: NS=',$ns,' Room=',$room,' Bed=',$bed,' Lookup Data: NS=',$NSSns,' Location=',$roomBed)"/>
	</xsl:variable>

	<!-- lookup room -->
	<xsl:variable name="xRoom">
		<xsl:value-of select="translator:lookupRoom(string(../../operation/facility), string($NursingIdOrName), string($NSSns), string($roomBed))"/>
	</xsl:variable>

	<xsl:choose>
		<xsl:when test="$xRoom=''">
			<xsl:value-of select="concat('Invalid location: ',$keyStr,'.')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$xRoom"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- visitInfo cont. -->
<xsl:template match="BillingID">
	<xsl:if test="string(.)!=''">
		<BillingID><xsl:value-of select="."/></BillingID>
	</xsl:if>
</xsl:template>
<xsl:template match="Physician">
	<xsl:if test="string(.)!=''">
		<Physician><xsl:value-of select="."/></Physician>
	</xsl:if>
</xsl:template>
<xsl:template match="AdmitDiag">
	<xsl:if test="string(.)!=''">
		<AdmitDiag><xsl:value-of select="."/></AdmitDiag>
	</xsl:if>
</xsl:template>
<xsl:template match="CodedDiagList">
	<CodedDiagList>
		<xsl:apply-templates select="./*"/>
	</CodedDiagList>
</xsl:template>
<!-- CodedDiagList -->
<xsl:template match="CodedDiag">
	<xsl:variable name="diagCode">
		<xsl:call-template name="extractxDiag">
                        <xsl:with-param name="value" select="."/>
		</xsl:call-template>
	</xsl:variable>
	
	<xsl:choose>
		<xsl:when test="contains($diagCode, 'is not valid.')">
			<WARNING><xsl:value-of select="$diagCode"/></WARNING>
		</xsl:when>
		<xsl:otherwise>
			<CodedDiag><xsl:value-of select="$diagCode"/></CodedDiag>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- Translate diagCode -->
<xsl:template name="extractxDiag">
	<xsl:param name="value"/>

	<xsl:variable name="codedDiag" select="translator:lookupDiag(string(../../../operation/facility),string($diagLookupKind),string($value))"/>

	<!-- generate warning on failure -->
	<xsl:if test="$codedDiag=''">
		<xsl:value-of select="concat('Coded diagnosis value ',$value,' is not valid.')"/>
	</xsl:if>
	<!-- successful lookup -->
	<xsl:if test="$codedDiag!=''">
		<xsl:value-of select="$codedDiag"/>
	</xsl:if>
</xsl:template>
<!-- ProtocolID -->
<!-- xsl:template match="ProtocolID">
	<ProtocolID>
		<xsl:apply-templates select="./*"/>
	</ProtocolID>
</xsl:template-->
<xsl:template match="ProtocolID">
	<xsl:variable name="protCode">
		<xsl:call-template name="extractxProt">
                        <xsl:with-param name="value" select="."/>
		</xsl:call-template>
	</xsl:variable>
	
	<xsl:choose>
		<xsl:when test="contains($protCode, 'is not valid.')">
			<WARNING><xsl:value-of select="$protCode"/></WARNING>
		</xsl:when>
		<xsl:otherwise>
			<ProtocolID><xsl:value-of select="$protCode"/></ProtocolID>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- Translate protCode -->
<xsl:template name="extractxProt">
	<xsl:param name="value"/>

	<xsl:variable name="ProtocolID" select="translator:lookupprotocol(string(../../operation/facility),string($value))"/>

	<!-- generate warning on failure -->
	<xsl:choose>
		<xsl:when test="$ProtocolID='' or $ProtocolID='-1'">
			<xsl:value-of select="concat('Coded Protocol ID value ',$value,' is not valid.')"/>
		</xsl:when>
		<!-- successful lookup -->
		<xsl:otherwise>
			<xsl:value-of select="$ProtocolID"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- CodedLanguagee -->

<xsl:template match="CodedLanguage">
	<xsl:variable name="langCode">
		<xsl:call-template name="extractxLang">
                        <xsl:with-param name="value" select="."/>
		</xsl:call-template>
	</xsl:variable>
	
	<xsl:choose>
		<xsl:when test="contains($langCode, 'is not valid.')">
			<WARNING><xsl:value-of select="$langCode"/></WARNING>
		</xsl:when>
		<xsl:otherwise>
			<CodedLanguage><xsl:value-of select="$langCode"/></CodedLanguage>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- Translate langCode -->
<xsl:template name="extractxLang">
	<xsl:param name="value"/>

	<xsl:variable name="CodedLanguage" select="translator:lookupLanguage(string(../../operation/facility),string($value))"/>

	<!-- generate warning on failure -->
	<xsl:choose>
		<xsl:when test="$CodedLanguage='' or $CodedLanguage='-1'">
			<xsl:value-of select="concat('Coded Language value ',$value,' is not valid.')"/>
		</xsl:when>
		<!-- successful lookup -->
		<xsl:otherwise>
			<xsl:value-of select="$CodedLanguage"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- visitInfo cont. -->
<xsl:template match="AllergyList">
	<AllergyList>
                <xsl:apply-templates select="./Allergy"/>
	</AllergyList>
</xsl:template>
<!-- AllergyList -->
<xsl:template match="Allergy">
	<xsl:variable name="allergyCode">
		<xsl:call-template name="extractxAllergy">
                        <xsl:with-param name="value" select="."/>
		</xsl:call-template>
	</xsl:variable>
	
	<xsl:choose>
		<xsl:when test="contains($allergyCode, 'is not valid.')">
			<WARNING><xsl:value-of select="$allergyCode"/></WARNING>
		</xsl:when>
		<xsl:otherwise>
			<Allergy><xsl:value-of select="$allergyCode"/></Allergy>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- Translate allergyCode -->
<xsl:template name="extractxAllergy">
	<xsl:param name="value"/>

	<xsl:variable name="codedAllergy" select="translator:lookupallergyrestriction(string(../../../operation/facility),string($value))"/>

	<xsl:choose>
		<!-- used for when remove allergies -->
		<xsl:when test= "$value = '-1'">
			<xsl:value-of select= "-1"/>
		</xsl:when>
		<!-- generate warning on failure -->
		<xsl:when test="$codedAllergy=''">
                	<xsl:value-of select="concat('Coded allergy value ',$value,' is not valid.')"/>
		</xsl:when>
		<!-- successful lookup -->
		<xsl:when test="$codedAllergy!=''">
			<xsl:value-of select="$codedAllergy"/>
		</xsl:when>
	</xsl:choose>

</xsl:template>
<!-- visitInfo cont. -->
<xsl:template match="DiagStartDTM">
        <xsl:choose>
                <xsl:when test="./DiagStart">
                	<DiagStartDTM>
                		<xsl:value-of select="./DiagStart"/>
                		<xsl:apply-templates select="./WARNING"/>
                	</DiagStartDTM>
                </xsl:when>
                <xsl:when test="not(./DiagStart) and ./WARNING">
                        <xsl:apply-templates select="./WARNING"/>
                </xsl:when>
                <xsl:otherwise>
			<DiagStartDTM><xsl:value-of select="./DiagStart"/></DiagStartDTM>
                </xsl:otherwise>                
        </xsl:choose>
</xsl:template>
<xsl:template match="AdmitDTM">
        <xsl:choose>
                <!-- msgAction is Admit use Admit tag (system DTM) and report WARNING -->              
                <xsl:when test="./Admit">
                        <AdmitDTM>
                        	<xsl:value-of select="./Admit"/>
                        	<xsl:apply-templates select="./WARNING"/>
                        </AdmitDTM>
                </xsl:when>
                <!-- msgAction is not admit (no Admit tag) report WARNING -->
                <xsl:when test="not(./Admit) and ./WARNING">
                        <xsl:apply-templates select="./WARNING"/>
                </xsl:when>
                <!-- Admit DTM valid -->
                <xsl:otherwise>
                        <AdmitDTM><xsl:value-of select="."/></AdmitDTM>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>
<xsl:template match="DischargeDTM">
        <xsl:choose>
                <!-- msgAction is Discharge use Discharge tag (system DTM) and report WARNING -->              
                <xsl:when test="./Discharge">
                        <DischargeDTM>
                        	<xsl:value-of select="./Discharge"/>
                        	<xsl:apply-templates select="./WARNING"/>
                        </DischargeDTM>
                </xsl:when>
                <!-- msgAction is not Discharge (no Discharge tag) report WARNING -->
                <xsl:when test="not(./Discharge) and ./WARNING">
                        <xsl:apply-templates select="./WARNING"/>
                </xsl:when>
                <!-- Discharge DTM valid -->
                <xsl:otherwise>
                        <DischargeDTM><xsl:value-of select="."/></DischargeDTM>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>
<xsl:template match="anthroInfo">
        <anthroInfo>
                <xsl:apply-templates select="./*"/>
        </anthroInfo>
</xsl:template>
<!-- anthroInfo -->
<xsl:template match="Frame">
        <Frame><xsl:value-of select="."/></Frame>
</xsl:template>
<xsl:template match="anthroStartDTM">
        <!--xsl:choose>
                <xsl:when test="./WARNING">
                	<xsl:apply-templates select="./WARNING"/>
                </xsl:when>
                <xsl:otherwise>
                        <StartDTM><xsl:value-of select="."/></StartDTM>
                </xsl:otherwise>
        </xsl:choose-->
        <xsl:if test="./WARNING">
        	<xsl:apply-templates select="./WARNING"/>
        </xsl:if>
        <StartDTM><xsl:value-of select="StartDTM"/></StartDTM>
</xsl:template>
<xsl:template match="WARNING">
	<WARNING><xsl:value-of select="."/></WARNING>
</xsl:template>
<xsl:template match="Height">
        <Height>
                <xsl:call-template name="attribute">
                        <xsl:with-param name="value" select="./@HeightUOFM"/>
                        <xsl:with-param name="attributeName" select="'HeightUOFM'"/>
                </xsl:call-template>
                <xsl:value-of select="."/>
        </Height>
</xsl:template>
<xsl:template match="Weight">
        <Weight>
                <xsl:call-template name="attribute">
                        <xsl:with-param name="value" select="./@WeightUOFM"/>
                        <xsl:with-param name="attributeName" select="'WeightUOFM'"/>
                </xsl:call-template>
                <xsl:value-of select="."/>
        </Weight>
</xsl:template>

<!-- mergeInfo -->
<xsl:template match="priorMRN">
        <priorMRN><xsl:value-of select="."/></priorMRN>
</xsl:template>
<xsl:template match="priorBillingID">
        <priorBillingID><xsl:value-of select="."/></priorBillingID>
</xsl:template>

<!-- orderInfo -->
<xsl:template match="DietOrderList">
        <DietOrderList>
                <xsl:apply-templates select="./*"/>
        </DietOrderList>
</xsl:template>
<!-- DietOrderList -->
<xsl:template match="DietOrderInfo">
        <DietOrderInfo>
                <xsl:apply-templates select="./*"/>
        </DietOrderInfo>
</xsl:template>
<!-- DietOrderInfo -->
<xsl:template match="OrderID">
        <OrderID><xsl:value-of select="."/></OrderID>
</xsl:template>
<xsl:template match="orderStartDTM">
        <xsl:choose>
                <xsl:when test="./Start">
                        <StartDTM><xsl:value-of select="./Start"/><WARNING><xsl:value-of select="./WARNING"/></WARNING></StartDTM>
                </xsl:when>
                <xsl:otherwise>
		    	<StartDTM>
				<xsl:if test="$orderDTMBeforeNow = 'YES'">
	                        <xsl:value-of select="."/>
				</xsl:if>
				<xsl:if test="$orderDTMBeforeNow = 'NO'">
					<xsl:variable name="messageDTM" select="."/>	
					<xsl:call-template name="dtmCompare">
		                        <xsl:with-param name="messageDTM" select="$messageDTM"/>
		                        <xsl:with-param name="systemDTM" select="cbord:getNow()"/>
					</xsl:call-template>
				</xsl:if>
			</StartDTM>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>

<xsl:template name="dtmCompare">
	<xsl:param name="messageDTM"/>
	<xsl:param name="systemDTM"/>

	<xsl:variable name="messageYear" select="substring($messageDTM,1,4)"/>
	<xsl:variable name="systemYear" select="substring($systemDTM,1,4)"/>
	<xsl:variable name="messageMonth" select="substring($messageDTM,6,2)"/>
	<xsl:variable name="systemMonth" select="substring($systemDTM,6,2)"/>
	<xsl:variable name="messageDay" select="substring($messageDTM,9,2)"/>
	<xsl:variable name="systemDay" select="substring($systemDTM,9,2)"/>
	<xsl:variable name="messageHour" select="substring($messageDTM,12,2)"/>
	<xsl:variable name="systemHour" select="substring($systemDTM,12,2)"/>
	<xsl:variable name="messageMin" select="substring($messageDTM,15,2)"/>
	<xsl:variable name="systemMin" select="substring($systemDTM,15,2)"/>
	<xsl:variable name="messageSec" select="substring($messageDTM,18,2)"/>
	<xsl:variable name="systemSec" select="substring($systemDTM,18,2)"/>

	<xsl:choose>
		<xsl:when test="$systemYear > $messageYear">
	            <xsl:value-of select="$systemDTM"/>
		</xsl:when>
		<xsl:when test="($systemYear = $messageYear) and ($systemMonth > $messageMonth)">
	            <xsl:value-of select="$systemDTM"/>
		</xsl:when>
		<xsl:when test="($systemYear = $messageYear) and 
				    ($systemMonth = $messageMonth) and 
				    ($systemDay > $messageDay)">
	            <xsl:value-of select="$systemDTM"/>
		</xsl:when>
		<xsl:when test="($systemYear = $messageYear) and 
				    ($systemMonth = $messageMonth) and 
				    ($systemDay = $messageDay) and
				    ($systemHour > $messageHour)">
	            <xsl:value-of select="$systemDTM"/>
		</xsl:when>
		<xsl:when test="($systemYear = $messageYear) and 
				    ($systemMonth = $messageMonth) and 
				    ($systemDay = $messageDay) and
				    ($systemHour = $messageHour) and
				    ($systemMin > $messageMin)">
	            <xsl:value-of select="$systemDTM"/>
		</xsl:when>
		<xsl:when test="($systemYear = $messageYear) and 
				    ($systemMonth = $messageMonth) and 
				    ($systemDay = $messageDay) and
				    ($systemHour = $messageHour) and
				    ($systemMin = $messageMin) and
				    ($systemSec > $messageSec)">
	            <xsl:value-of select="$systemDTM"/>
		</xsl:when>
		<xsl:otherwise>
	            <xsl:value-of select="$messageDTM"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="EndDTM">
        <xsl:choose>
                <xsl:when test="./End">
                        <EndDTM><xsl:value-of select="./End"/><WARNING><xsl:value-of select="./WARNING"/></WARNING></EndDTM>
                </xsl:when>
                <xsl:when test="./WARNING">
                        <WARNING><xsl:value-of select="./WARNING"/></WARNING>
                </xsl:when>
                <xsl:otherwise>
                        <EndDTM><xsl:value-of select="."/></EndDTM>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>
<xsl:template match="ConfirmedBy">
        <ConfirmedBy><xsl:value-of select="."/></ConfirmedBy>
</xsl:template>
<xsl:template match="OrderNote">
	<xsl:if test="count(Note)&gt;0">
		<OrderNote>
			<xsl:for-each select="Note">
				<xsl:value-of select="."/>
				<xsl:if test="not(position()=last())">
					<xsl:value-of select="';  '"/>
				</xsl:if>
			</xsl:for-each>
		</OrderNote>
 	</xsl:if>
</xsl:template>
<xsl:template match="CodedRestrictionList">
	<CodedRestrictionList>
		<xsl:apply-templates select="./CodedRestriction"/>
	</CodedRestrictionList>
</xsl:template>
<xsl:template match="CodedRestriction">
	<xsl:variable name="codedRestriction">
		<xsl:call-template name="extractxRestriction">
			<xsl:with-param name="value" select="."/>
		</xsl:call-template>
        </xsl:variable>
	
	<xsl:choose>
		<!-- Even though the restriction is invalid, pass it to the update layer. -->
		<!-- The update layer will see the restriction is invalid and will prevent any of the diet order from processing. -->
                <xsl:when test="contains($codedRestriction, 'is not valid.')">
	                <CodedRestriction><xsl:value-of select="-9999"/></CodedRestriction>
			<WARNING><xsl:value-of select="$codedRestriction"/></WARNING>
		</xsl:when>
		<xsl:otherwise>
                        <CodedRestriction><xsl:value-of select="$codedRestriction"/></CodedRestriction>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!-- Lookup restrictionCode -->
<xsl:template name="extractxRestriction">
	<xsl:param name="value"/>

        <xsl:variable name="codedRestriction" select="translator:lookupRestriction(string(../../../../../operation/facility), string($value))"/>
	
	<xsl:choose>
		<xsl:when test="$codedRestriction=''">
			<xsl:value-of select="concat('Coded restriction value ',$value,' is not valid.')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$codedRestriction"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
	

<!-- Supplement Orders -->
<!-- SupplementOrderList -->
<xsl:template match="SupplementOrderList">
        <SupplementOrderList>
                <xsl:apply-templates select="./*"/>
        </SupplementOrderList>
</xsl:template>
<!-- SupplementOrderInfo -->
<xsl:template match="SupplementOrderInfo">
        <SupplementOrderInfo>
                <xsl:apply-templates select="./*"/>
        </SupplementOrderInfo>
</xsl:template>
<xsl:template match="OrderID">
        <OrderID><xsl:value-of select="."/></OrderID>
</xsl:template>
<xsl:template match="orderStartDTM">
       <xsl:choose>
                <xsl:when test="./Start">
                        <StartDTM><xsl:value-of select="./Start"/><WARNING><xsl:value-of select="./WARNING"/></WARNING></StartDTM>
                </xsl:when>
                <xsl:when test="./WARNING">
                        <WARNING><xsl:value-of select="./WARNING"/></WARNING>
                </xsl:when>
                <xsl:otherwise>
                        <StartDTM><xsl:value-of select="."/></StartDTM>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>
<xsl:template match="EndDTM">
        <xsl:choose>
                <xsl:when test="./End">
                        <EndDTM><xsl:value-of select="./End"/><WARNING><xsl:value-of select="./WARNING"/></WARNING></EndDTM>
                </xsl:when>
                <xsl:when test="./WARNING">
                        <WARNING><xsl:value-of select="./WARNING"/></WARNING>
                </xsl:when>
                <xsl:otherwise>
                        <EndDTM><xsl:value-of select="."/></EndDTM>
                </xsl:otherwise>
        </xsl:choose>
</xsl:template>

<xsl:template match="CodedMealRuleList">
	<xsl:choose>
		<xsl:when test="count(CodedMealRule)&gt;1">
			<CodedMealRule>
				<!--Generate Error Msg to be used with rest of PB error msg -->
				<xsl:value-of select="'Meal combination not found in Meal Translation Table.  Supplement Meal Combinations:  '"/>
				<xsl:for-each select="CodedMealRule">
					<xsl:value-of select="."/>
					<xsl:if test="not(position()=last())">
						<xsl:value-of select="', '"/>
					</xsl:if>
					<xsl:if test="(position()=last())">
						<xsl:value-of select="' '"/>
					</xsl:if>
				</xsl:for-each>
			</CodedMealRule>
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="CodedMealRuleNotSpecified">
					<!-- When no Meals are specified we default to All Meals.  -2 is the internal ID indicate for ALL Meals.-->
					<CodedMealRule>-2</CodedMealRule>				
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="codedMealRule">
						<xsl:call-template name="extractxcodedMealRule">
							<xsl:with-param name="value" select="CodedMealRule"/>
						</xsl:call-template>
				        </xsl:variable>
					<xsl:choose>
						<!-- Even though the Supplement Meal Code is invalid, pass it to the update layer. -->
						<xsl:when test="contains($codedMealRule, 'is not valid.')">
							<!--Generate Error Msg to be used with rest of PB error msg -->
							<CodedMealRule><xsl:value-of select="-9999"/></CodedMealRule>
							<WARNING><xsl:value-of select="concat('Supplement Meal value ',CodedMealRule,' is not a valid Supplement Meal.')"/></WARNING>
						</xsl:when>
						<xsl:otherwise>
        		        		        <CodedMealRule><xsl:value-of select="$codedMealRule"/></CodedMealRule>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- Lookup supplement Meal Rule -->
<xsl:template name="extractxcodedMealRule">
	<xsl:param name="value"/>

        <xsl:variable name="codedMealRule">
		<xsl:choose>
			<xsl:when test="string(number($value))= 'NaN'">
				<xsl:value-of select="translator:lookupmealname(string(../../../../operation/facility), string($value))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="translator:lookupmealtimeperiod(string(../../../../operation/facility), number($value))"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:choose>
		<xsl:when test="$codedMealRule=''">
			<xsl:value-of select="concat('Supplement Meal value ',$value,' is not valid.')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$codedMealRule"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="CodedSupplementList">
	<CodedSupplementList>
		<xsl:apply-templates select="./*"/>
	</CodedSupplementList>
</xsl:template>

<xsl:template match="CodedSupplementInfo">
	<CodedSupplementInfo>
		<xsl:apply-templates select="./CodedItemUofM"/>
		<xsl:apply-templates select="./Quantity"/>
	</CodedSupplementInfo>
</xsl:template>

<xsl:template match="CodedItemUofM">
	<xsl:variable name="codedItemUofM">
		<xsl:call-template name="extractxItemUofM">
			<xsl:with-param name="value" select="."/>
		</xsl:call-template>
        </xsl:variable>

	<xsl:choose>
		<!-- Even though the Supplement is invalid, pass it to the update layer. -->
		<xsl:when test="contains($codedItemUofM, 'is not valid.')">
	                <CodedItemUofM><xsl:value-of select="-9999"/></CodedItemUofM>
			<WARNING><xsl:value-of select="$codedItemUofM"/></WARNING>
		</xsl:when>
		<xsl:otherwise>
                        <CodedItemUofM><xsl:value-of select="$codedItemUofM"/></CodedItemUofM>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!-- Lookup supplementCode -->
<xsl:template name="extractxItemUofM">
	<xsl:param name="value"/>

        <xsl:variable name="codedItemUofM" select="translator:lookupsupplement(string(../../../../../../operation/facility), string($value))"/>
	
	<xsl:choose>
		<xsl:when test="$codedItemUofM=''">
			<xsl:value-of select="concat('Coded supplement value ',$value,' is not valid.')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$codedItemUofM"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="Quantity">
        <Quantity><xsl:value-of select="."/></Quantity>
</xsl:template>

<!-- Begin ServiceOrder -->
<xsl:template match="ServiceOrderList">
	<ServiceOrderList>
		<xsl:apply-templates select="./*"/>
	</ServiceOrderList>
</xsl:template>
<xsl:template match="ServiceOrderInfo">
	<ServiceOrderInfo>
		<xsl:apply-templates select="./*"/>
	</ServiceOrderInfo>
</xsl:template>

<xsl:template match="serviceStartDTM">
	<xsl:if test="string(./returnTime)!=''">
		<StartDTM>
			<xsl:value-of select="substring(string(./returnTime),1,10)"/>
		</StartDTM>
	</xsl:if>
	<xsl:for-each select="./WARNING">
		<WARNING><xsl:value-of select="."/></WARNING>
	</xsl:for-each>
</xsl:template>
<xsl:template match="serviceEndDTM">
	<xsl:if test="string(./returnTime)!=''">
		<EndDTM>
			<xsl:value-of select="substring(string(./returnTime),1,10)"/>
		</EndDTM>
	</xsl:if>
	<xsl:for-each select="./WARNING">
		<WARNING><xsl:value-of select="."/></WARNING>
	</xsl:for-each>
</xsl:template>
<xsl:template match="codedDayRule">
	<xsl:if test="string(.)!=''">
		<CodedDayRule>
			<xsl:value-of select="translator:lookupDayruleName(string(../../../../operation/facility),string(./dayOfWeek))"/>
		</CodedDayRule>
	</xsl:if>
</xsl:template>
<xsl:template match="codedStartMeal">
	<xsl:if test="string(.)!=''">
		<CodedStartMeal>
			<xsl:call-template name="extractxcodedMealRule">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>	
		</CodedStartMeal>
	</xsl:if>
</xsl:template>
<xsl:template match="codedEndMeal">
	<xsl:if test="string(.)!=''">
		<CodedEndMeal>
			<xsl:call-template name="extractxcodedMealRule">
				<xsl:with-param name="value" select="."/>
			</xsl:call-template>	
		</CodedEndMeal>
	</xsl:if>
</xsl:template>
<xsl:template match="serviceLocationInfo">
	<CodedService>
		<xsl:call-template name="getCodedService">
			<xsl:with-param name="serviceLocation" select="./serviceLocation"/>
			<xsl:with-param name="serviceNS" select="./serviceNS"/>
		</xsl:call-template>
	</CodedService>
</xsl:template>
<xsl:template name="getCodedService">
	<xsl:param name="serviceLocation"/>
	<xsl:param name="serviceNS"/>

	<xsl:if test="string($serviceNS)!='' or string($serviceLocation)!=''">
		<!-- for warning messages -->
		<xsl:variable name="keyStr">
			<xsl:value-of select="concat('Lookup Data: NS=',$serviceNS,' Location=',$serviceLocation)"/>
		</xsl:variable>

		<!-- lookup room -->
		<xsl:variable name="xRoom">
			<xsl:value-of select="translator:lookupRoom(string(../../../../operation/facility), string('ID'), string($serviceNS), string($serviceLocation))"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$xRoom=''">
				<WARNING><xsl:value-of select="concat('Invalid location: ',$keyStr,'.')"/></WARNING>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$xRoom"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
</xsl:template>
<xsl:template match="trayNote">
	<xsl:if test="string(.)!=''">
		<TrayNote>
			<xsl:value-of select="."/>
		</TrayNote>
	</xsl:if>
</xsl:template>
<xsl:template match="menuNote">
	<xsl:if test="string(.)!=''">
		<MenuNote>
			<xsl:value-of select="."/>
		</MenuNote>
	</xsl:if>
</xsl:template>

<!-- Enteral Order Info -->
<xsl:template match="EnteralOrderList">
	<EnteralOrderList>
		<xsl:apply-templates select="./*"/>
	</EnteralOrderList>
</xsl:template>
<xsl:template match="EnteralOrderInfo">
	<EnteralOrderInfo>
		<xsl:apply-templates select="./*"/>
	</EnteralOrderInfo>
</xsl:template>
<xsl:template match="enteralStartDTM">
	<xsl:if test="string(./returnTime)!=''">
		<StartDTM>
			<xsl:value-of select="./returnTime"/>
		</StartDTM>
	</xsl:if>
	<xsl:for-each select="./WARNING">
		<WARNING><xsl:value-of select="."/></WARNING>
	</xsl:for-each>
</xsl:template>
<xsl:template match="enteralEndDTM">
	<xsl:if test="string(./returnTime)!=''">
		<EndDTM>
			<xsl:value-of select="./returnTime"/>
		</EndDTM>
	</xsl:if>
	<xsl:for-each select="./WARNING">
		<WARNING><xsl:value-of select="."/></WARNING>
	</xsl:for-each>
</xsl:template>
<xsl:template match="CalculatedBy">
	<xsl:if test="string(.)!=''">
		<CalculatedBy>
			<xsl:value-of select="."/>
		</CalculatedBy>
	</xsl:if>
</xsl:template>
<xsl:template match="CalculatedDTM">
	<xsl:if test="string(.)!=''">
		<CalculatedDTM>
			<xsl:value-of select="."/>
		</CalculatedDTM>
	</xsl:if>
</xsl:template>
<xsl:template match="ConfirmedBy">
	<xsl:if test="string(.)!=''">
		<ConfirmedBy>
			<xsl:value-of select="."/>
		</ConfirmedBy>
	</xsl:if>
</xsl:template>
<xsl:template match="ConfirmedDTM">
	<xsl:if test="string(.)!=''">
		<ConfirmedDTM>
			<xsl:value-of select="."/>
		</ConfirmedDTM>
	</xsl:if>
</xsl:template>
<xsl:template match="NotesInfo">
	<xsl:if test="string(.)!=''">
		<NotesInfo>
			<xsl:apply-templates select="./*"/>
		</NotesInfo>
	</xsl:if>
</xsl:template>
<xsl:template match="OrderSummaryNote">
	<xsl:if test="string(.)!=''">
		<OrderSummaryNote>
			<xsl:value-of select="."/>
		</OrderSummaryNote>
	</xsl:if>
</xsl:template>



<!-- Apply attribute -->
<xsl:template name="attribute">
	<xsl:param name="value"/>
	<xsl:param name="attributeName"/>
	<xsl:if test="$attributeName='name'">
		<xsl:attribute name="name"><xsl:value-of select="$value"/></xsl:attribute>
	</xsl:if>
	<xsl:if test="$attributeName='sex'">
		<xsl:attribute name="sex"><xsl:value-of select="$value"/></xsl:attribute>
	</xsl:if>
	<xsl:if test="$attributeName='pregnant'">
		<xsl:attribute name="pregnant"><xsl:value-of select="$value"/></xsl:attribute>
	</xsl:if>
	<xsl:if test="$attributeName='lactating'">
		<xsl:attribute name="lactating"><xsl:value-of select="$value"/></xsl:attribute>
	</xsl:if>
	<xsl:if test="$attributeName='VIP'">
		<xsl:attribute name="VIP"><xsl:value-of select="$value"/></xsl:attribute>
	</xsl:if>
	<xsl:if test="$attributeName='insulin'">
		<xsl:attribute name="insulin"><xsl:value-of select="$value"/></xsl:attribute>
	</xsl:if>
        <xsl:if test="$attributeName='HeightUOFM'">
                <xsl:attribute name="HeightUOFM"><xsl:value-of select="$value"/></xsl:attribute>
	</xsl:if>
        <xsl:if test="$attributeName='WeightUOFM'">
                <xsl:attribute name="WeightUOFM"><xsl:value-of select="$value"/></xsl:attribute>
	</xsl:if>
</xsl:template>

<!-- Message Filtered -->
<xsl:template name="filterMsg"/>

</xsl:stylesheet>
