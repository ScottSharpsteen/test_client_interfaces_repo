// HL7 message rule definitions with XML output
// PVCS version $Revision: #2 $
// 	msgRules.v21 - HL7 version 2.1 message rules. Have two formats:
// 	  colon format 
// 		msg_typeoptional_event:segment segment ... on one line.
// 	  semicolon format
// 		msg_typeoptional_event;previous msg_typeoptional_event
// 	note the ^ has been removed from between msg_type and optional_event
// 		[ xxx ] - means 0 or 1 of xxx in message.
// 		{ xxx } - means 1 or more of xxx in message.
// 		< xxx > - means 0 or more of xxx in message, this replaces
// 				{[ xxx ]} and [{ xxx }] in the manual.
// 		( xxx yyy zzz) - means, only one of.
// 	msgRules.v22c236 - chapters 2,3 and 6 of version 2.2
// 
// XML annotations are in " "; these provide the translation into HL7 v2.3.1 XML
// annotations can be missing, which implies that the message is ignored by CBORD NSS ADT

// NON HL7 behavior
// uncomment the following to allow linefeeds (0x0a) after a segment terminator (0x0c)
// #pragma AllowLF

// Acknowledge received message
ACK"ACK":MSH MSA [ ERR ] 

// admit a patient
ADTA01"ADT_A01":MSH [EVN] PID [PD1] <NK1> PV1 [PV2]<DB1><OBX><AL1><DG1>[DRG] <PR1 <ROL>> <GT1> <IN1 [IN2]<IN3>> [ACC][UB1][UB2] 

// transfer a patient
ADTA02"ADT_A02":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX> 

// discharge a patient
ADTA03"ADT_A03":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <DG1> [DRG] <PR1 <ROL>> <OBX>

// register a patient (treat as admit)
ADTA04"ADT_A04":MSH [EVN] PID [PD1] <NK1> PV1 [PV2] <DB1> <OBX> <AL1> <DG1> [DRG] <PR1 <ROL>> <GT1> <IN1 [IN2] <IN3>>[ACC] [UB1] [UB2]

// pre-admit a patient (treat as admit)
ADTA05"ADT_A05":MSH [EVN] PID [PD1] <NK1> PV1 [PV2] <DB1> <OBX> <AL1> <DG1> [DRG] <PR1 <ROL>> <GT1> <IN1 [IN2] <IN3>>[ACC] [UB1] [UB2]

// transfer an outpatient to inpatient (treated as Admit)
ADTA06"ADT_A06":MSH [EVN] PID [PD1] [MRG] <NK1> PV1 [PV2]<DB1><OBX><AL1><DG1>[DRG]<PR1<ROL>><GT1><IN1 [IN2]<IN3>>[ACC][UB1][UB2]

// transfer an inpatient to outpatient (treated as Discharge)
ADTA07"ADT_A07":MSH [EVN] PID [PD1] [MRG] <NK1> PV1 [PV2]<DB1><OBX><AL1><DG1>[DRG]<PR1<ROL>><GT1><IN1 [IN2]<IN3>>[ACC][UB1][UB2]

// update patient information
ADTA08"ADT_A08":MSH [EVN] PID [PD1] <NK1> PV1 [PV2]<DB1><OBX><AL1><DG1>[DRG]<PR1<ROL>><GT1><IN1 [IN2]<IN3>>[ACC][UB1][UB2]

// patient departing
ADTA09"ADT_A09":MSH [EVN] PID [PD1] PV1 [PV2]<DB1><OBX><DG1>

// patient arriving
ADTA10"ADT_A10":MSH [EVN] PID [PD1] PV1 [PV2]<DB1><OBX><DG1>

// cancel admit (treated as discharge)
ADTA11"ADT_A11":MSH [EVN] PID [PD1] PV1 [PV2]<DB1><OBX><DG1>

// cancel transfer
ADTA12"ADT_A12":MSH [EVN] PID [PD1] PV1 [PV2]<DB1><OBX><DG1>

// Cancel Discharge
ADTA13"ADT_A13":MSH [EVN] PID [PD1] <NK1> PV1 [PV2]<DB1><OBX><AL1><DG1>[DRG]<PR1<ROL>><GT1><IN1 [IN2]<IN3>>[ACC][UB1][UB2]

// Swap patients
ADTA17"ADT_A17":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX> PID [PD1] PV1 [PV2] <DB1> <OBX> 

// Update Patient ID
ADTA18"ADT_A18":MSH [EVN] PID [PD1] [MRG] [PV1]

// patient goes on LOA (treat as discharge)
ADTA21"ADT_A21":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX>

// patient returns from LOA (treat as admit)
ADTA22"ADT_A22":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX>

// Update Person Information
ADTA31"ADT_A31":MSH [EVN] PID [PD1] <NK1> PV1 [PV2] <DB1><OBX><AL1><DG1>[DRG]<PR1 <ROL>><GT1> <IN1 [IN2] <IN3>>[ACC] [UB1] [UB2] 

//Cancel patient arriving - a discharge
ADTA32"ADT_A32":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX>

//Cancel patient departing - a cancel discharge
ADTA33"ADT_A33":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX>

// Update Patient ID
ADTA34"ADT_A34":MSH [EVN] PID [PD1] MRG [PV1]
ADTA35"ADT_A35":MSH [EVN] PID [PD1] MRG [PV1]
ADTA36"ADT_A36":MSH [EVN] PID [PD1] MRG [PV1]

//Diet Orders
ORMO01"ORM_O01":MSH [EVN] <NTE> PID <NTE> [PV1][PV2] [IN1][IN2][IN3][GT1] <AL1> <ORC <ODS <NTE>> <DG1> [<OBX<NTE>>]<ODT><NTE><RXO<NTE>><RXR><RXC<NTE>>>
ORMO09"ORM_O09":MSH [EVN] <NTE> PID <NTE> [PV1][PV2] [IN1][IN2][IN3][GT1] <AL1> <ORC <ODS <NTE>> <DG1> [<OBX<NTE>>]<ODT><NTE><RXO<NTE>><RXR><RXC<NTE>>>
OMPO09"OMP_O09":MSH [EVN] <NTE> PID <NTE> [PV1][PV2] [IN1][IN2][IN3][GT1] <AL1> <ORC <ODS <NTE>> <DG1> [<OBX<NTE>>]<ODT><NTE><RXO<NTE>><RXR><RXC<NTE>>>


// IGNORED
// pending Admit (IGNORED)
ADTA14"ADT_A14";ADTA01
// pending transfer (IGNORED)
ADTA15"ADT_A15";ADTA09
// pending Discharge (IGNORED)
ADTA16"ADT_A16":MSH [EVN] PID [PD1] PV1 [PV2]<DB1><OBX><DG1>[DRG]

ADTA19"ADT_A19":MSH QRD [QRF]
ADRA19"ADR_A19":MSH MSA QRD { [EVN] PID PV1 [PV2]<OBX><AL1><DG1><PR1><GT1><IN1 [IN2][IN3]>[ACC] [UB1][UB2] }[DSC] 
ADTA20"ADT_A20":MSH EVN NPU 
ADTA23"ADT_A23":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX> 
ADTA24"ADT_A24":MSH [EVN] PID [PD1] [PV1]<DB1> PID [PD1] [PV1]<DB1>
ADTA25"ADT_A25":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX>
ADTA26"ADT_A26":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX>
ADTA27"ADT_A27":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX> 
ADTA28"ADT_A28":MSH [EVN] PID [PD1] <NK1> PV1 [PV2]<DB1><OBX><AL1><DG1>[DRG] <PR1 <ROL>> <GT1> <IN1 [IN2]<IN3>> [ACC][UB1][UB2] 
ADTA29"ADT_A29":MSH [EVN] PID [PD1] PV1 [PV2] <DB1> <OBX>
ADTA30"ADT_A30":MSH [EVN] PID [PD1] MRG 
// unlink patient information
ADTA37"ADT_A37";ADTA24
// Cancel pre-admit
ADTA38"ADT_A38":MSH[EVN]PID[PD1] PV1[PV2]<DB1><OBX><DB1>[DRG]
//merge person - patient id 
ADTA39"ADT_A39":MSH[EVN]PID[PD1]MRG[PV1]
//merge patitent - patient identifier list
ADTA40"ADT_A40":MSH[EVN]PID[PD1]MRG[PV1]
// merge account - patient account number
ADTA41"ADT_A41":MSH[EVN]PID[PD1]MRG[PV1]
// merge visit - visit number
ADTA42"ADT_A42":MSH[EVN]PID[PD1]MRG[PV1]
// move patient information - patient identifier list
ADTA43"ADT_A43":MSH[EVN]PID[PD1]MRG
// move account information - patient account number
ADTA44"ADT_A44":MSH[EVN]PID[PD1]MRG
// move visit information - visit number
ADTA45"ADT_A45":MSH[EVN]PID[PD1]MRG PV1
// change patient id
ADTA46"ADT_A46":MSH[EVN]PID[PD1]MRG
// change patient identifier list
ADTA47"ADT_A47":MSH[EVN]PID[PD1]MRG
// change alternate patient ID
ADTA48"ADT_A48":MSH[EVN]PID[PD1]MRG
// change patient account number
ADTA49"ADT_A49":MSH[EVN]PID[PD1]MRG
// change visit number
ADTA50"ADT_A50":MSH[EVN]PID[PD1]MRG PV1
// change alternate visit ID
ADTA51"ADT_A51":MSH[EVN]PID[PD1]MRG PV1


MCF"MCF":MSH MSA [ ERR ]
UDMQ05"UDM_Q05":MSH URD [URS] {DSP} [DSC]    
QRYQ01"QRY_Q01":MSH QRD [QRF] [DSC]  
DSRQ01"DSR_Q01":MSH MSA QRD [QRF] {DSP} [DSC]  
QRYQ02"QRY_Q02":MSH QRD [ QRF ]  [ DSC ]  
DSRQ03"DSR_Q03":MSH QRD [QRF]{DSP}[DSC]

BARP01"BAR_P01":MSH EVN PID { [PV1][PV2] <OBX><AL1><DG1><PR1><GT1><NK1> <IN1[IN2][IN3]>[ACC][UB1][UB2] }
BARP02"BAR_P02":MSH EVN { PID  [PV1]}
DFTP03"DFT_P03":MSH EVN PID [PV1][PV2] <OBX> {FT1}    
QRYP04"QRY_P04":MSH
ORUR01"ORU_R01":MSH [MSA] [ [ PID <NTE> [PV1] ] { [ORC] OBR <NTE> { [OBX] <NTE> } } ] [DSC]
